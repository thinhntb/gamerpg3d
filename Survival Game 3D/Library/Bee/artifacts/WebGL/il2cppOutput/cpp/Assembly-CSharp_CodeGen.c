﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DynamicSkyLite::Awake()
extern void DynamicSkyLite_Awake_mD05B187151D8AF82976909464999B18975599B58 (void);
// 0x00000002 System.Void DynamicSkyLite::LateUpdate()
extern void DynamicSkyLite_LateUpdate_m4B34FC4F639CF6987CC7FC153E5C895D745E9561 (void);
// 0x00000003 System.Void DynamicSkyLite::.ctor()
extern void DynamicSkyLite__ctor_mCB7C02619FA1BB4FF3566B436B49500C0FCBDE14 (void);
// 0x00000004 System.Void FPSCounter::Awake()
extern void FPSCounter_Awake_mADDB1FC3597AD55B60F1359D329EC9E81774C22C (void);
// 0x00000005 System.Void FPSCounter::OnEnable()
extern void FPSCounter_OnEnable_m6554D2DACA28C66EBE821F42F83BA2C955141EDC (void);
// 0x00000006 System.Void FPSCounter::Update()
extern void FPSCounter_Update_m8FD51CDD4FC6103E47194F4D9848A7D325004E30 (void);
// 0x00000007 System.Collections.IEnumerator FPSCounter::DrawGraph()
extern void FPSCounter_DrawGraph_m7E4BEB5E1EE59B0A11047D1B5DB474D255557414 (void);
// 0x00000008 System.Void FPSCounter::CreateCounter()
extern void FPSCounter_CreateCounter_m9B1865ED789B779C54FB906FFC1E5AD0D11CEB68 (void);
// 0x00000009 UnityEngine.GameObject FPSCounter::GiveLine()
extern void FPSCounter_GiveLine_m2A91354B96AC53F958EA82E35C28A7D50C81AD53 (void);
// 0x0000000A System.Void FPSCounter::.ctor()
extern void FPSCounter__ctor_m4814EB8CF15E05040C15D4F6B057357EC68B7268 (void);
// 0x0000000B System.Void FPSCounter/<DrawGraph>d__20::.ctor(System.Int32)
extern void U3CDrawGraphU3Ed__20__ctor_m8CC663E118E5465C99EC34EDCCD7756345740767 (void);
// 0x0000000C System.Void FPSCounter/<DrawGraph>d__20::System.IDisposable.Dispose()
extern void U3CDrawGraphU3Ed__20_System_IDisposable_Dispose_m0E08B87199DB83B60DAB58AE965E204B93EFD195 (void);
// 0x0000000D System.Boolean FPSCounter/<DrawGraph>d__20::MoveNext()
extern void U3CDrawGraphU3Ed__20_MoveNext_m1138E88461B5BBDF9620CB6F87BB1BE76B860581 (void);
// 0x0000000E System.Object FPSCounter/<DrawGraph>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDrawGraphU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08CAD522045EAD1AF11B3C36015BD171E8B20354 (void);
// 0x0000000F System.Void FPSCounter/<DrawGraph>d__20::System.Collections.IEnumerator.Reset()
extern void U3CDrawGraphU3Ed__20_System_Collections_IEnumerator_Reset_m583A769391B91A101E5FCB10D336F77CA90AB004 (void);
// 0x00000010 System.Object FPSCounter/<DrawGraph>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CDrawGraphU3Ed__20_System_Collections_IEnumerator_get_Current_mB540BFA0CEBB2A5943109DB0D85AA3F9770A650C (void);
// 0x00000011 UnityEngine.Vector3Int StFPS::Counter(System.Int32,System.Single)
extern void StFPS_Counter_m51132A4B6EF178E1BB431E75DBACA71E6DF2BC7E (void);
// 0x00000012 System.Void StFPS::.cctor()
extern void StFPS__cctor_m4757E6940866B358734CA065EB65C97C653AEED3 (void);
// 0x00000013 System.Void Water::Awake()
extern void Water_Awake_mA0CF580A48B28332DF36ABC2AD9B1EDDA0C08F08 (void);
// 0x00000014 System.Void Water::OnEnable()
extern void Water_OnEnable_mD7D50AE2976B32F886BDF5592B2B57F91E3AE822 (void);
// 0x00000015 System.Void Water::LateUpdate()
extern void Water_LateUpdate_mEE64408A397585CD33EA9825A952E68A31827928 (void);
// 0x00000016 System.Void Water::.ctor()
extern void Water__ctor_m39BD6E49B74929BAB39A81C4A4A7DDFFC5C8146F (void);
// 0x00000017 System.Void Shark::Awake()
extern void Shark_Awake_m7861399260147AB35AF2A7384CA09B84E8959975 (void);
// 0x00000018 System.Void Shark::LateUpdate()
extern void Shark_LateUpdate_m6C05851ACF1C0DEFCECDB1A29AFB05954A96EC71 (void);
// 0x00000019 System.Void Shark::Hunting()
extern void Shark_Hunting_m8E2B397B7AD1E0F987420E821E0D32CBF84DD18B (void);
// 0x0000001A System.Void Shark::Move()
extern void Shark_Move_m94231792C92A537C8B73F5E12C7099C32746B93A (void);
// 0x0000001B System.Void Shark::CameraRig()
extern void Shark_CameraRig_m687ED8420E7B8D158E526358B4865E8EDC0330DB (void);
// 0x0000001C System.Collections.IEnumerator Shark::RandomVector()
extern void Shark_RandomVector_mAC25B961835E406CC55C6F8C7F0E8820684C3937 (void);
// 0x0000001D System.Void Shark::DebugPath()
extern void Shark_DebugPath_m519E1209F731F0F86408D4869FB9FE7C60ACC443 (void);
// 0x0000001E System.Void Shark::.ctor()
extern void Shark__ctor_m44D4C7E30B83B9FA3CB11C7AFA49CBDCA79CE53E (void);
// 0x0000001F System.Void Shark::.cctor()
extern void Shark__cctor_mA83CD825BC2540B957117191B22B9F49AE6419F2 (void);
// 0x00000020 System.Void Shark/<RandomVector>d__26::.ctor(System.Int32)
extern void U3CRandomVectorU3Ed__26__ctor_mE3EF35212D466F18FCED8FCD2B2C4DFAC6BDF7C1 (void);
// 0x00000021 System.Void Shark/<RandomVector>d__26::System.IDisposable.Dispose()
extern void U3CRandomVectorU3Ed__26_System_IDisposable_Dispose_m5D1FA545D964689830D48232E7EB119B65315F98 (void);
// 0x00000022 System.Boolean Shark/<RandomVector>d__26::MoveNext()
extern void U3CRandomVectorU3Ed__26_MoveNext_m3881F6AC56F441A25FF117AB498608723CF4B92E (void);
// 0x00000023 System.Object Shark/<RandomVector>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRandomVectorU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF68C011DBED959E418ECACB26C2D38919C6EDE0 (void);
// 0x00000024 System.Void Shark/<RandomVector>d__26::System.Collections.IEnumerator.Reset()
extern void U3CRandomVectorU3Ed__26_System_Collections_IEnumerator_Reset_m719569C8C8F7A881438D7234399BCB66FAEA3617 (void);
// 0x00000025 System.Object Shark/<RandomVector>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CRandomVectorU3Ed__26_System_Collections_IEnumerator_get_Current_m77C114A7B587BF2CE52C2E84BBF611CBCCF6D526 (void);
// 0x00000026 System.Void SlowMo::Awake()
extern void SlowMo_Awake_mC3D61772F946ECB30A57EFA558587F8317C500B8 (void);
// 0x00000027 System.Void SlowMo::LateUpdate()
extern void SlowMo_LateUpdate_m84837A88B05D29F4C136ABF267D890352B285A47 (void);
// 0x00000028 System.Void SlowMo::OnApplicationQuit()
extern void SlowMo_OnApplicationQuit_mB2C144681DE899649E309847D9400C859923CA78 (void);
// 0x00000029 System.Void SlowMo::.ctor()
extern void SlowMo__ctor_mA7CE26DF0BD2C3F0397843C764C5F07C6C012CC7 (void);
// 0x0000002A System.Void TDControl::Awake()
extern void TDControl_Awake_mD9C8F1D0EBE6862EC456DE56170CE73762A16B2C (void);
// 0x0000002B System.Void TDControl::LateUpdate()
extern void TDControl_LateUpdate_m8EC9D696B49C2B43EE3F7BDA00EC995B63911EB3 (void);
// 0x0000002C System.Void TDControl::Rotation()
extern void TDControl_Rotation_mD9A3545A1ABA6739FEFE0A8163AAF926A26E86E2 (void);
// 0x0000002D System.Void TDControl::Lift()
extern void TDControl_Lift_m82831324FEA6BB2E57EBAA91983250DE5E096F1B (void);
// 0x0000002E System.Void TDControl::CameraTransform()
extern void TDControl_CameraTransform_mE5052E37A7FB82B30C6725C4165AC298C9E8A6DA (void);
// 0x0000002F System.Void TDControl::.ctor()
extern void TDControl__ctor_m3214C8ED125DFB18731A2A8FC8FA08B7D68F0CDB (void);
// 0x00000030 System.Void Underwater::Awake()
extern void Underwater_Awake_m0FB6A411A1E5417982DC4DA1125A0F5CC6880DFA (void);
// 0x00000031 System.Void Underwater::LateUpdate()
extern void Underwater_LateUpdate_mB052DF2D28001B461CE1962133317B2E84DCBD16 (void);
// 0x00000032 System.Void Underwater::.ctor()
extern void Underwater__ctor_mF7835E6A62844ED26594301F6052C3C8AE099265 (void);
// 0x00000033 System.Void NVBoids::Awake()
extern void NVBoids_Awake_m5ADBFB6AABCC2EBE83BE280BD84359AB1B277147 (void);
// 0x00000034 System.Void NVBoids::LateUpdate()
extern void NVBoids_LateUpdate_m4BE7F707DA445016F2DD35743B7F86C02F7EE7DE (void);
// 0x00000035 System.Void NVBoids::FlocksMove()
extern void NVBoids_FlocksMove_m530F8D5D816472DDA7B57983D8EF2AA8F43C008B (void);
// 0x00000036 System.Void NVBoids::BirdsMove()
extern void NVBoids_BirdsMove_m2793ABDB45B5B9C7AFF873E7008F65B75104159D (void);
// 0x00000037 System.Collections.IEnumerator NVBoids::Danger()
extern void NVBoids_Danger_m8FF58867142747A9811C3393C3D1E89FFF892C5A (void);
// 0x00000038 System.Collections.IEnumerator NVBoids::BehavioralChange()
extern void NVBoids_BehavioralChange_m017C5707C20BF800D129CD103F1D27AC9521098E (void);
// 0x00000039 System.Void NVBoids::CreateFlock()
extern void NVBoids_CreateFlock_mBEC8F2E91EF7D4936E117ED12D8DFDC253573D34 (void);
// 0x0000003A System.Void NVBoids::CreateBird()
extern void NVBoids_CreateBird_mDEE562AA687E0F493A7414A7E08AA86961B55408 (void);
// 0x0000003B UnityEngine.Quaternion NVBoids::BirdsRotationClamp(UnityEngine.Quaternion,System.Single)
extern void NVBoids_BirdsRotationClamp_mB85FE5E10687EA18E5DD9817006CEF894A5526BB (void);
// 0x0000003C System.Void NVBoids::.ctor()
extern void NVBoids__ctor_mD8A280E8811740B328B85FA57E5A7628615EABB0 (void);
// 0x0000003D System.Void NVBoids/<Danger>d__46::.ctor(System.Int32)
extern void U3CDangerU3Ed__46__ctor_mAB0BB3A40C436B3D0F4084C41EFFA0F8803F566F (void);
// 0x0000003E System.Void NVBoids/<Danger>d__46::System.IDisposable.Dispose()
extern void U3CDangerU3Ed__46_System_IDisposable_Dispose_m961FB5C0299DD1B9A408A3E121E337CAE7374316 (void);
// 0x0000003F System.Boolean NVBoids/<Danger>d__46::MoveNext()
extern void U3CDangerU3Ed__46_MoveNext_m6528C89181BE2B86315709763A6924E756CB4927 (void);
// 0x00000040 System.Object NVBoids/<Danger>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDangerU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17A5CE6205D59D77D83AEB82B980ED780895255C (void);
// 0x00000041 System.Void NVBoids/<Danger>d__46::System.Collections.IEnumerator.Reset()
extern void U3CDangerU3Ed__46_System_Collections_IEnumerator_Reset_m78045BB15689020B4CC8FADEBB764942339619F0 (void);
// 0x00000042 System.Object NVBoids/<Danger>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CDangerU3Ed__46_System_Collections_IEnumerator_get_Current_m6E552E8C095410E56A45CA746EFA786660AD161C (void);
// 0x00000043 System.Void NVBoids/<BehavioralChange>d__47::.ctor(System.Int32)
extern void U3CBehavioralChangeU3Ed__47__ctor_mEFC303C8B78FEE506C47D6A7471B08A1A30CBF77 (void);
// 0x00000044 System.Void NVBoids/<BehavioralChange>d__47::System.IDisposable.Dispose()
extern void U3CBehavioralChangeU3Ed__47_System_IDisposable_Dispose_mF6CFD9756C20CAA2179EA57276A5199E68C24CE8 (void);
// 0x00000045 System.Boolean NVBoids/<BehavioralChange>d__47::MoveNext()
extern void U3CBehavioralChangeU3Ed__47_MoveNext_m19817EF5F5018F09BD1952B9A5502D42EE9ED5C1 (void);
// 0x00000046 System.Object NVBoids/<BehavioralChange>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBehavioralChangeU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60025393D50350AC0B0455728682E136EBF86993 (void);
// 0x00000047 System.Void NVBoids/<BehavioralChange>d__47::System.Collections.IEnumerator.Reset()
extern void U3CBehavioralChangeU3Ed__47_System_Collections_IEnumerator_Reset_m62FD827919A6E92EFCD77E55A19B38508687472C (void);
// 0x00000048 System.Object NVBoids/<BehavioralChange>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CBehavioralChangeU3Ed__47_System_Collections_IEnumerator_get_Current_m2C6C4D45FF21DEDC04DB0EEEA8F3F547306A4F0E (void);
// 0x00000049 System.Void QuirkySeriesDemo::Start()
extern void QuirkySeriesDemo_Start_m747A4AC49D3CAFEC9C1103AB871870474E14271B (void);
// 0x0000004A System.Void QuirkySeriesDemo::Update()
extern void QuirkySeriesDemo_Update_mD02D19C041CC46536540BB8494375E3995F8D282 (void);
// 0x0000004B System.Void QuirkySeriesDemo::NextAnimal()
extern void QuirkySeriesDemo_NextAnimal_m10DF76EF2E976EF99487454A585076911C9FECF3 (void);
// 0x0000004C System.Void QuirkySeriesDemo::PrevAnimal()
extern void QuirkySeriesDemo_PrevAnimal_m190A48284BE754E311E8E9868B7F3F4B31D75FE4 (void);
// 0x0000004D System.Void QuirkySeriesDemo::ChangeAnimal()
extern void QuirkySeriesDemo_ChangeAnimal_m5B17A23EEBC0FB89F0787BCC063E3705D8BA6166 (void);
// 0x0000004E System.Void QuirkySeriesDemo::NextAnimation()
extern void QuirkySeriesDemo_NextAnimation_mC8627130D4B4A66CF187EE140FD76536F34EFFD4 (void);
// 0x0000004F System.Void QuirkySeriesDemo::PrevAnimation()
extern void QuirkySeriesDemo_PrevAnimation_mB71B2F4A9D6BBA49098F2A38FDCDDC50DD00799E (void);
// 0x00000050 System.Void QuirkySeriesDemo::ChangeAnimation()
extern void QuirkySeriesDemo_ChangeAnimation_m4C082A9C5E2C5D2DADEDC3FC5B287E32795FF80F (void);
// 0x00000051 System.Void QuirkySeriesDemo::NextShapekey()
extern void QuirkySeriesDemo_NextShapekey_m57CD6F4C160E1ECE7F636DF61CC338099C233A49 (void);
// 0x00000052 System.Void QuirkySeriesDemo::PrevShapekey()
extern void QuirkySeriesDemo_PrevShapekey_m57419B1380DFC1850AF8A4D0844FE4529BE9336F (void);
// 0x00000053 System.Void QuirkySeriesDemo::ChangeShapekey()
extern void QuirkySeriesDemo_ChangeShapekey_m97D4F6B5F91F408A66FCB7FFD2DE74EA15117ABF (void);
// 0x00000054 System.Void QuirkySeriesDemo::GoToWebsite(System.String)
extern void QuirkySeriesDemo_GoToWebsite_m496EFD0D64E397CD015EC4122EF7D778D58F0CE1 (void);
// 0x00000055 System.Void QuirkySeriesDemo::.ctor()
extern void QuirkySeriesDemo__ctor_m0708F2164E1C1F2AB81B0BA14F9085A1E73C6599 (void);
// 0x00000056 System.Void RotateOnScroll::Update()
extern void RotateOnScroll_Update_mE46306C0A627A240A57F576907537CE452AB7E28 (void);
// 0x00000057 System.Void RotateOnScroll::.ctor()
extern void RotateOnScroll__ctor_mAC7C1AD78086BD0DDEDF115BBBC008C544CF3B47 (void);
// 0x00000058 System.Void AI_Movement::Start()
extern void AI_Movement_Start_m92572122060FC96E299085951E34A0D66D97A9D7 (void);
// 0x00000059 System.Void AI_Movement::Update()
extern void AI_Movement_Update_mE02A5C38F41C6D1A4406404D5452257E758DDE7F (void);
// 0x0000005A System.Void AI_Movement::ChooseDirection()
extern void AI_Movement_ChooseDirection_mB34608D1D225C4A0077E25F67E49463FE8AE3331 (void);
// 0x0000005B System.Void AI_Movement::.ctor()
extern void AI_Movement__ctor_mDD9BFBD7B4A7353BE29D02C3B224CC9DD452F1E7 (void);
// 0x0000005C System.Void Blueprint::.ctor(System.String,System.Int32,System.Int32,System.String,System.Int32,System.String,System.Int32)
extern void Blueprint__ctor_m29DF651A6C8C9B2B76BC5062C7FBF881CBD82007 (void);
// 0x0000005D System.Void Constructable::Start()
extern void Constructable_Start_m6E364968F6A936DAD69EF5E1511F4B8A08395C4E (void);
// 0x0000005E System.Void Constructable::Update()
extern void Constructable_Update_mF9B64AD64C14660972FF9569DFA7C64358D90383 (void);
// 0x0000005F System.Void Constructable::OnTriggerEnter(UnityEngine.Collider)
extern void Constructable_OnTriggerEnter_m08203DCC67338E1FB4033EB266F3A00FE98909C1 (void);
// 0x00000060 System.Void Constructable::OnTriggerExit(UnityEngine.Collider)
extern void Constructable_OnTriggerExit_m3681F8B59E5BA5ADD3B6EF53775DCF311549DFA0 (void);
// 0x00000061 System.Void Constructable::SetInvalidColor()
extern void Constructable_SetInvalidColor_m6D1D4172572AF42C322DFB78C947C489BBDEADE6 (void);
// 0x00000062 System.Void Constructable::SetValidColor()
extern void Constructable_SetValidColor_m47D0B59FAE464206DE0CE22AC4547452A000ECF3 (void);
// 0x00000063 System.Void Constructable::SetDefaultColor()
extern void Constructable_SetDefaultColor_m952BDAE0373EBC8DF8D5DB352130B6509A066854 (void);
// 0x00000064 System.Void Constructable::ExtractGhostMembers()
extern void Constructable_ExtractGhostMembers_m6E83619A377A9C76BEC947A0B9FA5B71D127F0F2 (void);
// 0x00000065 System.Void Constructable::.ctor()
extern void Constructable__ctor_m0519ABA7B9B748A69C6D17C15E3AFB2DACE8EEEB (void);
// 0x00000066 ConstructionManager ConstructionManager::get_Instance()
extern void ConstructionManager_get_Instance_mA6F04AA889A753B3E875CA6FB446F45C7BE48B75 (void);
// 0x00000067 System.Void ConstructionManager::set_Instance(ConstructionManager)
extern void ConstructionManager_set_Instance_mC6928E040A34938A8274E41DD98280819A178CA0 (void);
// 0x00000068 System.Void ConstructionManager::Awake()
extern void ConstructionManager_Awake_mB486DDBF1FB096511ACBBD07144FF2982ECCF072 (void);
// 0x00000069 System.Void ConstructionManager::ActivateConstructionPlacement(System.String)
extern void ConstructionManager_ActivateConstructionPlacement_m8A31E747C1AC48F1C2DA582A3A42C6A9F1EB81E4 (void);
// 0x0000006A System.Void ConstructionManager::GetAllGhosts(UnityEngine.GameObject)
extern void ConstructionManager_GetAllGhosts_m180E56CD36DF8A21C9F7BF66177888EBD9E2A974 (void);
// 0x0000006B System.Void ConstructionManager::PerformGhostDeletionScan()
extern void ConstructionManager_PerformGhostDeletionScan_m3D5FD613F1F298F16EE5B7D361FFE45FF2B751AF (void);
// 0x0000006C System.Single ConstructionManager::XPositionToAccurateFloat(UnityEngine.GameObject)
extern void ConstructionManager_XPositionToAccurateFloat_m9B8B38C899B00FA9F684AEAF2CBFB1DFA9840780 (void);
// 0x0000006D System.Single ConstructionManager::ZPositionToAccurateFloat(UnityEngine.GameObject)
extern void ConstructionManager_ZPositionToAccurateFloat_m29659109F4BD500F73A172BED690F1C201CDB610 (void);
// 0x0000006E System.Void ConstructionManager::Update()
extern void ConstructionManager_Update_mEED81B962801098D4388F50FB89F48D172D1C515 (void);
// 0x0000006F System.Void ConstructionManager::PlaceItemInGhostPosition(UnityEngine.GameObject)
extern void ConstructionManager_PlaceItemInGhostPosition_m31A30A04E5297450BB3FDF28260C9620F5D0BE1B (void);
// 0x00000070 System.Void ConstructionManager::PlaceItemFreeStyle()
extern void ConstructionManager_PlaceItemFreeStyle_m37CC14D0862C1741DE5BD2D4A5A59237FDDA9698 (void);
// 0x00000071 System.Void ConstructionManager::DestroyItem(UnityEngine.GameObject)
extern void ConstructionManager_DestroyItem_mC2CEB497CFA70F12C6FAC81F6FD2276A87359E76 (void);
// 0x00000072 System.Boolean ConstructionManager::CheckValidConstructionPosition()
extern void ConstructionManager_CheckValidConstructionPosition_m48CC28362CBCFF8A893C9C5F1B02718D5C019CF6 (void);
// 0x00000073 System.Void ConstructionManager::.ctor()
extern void ConstructionManager__ctor_m30D1B98B383CC7D5C54F1A8C37E5F45F2BAB6BE6 (void);
// 0x00000074 System.Void GhostItem::Start()
extern void GhostItem_Start_mCFDC822ED48463498392286878682FC15A59CF3F (void);
// 0x00000075 System.Void GhostItem::Update()
extern void GhostItem_Update_mE0B362B6E8CBD2D0186AB27338D1AC365EEE20A8 (void);
// 0x00000076 System.Void GhostItem::.ctor()
extern void GhostItem__ctor_m3659200A968AC3F0E1C5983C63F5F703EF1ECC83 (void);
// 0x00000077 System.Void CaloriesBar::Awake()
extern void CaloriesBar_Awake_mB05DF410AACD70A46317E05A01EF6AD89662C488 (void);
// 0x00000078 System.Void CaloriesBar::Update()
extern void CaloriesBar_Update_m6AF7EF7EDA36D5AB669BC3A6AD1E38B63317A285 (void);
// 0x00000079 System.Void CaloriesBar::.ctor()
extern void CaloriesBar__ctor_m6EB7120EE1F5797B161392E31E5D57C9825BE4E1 (void);
// 0x0000007A System.Void ChoppableTree::Start()
extern void ChoppableTree_Start_m0065824BADB628018FBB6AE9EE93E0312B857C92 (void);
// 0x0000007B System.Void ChoppableTree::OnTriggerEnter(UnityEngine.Collider)
extern void ChoppableTree_OnTriggerEnter_m843F92C2C5FF87630817841E6C26655672DAA4DE (void);
// 0x0000007C System.Void ChoppableTree::OnTriggerExit(UnityEngine.Collider)
extern void ChoppableTree_OnTriggerExit_m47C18343B31F8AD02504D9CE1B4A06DAC8C3D22C (void);
// 0x0000007D System.Void ChoppableTree::GetHit()
extern void ChoppableTree_GetHit_m2CE97ADD642FA6B4BEC6C365EAE3FF72DAEED5D2 (void);
// 0x0000007E System.Void ChoppableTree::treeIsDead()
extern void ChoppableTree_treeIsDead_mAB1BB216708E4C1CE351DF396A772BB6727E7728 (void);
// 0x0000007F System.Void ChoppableTree::Update()
extern void ChoppableTree_Update_m650A6A56603AB2869A27ACDA6D936710E4EEAA9C (void);
// 0x00000080 System.Void ChoppableTree::.ctor()
extern void ChoppableTree__ctor_mCA8BD881BD4664BB39130C8CE29933A699E626D3 (void);
// 0x00000081 System.Void CircleMovement::Start()
extern void CircleMovement_Start_m21C05D9867F846187CC736CE81FEF598C1CE931A (void);
// 0x00000082 System.Void CircleMovement::Update()
extern void CircleMovement_Update_mB2D3E0502EC5A14FE7DA64188E6B1B2BD66CA628 (void);
// 0x00000083 System.Void CircleMovement::.ctor()
extern void CircleMovement__ctor_m1B8FE3DE581E0F20F6C4F8B44F9F7C2329C18F86 (void);
// 0x00000084 System.Void ControllMenu::choiMoi()
extern void ControllMenu_choiMoi_mC702996169C127BE46EEC1D872A6D425CCC3E8D3 (void);
// 0x00000085 System.Void ControllMenu::Quit()
extern void ControllMenu_Quit_mBCCDBC8B3FB744371445B79A27383B6C8350A51B (void);
// 0x00000086 System.Void ControllMenu::thoat()
extern void ControllMenu_thoat_m3FC424C83CD50B856B331B031817C50687E22249 (void);
// 0x00000087 System.Void ControllMenu::.ctor()
extern void ControllMenu__ctor_mAF8358B5CC64C36C311945EE1844DF097E5CA0C6 (void);
// 0x00000088 System.Void ControllMenu1::choiMoi()
extern void ControllMenu1_choiMoi_m12D676A33E6F0A3E7BDEC9A2F24C652C81291D46 (void);
// 0x00000089 System.Void ControllMenu1::thoat()
extern void ControllMenu1_thoat_mB3E3B3FA88EFAA5468083A1FFD2754E4B90F9665 (void);
// 0x0000008A System.Void ControllMenu1::toMainMenu()
extern void ControllMenu1_toMainMenu_m2EB85A26C37882509092374CDFCAF1E4EC666CBF (void);
// 0x0000008B System.Void ControllMenu1::toSettingMenu()
extern void ControllMenu1_toSettingMenu_m638CB156AFA8269C62270678812794865E155B6A (void);
// 0x0000008C System.Void ControllMenu1::.ctor()
extern void ControllMenu1__ctor_mCB999EDAD568A09C730BC248915FAD37DB058F99 (void);
// 0x0000008D CraftingSystem CraftingSystem::get_instance()
extern void CraftingSystem_get_instance_m9593F248B3D6EEA7B80AA3B7FCF876AA44E846F2 (void);
// 0x0000008E System.Void CraftingSystem::set_instance(CraftingSystem)
extern void CraftingSystem_set_instance_mD77417C842A63FD64BDC864EA8717EE7D059A3B2 (void);
// 0x0000008F System.Void CraftingSystem::Awake()
extern void CraftingSystem_Awake_m7A892F1440082227DB7AD26A88BEB9CA4C1C12CB (void);
// 0x00000090 System.Void CraftingSystem::Start()
extern void CraftingSystem_Start_m9D06C83690A9ED5A82EA79B88ECBA1A79C99CABA (void);
// 0x00000091 System.Void CraftingSystem::OpenToolsCategory()
extern void CraftingSystem_OpenToolsCategory_mE382B9D843F9B46E2DD1B30441CE007A94A78A47 (void);
// 0x00000092 System.Void CraftingSystem::OpenSurvivalCategory()
extern void CraftingSystem_OpenSurvivalCategory_m85234E7A002A5F916A812B1C973D18FEE0F805CE (void);
// 0x00000093 System.Void CraftingSystem::OpenRefineCategory()
extern void CraftingSystem_OpenRefineCategory_m897DFFFA3302DEE0755A3D70EC5FFCEEACB8DC2A (void);
// 0x00000094 System.Void CraftingSystem::OpenConstructionCategory()
extern void CraftingSystem_OpenConstructionCategory_mDE1646DAB01A8A0155AC2FD34F413B9525F66CC4 (void);
// 0x00000095 System.Void CraftingSystem::CraftAnyItem(Blueprint)
extern void CraftingSystem_CraftAnyItem_mDCD0FB86F06AE60AC0F49456C1F2351A6D33AAAC (void);
// 0x00000096 System.Collections.IEnumerator CraftingSystem::calculate()
extern void CraftingSystem_calculate_m8355A04BF802E2D458848E9D4930D59EB3A1F72A (void);
// 0x00000097 System.Collections.IEnumerator CraftingSystem::craftedDelayForSound(Blueprint)
extern void CraftingSystem_craftedDelayForSound_m6647CCCB27E318BC95745DDF6E798BAB77F6143B (void);
// 0x00000098 System.Void CraftingSystem::Update()
extern void CraftingSystem_Update_m87A363596B4EDD089E0ACC9324B22E5B32C03C1B (void);
// 0x00000099 System.Void CraftingSystem::onCraft()
extern void CraftingSystem_onCraft_m51E317055A0C863CC45EF2B1FD7BF4982864A08B (void);
// 0x0000009A System.Void CraftingSystem::RefreshNeededItems()
extern void CraftingSystem_RefreshNeededItems_m903B8F8D973A9B5525A249555DD334BEAD91723F (void);
// 0x0000009B System.Void CraftingSystem::.ctor()
extern void CraftingSystem__ctor_mA635F5340DEFD3ED0C726058910CA4FB84233207 (void);
// 0x0000009C System.Void CraftingSystem::<Start>b__29_0()
extern void CraftingSystem_U3CStartU3Eb__29_0_m54A552C21F4F3C53D30B0E002F90A40AAAA65A56 (void);
// 0x0000009D System.Void CraftingSystem::<Start>b__29_1()
extern void CraftingSystem_U3CStartU3Eb__29_1_m5AE907AD87DAA27A2A8E4FE171144FC52ABF4118 (void);
// 0x0000009E System.Void CraftingSystem::<Start>b__29_2()
extern void CraftingSystem_U3CStartU3Eb__29_2_mCC1B78F48EFB0FAB9B1C06B4543604C95F37CEE5 (void);
// 0x0000009F System.Void CraftingSystem::<Start>b__29_3()
extern void CraftingSystem_U3CStartU3Eb__29_3_m20A89377360F42872B48F0F49AA1722907A4F047 (void);
// 0x000000A0 System.Void CraftingSystem::<Start>b__29_4()
extern void CraftingSystem_U3CStartU3Eb__29_4_mA4A8452F855FC2DCD97139CC89379F1A851AC102 (void);
// 0x000000A1 System.Void CraftingSystem::<Start>b__29_5()
extern void CraftingSystem_U3CStartU3Eb__29_5_m4901D723F18AB4CBE961BB55FD280A4C7149A275 (void);
// 0x000000A2 System.Void CraftingSystem::<Start>b__29_6()
extern void CraftingSystem_U3CStartU3Eb__29_6_mD9BE6093408D4AD3473E9497437981DED44DDE30 (void);
// 0x000000A3 System.Void CraftingSystem::<Start>b__29_7()
extern void CraftingSystem_U3CStartU3Eb__29_7_m6EE2D9A7C9214E6FB6BD3769942A88C55FDA499F (void);
// 0x000000A4 System.Void CraftingSystem/<calculate>d__35::.ctor(System.Int32)
extern void U3CcalculateU3Ed__35__ctor_m64A8ADEC6B7CACC99228C5CCF90D7CD614B44F2A (void);
// 0x000000A5 System.Void CraftingSystem/<calculate>d__35::System.IDisposable.Dispose()
extern void U3CcalculateU3Ed__35_System_IDisposable_Dispose_mC468F7AB616EEF078F53697549522E274E78AA70 (void);
// 0x000000A6 System.Boolean CraftingSystem/<calculate>d__35::MoveNext()
extern void U3CcalculateU3Ed__35_MoveNext_m477892FD58209B35885AE52AEF2FECB611F476A7 (void);
// 0x000000A7 System.Object CraftingSystem/<calculate>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcalculateU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m83EECEE35820BA91456B47775E9EBAA669FC21C0 (void);
// 0x000000A8 System.Void CraftingSystem/<calculate>d__35::System.Collections.IEnumerator.Reset()
extern void U3CcalculateU3Ed__35_System_Collections_IEnumerator_Reset_mB2D7CC0AB280B1119F91C70A60AFED37AF6FE30C (void);
// 0x000000A9 System.Object CraftingSystem/<calculate>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CcalculateU3Ed__35_System_Collections_IEnumerator_get_Current_mD31488D38404FFA813FD57A40D387AD8388DEC36 (void);
// 0x000000AA System.Void CraftingSystem/<craftedDelayForSound>d__36::.ctor(System.Int32)
extern void U3CcraftedDelayForSoundU3Ed__36__ctor_mE036DE455AFA1540452EDD0F135F6E09C3385B0A (void);
// 0x000000AB System.Void CraftingSystem/<craftedDelayForSound>d__36::System.IDisposable.Dispose()
extern void U3CcraftedDelayForSoundU3Ed__36_System_IDisposable_Dispose_mFD0DEF72FD0E913C76D7BBA680BC5A29BB36ECD1 (void);
// 0x000000AC System.Boolean CraftingSystem/<craftedDelayForSound>d__36::MoveNext()
extern void U3CcraftedDelayForSoundU3Ed__36_MoveNext_mE751BECCD30AC4D8CB2F65EA40EAF07DB9E92990 (void);
// 0x000000AD System.Object CraftingSystem/<craftedDelayForSound>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcraftedDelayForSoundU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m980A3F556F00B73336E1A73C1869AE3FFE06A800 (void);
// 0x000000AE System.Void CraftingSystem/<craftedDelayForSound>d__36::System.Collections.IEnumerator.Reset()
extern void U3CcraftedDelayForSoundU3Ed__36_System_Collections_IEnumerator_Reset_m43430CA9041C4E783A6B4373C67131E546E18C8F (void);
// 0x000000AF System.Object CraftingSystem/<craftedDelayForSound>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CcraftedDelayForSoundU3Ed__36_System_Collections_IEnumerator_get_Current_mE4F12D5040BEDC69C2D31995C0C6213A8923AB7F (void);
// 0x000000B0 System.Void DayNightCycle::Start()
extern void DayNightCycle_Start_mEBC69829A9266BB95FA3439A12DD170B6CCA4EFC (void);
// 0x000000B1 System.Void DayNightCycle::Update()
extern void DayNightCycle_Update_m93263D9CE1D7218754CD5AB4ABF1FB8D63C8ECC5 (void);
// 0x000000B2 System.Void DayNightCycle::.ctor()
extern void DayNightCycle__ctor_mA2B6C9B178964D2867D717FCDCD1A4539370C38E (void);
// 0x000000B3 System.Void DragDrop::Awake()
extern void DragDrop_Awake_m3D9656016C42E05B0F64F12CCBE69AF06E40BBB4 (void);
// 0x000000B4 System.Void DragDrop::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnBeginDrag_m7969B96669116003DCA318A99818A1215DF6AC2C (void);
// 0x000000B5 System.Void DragDrop::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnDrag_m66843E500FB5601B0C526881781E743DACA78302 (void);
// 0x000000B6 System.Void DragDrop::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnEndDrag_m30CB848DAE1C7CF2912120A49C20B5617E76C448 (void);
// 0x000000B7 System.Void DragDrop::.ctor()
extern void DragDrop__ctor_mE5A2331828F64E1B46DA7C6AC3CC64A1D6721707 (void);
// 0x000000B8 System.Void EquipableItem::Start()
extern void EquipableItem_Start_m990822285B4BDC81B1F765DE8ED87260D607C0BD (void);
// 0x000000B9 System.Void EquipableItem::Update()
extern void EquipableItem_Update_mFD9342585CF35EF348C99A5C85E0A1F364E73759 (void);
// 0x000000BA System.Void EquipableItem::GetHit()
extern void EquipableItem_GetHit_mD8D433D14E14347E0511199E7F4A068DED941C41 (void);
// 0x000000BB System.Collections.IEnumerator EquipableItem::SwingSoundDelay()
extern void EquipableItem_SwingSoundDelay_mCF8EDF69F17B6878F21F953EB624902CF4F4D9F9 (void);
// 0x000000BC System.Void EquipableItem::.ctor()
extern void EquipableItem__ctor_m381DFC1B7500595871E06E6685577B5741BDDEAD (void);
// 0x000000BD System.Void EquipableItem/<SwingSoundDelay>d__4::.ctor(System.Int32)
extern void U3CSwingSoundDelayU3Ed__4__ctor_m5B281461312D8735C15679B0A49BAC39B963DDEB (void);
// 0x000000BE System.Void EquipableItem/<SwingSoundDelay>d__4::System.IDisposable.Dispose()
extern void U3CSwingSoundDelayU3Ed__4_System_IDisposable_Dispose_m7108440ED3D2DA311CC7EA2D76CD9792B0EB9CD2 (void);
// 0x000000BF System.Boolean EquipableItem/<SwingSoundDelay>d__4::MoveNext()
extern void U3CSwingSoundDelayU3Ed__4_MoveNext_mEDC318F55BCB9A37CCF133570CCF28B2DD1B168B (void);
// 0x000000C0 System.Object EquipableItem/<SwingSoundDelay>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSwingSoundDelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8D121DF3DA9B779DA2FD74F8BBB0E20805124A7 (void);
// 0x000000C1 System.Void EquipableItem/<SwingSoundDelay>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSwingSoundDelayU3Ed__4_System_Collections_IEnumerator_Reset_m2CDAC5D0BAB7D9B1BA4C63311DA146E588D815ED (void);
// 0x000000C2 System.Object EquipableItem/<SwingSoundDelay>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSwingSoundDelayU3Ed__4_System_Collections_IEnumerator_get_Current_m4B1E6271EA1CC237066A24DBAFA264CED342987C (void);
// 0x000000C3 EquipSystem EquipSystem::get_Instance()
extern void EquipSystem_get_Instance_mE74148BB6496B9C7D7340B2F6229EBCB660DB75A (void);
// 0x000000C4 System.Void EquipSystem::set_Instance(EquipSystem)
extern void EquipSystem_set_Instance_m4756034B115B194CFFEAE37A7EBDDAAB3E1C385F (void);
// 0x000000C5 System.Void EquipSystem::Awake()
extern void EquipSystem_Awake_m0728040A99A2E0C6ED340AFA12625467A1895D3E (void);
// 0x000000C6 System.Void EquipSystem::Start()
extern void EquipSystem_Start_m3E2B6440FF0574A4F435325CA085E78E249D2530 (void);
// 0x000000C7 System.Void EquipSystem::Update()
extern void EquipSystem_Update_m674AFF858E7981FFCBC18144631BC28E524BA9C3 (void);
// 0x000000C8 System.Void EquipSystem::SelectQuickSlot(System.Int32)
extern void EquipSystem_SelectQuickSlot_mDEAC5DDDCCEB4BA2971007BB70F28A0E6223CA75 (void);
// 0x000000C9 System.Void EquipSystem::SetEquippedModel(UnityEngine.GameObject)
extern void EquipSystem_SetEquippedModel_m319884207FB0AF920B561B5BA7BF0A80F4D0AF77 (void);
// 0x000000CA UnityEngine.GameObject EquipSystem::getSelectedItem(System.Int32)
extern void EquipSystem_getSelectedItem_m3A23A463D83A3E25FEC0F10B95E197BD4A9AF104 (void);
// 0x000000CB System.Boolean EquipSystem::checkIfSlotIsFull(System.Int32)
extern void EquipSystem_checkIfSlotIsFull_m04EEF5549FA7CDA3DB31C7AACBA91EA0688C9E9E (void);
// 0x000000CC System.Void EquipSystem::PopulateSlotList()
extern void EquipSystem_PopulateSlotList_mAF977CB7E0E8810BCBBE2B4D03821C74A7646B01 (void);
// 0x000000CD System.Void EquipSystem::AddToQuickSlots(UnityEngine.GameObject)
extern void EquipSystem_AddToQuickSlots_m20B57EA918A4B5AE2C19371EF5C37BA2EB49DFFE (void);
// 0x000000CE UnityEngine.GameObject EquipSystem::FindNextEmptySlot()
extern void EquipSystem_FindNextEmptySlot_m6C1C16DABCD15E3B8028DA753D074BDEBA648701 (void);
// 0x000000CF System.Boolean EquipSystem::CheckIfFull()
extern void EquipSystem_CheckIfFull_m1E50C1D9CDD9736C0642BC3617607E4FE0AC566D (void);
// 0x000000D0 System.Void EquipSystem::.ctor()
extern void EquipSystem__ctor_m70E1419644CB102B3047E7CFD6FD63FFE2F23D8C (void);
// 0x000000D1 GlobalState GlobalState::get_Instance()
extern void GlobalState_get_Instance_mC0D9E0D6620185B77A070D8E71BCB253700CCEB7 (void);
// 0x000000D2 System.Void GlobalState::set_Instance(GlobalState)
extern void GlobalState_set_Instance_mF9918E2BF57F8D8FC2BFEF8A78F9E36A588DBB15 (void);
// 0x000000D3 System.Void GlobalState::Awake()
extern void GlobalState_Awake_m00C055E230885B69230D401842BEC2F925AEDD0E (void);
// 0x000000D4 System.Void GlobalState::.ctor()
extern void GlobalState__ctor_m6A35850A8D251697ED58A2F94812DDE0E86813F5 (void);
// 0x000000D5 System.Void HealthBar::Awake()
extern void HealthBar_Awake_mD335445D153349F1E440D2FAAA2AC3CEC8803ED2 (void);
// 0x000000D6 System.Void HealthBar::Update()
extern void HealthBar_Update_mD3754EFEE710D376C4EEAA8D71E90B456C5E9AFD (void);
// 0x000000D7 System.Void HealthBar::.ctor()
extern void HealthBar__ctor_m6874A2796BC8D86E80B24E349500653ACFA80662 (void);
// 0x000000D8 System.Void HydrationBar::Awake()
extern void HydrationBar_Awake_mB053E895C4B0A107AD05B84EDBE74678323A29BD (void);
// 0x000000D9 System.Void HydrationBar::Update()
extern void HydrationBar_Update_mE3D614F7D8FB416D7586640D2604BB1C630C5025 (void);
// 0x000000DA System.Void HydrationBar::.ctor()
extern void HydrationBar__ctor_mF3F95E14303BE4749559E26BAE5F0B0C721E3141 (void);
// 0x000000DB System.String InteractableObject::GetItemName()
extern void InteractableObject_GetItemName_m9D1EEC4B43093BA55C86CEEA17D7517A47DC2BD9 (void);
// 0x000000DC System.Void InteractableObject::Update()
extern void InteractableObject_Update_mC1B0F9850D186FFF22389540655B9356F0B246AE (void);
// 0x000000DD System.Void InteractableObject::OnTriggerEnter(UnityEngine.Collider)
extern void InteractableObject_OnTriggerEnter_m51A41E9533CC7BE7E1C632FD61A3F6671028175D (void);
// 0x000000DE System.Void InteractableObject::OnTriggerExit(UnityEngine.Collider)
extern void InteractableObject_OnTriggerExit_mF0831C00D61A47FD1B23AB3E6FF6227A8ECE760C (void);
// 0x000000DF System.Void InteractableObject::.ctor()
extern void InteractableObject__ctor_m2349D95BF7851F97EB4CCD417B9C97041AED0A17 (void);
// 0x000000E0 System.Void IntroGame::Start()
extern void IntroGame_Start_mD21A1781443897F84ACBB3A1FD0A62A92AAFDA68 (void);
// 0x000000E1 System.Void IntroGame::Update()
extern void IntroGame_Update_m9C6D5C4B80C4B7AAD8429309FFD572D177EBAB39 (void);
// 0x000000E2 System.Void IntroGame::popupIntroA()
extern void IntroGame_popupIntroA_m4F3C899060141B29470884B9E802439D56019612 (void);
// 0x000000E3 System.Void IntroGame::popupIntroB()
extern void IntroGame_popupIntroB_m916F73F41E3A4063A827CA9A1365612E9E0A2E85 (void);
// 0x000000E4 System.Void IntroGame::popupIntroC()
extern void IntroGame_popupIntroC_m13102B4CBF3B67FF3FAB4BD946795F737F393D10 (void);
// 0x000000E5 System.Void IntroGame::popupIntroD()
extern void IntroGame_popupIntroD_m6F4DA272B0409767A03141F05AC6D15EF1EB4D2B (void);
// 0x000000E6 System.Void IntroGame::popupIntroE()
extern void IntroGame_popupIntroE_m2B5A1BED228D0B07A367E6B4BBB86E64610E4B9A (void);
// 0x000000E7 System.Void IntroGame::popupIntroF()
extern void IntroGame_popupIntroF_m6F27C1E0FE0CF6BF0BD098A72BB37461B716C050 (void);
// 0x000000E8 System.Void IntroGame::quitIntro()
extern void IntroGame_quitIntro_m99FF0D478FB443DF6D55190F0F6240A31D77F7E7 (void);
// 0x000000E9 System.Void IntroGame::.ctor()
extern void IntroGame__ctor_mD13DC8E8EEF690D18715893EE039F0B1D902F35F (void);
// 0x000000EA System.Void InventoryItem::Start()
extern void InventoryItem_Start_mB9C5506703D7C324664AAE562DE4FF0B544E7E3F (void);
// 0x000000EB System.Void InventoryItem::Update()
extern void InventoryItem_Update_m32C5FB14B4BB3A88AD3F5EC922A7FBAAA0403F51 (void);
// 0x000000EC System.Void InventoryItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void InventoryItem_OnPointerEnter_m6BD43353CB537D89464DD294E96D2712ECBC69FE (void);
// 0x000000ED System.Void InventoryItem::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void InventoryItem_OnPointerExit_m62157E08E9C5D02CB9BCD7D48C61622F71EA0172 (void);
// 0x000000EE System.Void InventoryItem::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void InventoryItem_OnPointerDown_mD86D1152600ADBED0AE1E6ECCB339D0A20984CDB (void);
// 0x000000EF System.Void InventoryItem::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void InventoryItem_OnPointerUp_m8ADF1669DC139D774B429E1FE41BA82BA124893C (void);
// 0x000000F0 System.Void InventoryItem::UseItem()
extern void InventoryItem_UseItem_m7DB0BBBC78DCCBAA1E6DF2F29B2008632B499832 (void);
// 0x000000F1 System.Void InventoryItem::consumingFunction(System.Single,System.Single,System.Single)
extern void InventoryItem_consumingFunction_mC2B9102BD4FBC5BFBB3FEFB58B62FA1760633BC8 (void);
// 0x000000F2 System.Void InventoryItem::healthEffectCalculation(System.Single)
extern void InventoryItem_healthEffectCalculation_m7C064A45674AADB84650CA5252C45B225B59FBD2 (void);
// 0x000000F3 System.Void InventoryItem::caloriesEffectCalculation(System.Single)
extern void InventoryItem_caloriesEffectCalculation_m565D1D4BD5BB1453FEB572BC37667EE2192C0640 (void);
// 0x000000F4 System.Void InventoryItem::hydrationEffectCalculation(System.Single)
extern void InventoryItem_hydrationEffectCalculation_m35C04D6692AB4966A24F46016DE738AE6B2BC334 (void);
// 0x000000F5 System.Void InventoryItem::.ctor()
extern void InventoryItem__ctor_m775807C9917B41C904F5B88DD55F6771AC7A8AB5 (void);
// 0x000000F6 InventorySystem InventorySystem::get_Instance()
extern void InventorySystem_get_Instance_mB6754F78EDB5BD12D53AF447D7619A2F4F95EB3F (void);
// 0x000000F7 System.Void InventorySystem::set_Instance(InventorySystem)
extern void InventorySystem_set_Instance_m21E25C52FC950C0920735724DDA41BFB70DD582F (void);
// 0x000000F8 System.Void InventorySystem::Awake()
extern void InventorySystem_Awake_mB6C33C30DC735179B928B792EB8B8220EAE2C7B3 (void);
// 0x000000F9 System.Void InventorySystem::Start()
extern void InventorySystem_Start_mEF024BBA06D3C3C9762B4E8415BF35CCA6E3B907 (void);
// 0x000000FA System.Void InventorySystem::PopulateSlotList()
extern void InventorySystem_PopulateSlotList_mE30A4F1661DB5554B4572CA932D8EB2EC661C7E1 (void);
// 0x000000FB System.Void InventorySystem::Update()
extern void InventorySystem_Update_m62B5F2641E3DF8D61AF348C70FC1D19157A93DE7 (void);
// 0x000000FC System.Void InventorySystem::onCursor()
extern void InventorySystem_onCursor_mC7A868AA6BCB850B7A6299E3E178CE81BF44D1BD (void);
// 0x000000FD System.Void InventorySystem::offCursor()
extern void InventorySystem_offCursor_m3A2C2B5396D8D1514512490120C1C282E9EA6384 (void);
// 0x000000FE System.Void InventorySystem::onBag()
extern void InventorySystem_onBag_mC7AB36BD26CBCB61335A5915BCF56C14A7A8EB80 (void);
// 0x000000FF System.Void InventorySystem::AddToInventory(System.String)
extern void InventorySystem_AddToInventory_m6DAE590D5699C39DC57FC76EF76D2643363CB7D2 (void);
// 0x00000100 System.Void InventorySystem::TriggerPickupPopUp(System.String,UnityEngine.Sprite)
extern void InventorySystem_TriggerPickupPopUp_m9B8CD2982FEBF9CCD9A01A4765BE311BB3CB5BBE (void);
// 0x00000101 System.Void InventorySystem::TriggerOffPopUp()
extern void InventorySystem_TriggerOffPopUp_mE8C4F716AE8FFB150D77D074B5C168547F3FFD82 (void);
// 0x00000102 System.Void InventorySystem::TriggerIsFullPopup()
extern void InventorySystem_TriggerIsFullPopup_m5F134A42AD461B11A0418F8E8E2CB69DC31314C2 (void);
// 0x00000103 System.Void InventorySystem::TriggerOffIsFullPopup()
extern void InventorySystem_TriggerOffIsFullPopup_mA2ABAADC926FABE42844AF00238F9D3BF475509B (void);
// 0x00000104 System.Boolean InventorySystem::CheckSlotsAvailable(System.Int32)
extern void InventorySystem_CheckSlotsAvailable_mF317C7813A1A783AFC31F2EE3EA3AC18F35294DB (void);
// 0x00000105 UnityEngine.GameObject InventorySystem::FindNextEmptySlot()
extern void InventorySystem_FindNextEmptySlot_mDED79AC1F99B09059DF534344618B475C226790F (void);
// 0x00000106 System.Void InventorySystem::RemoveItem(System.String,System.Int32)
extern void InventorySystem_RemoveItem_m54277B050AFE146336707738BDFAEF55DFB4190B (void);
// 0x00000107 System.Void InventorySystem::ReCalculateList()
extern void InventorySystem_ReCalculateList_mBEC98715D31F7ED4AABA42008A51D0BD710B01A0 (void);
// 0x00000108 System.Void InventorySystem::.ctor()
extern void InventorySystem__ctor_m6941EDD5EB50BD476EB6D26671D3CD7E9C09D976 (void);
// 0x00000109 UnityEngine.GameObject ItemSlot::get_Item()
extern void ItemSlot_get_Item_mBC890648338DC27ADE77C882A4398AFCBF470098 (void);
// 0x0000010A System.Void ItemSlot::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnDrop_mA85B22B894F538E98322BF1A3C3C972F7681FF95 (void);
// 0x0000010B System.Void ItemSlot::.ctor()
extern void ItemSlot__ctor_mAAA0B63EB83F00953BB1E7E67F72C2DC09B60620 (void);
// 0x0000010C System.Void MouseMovement::Start()
extern void MouseMovement_Start_m05EAE2791625108B3410AF53784B704477173D73 (void);
// 0x0000010D System.Void MouseMovement::Update()
extern void MouseMovement_Update_m8AB36A8D7BD05E1D4F3F7DAF92CA32DBFAE7D708 (void);
// 0x0000010E System.Void MouseMovement::.ctor()
extern void MouseMovement__ctor_m0EDD6A5D9AEA3DAAC726E751BEBE97919FA38C00 (void);
// 0x0000010F System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_m83FD44DCA324CE3D05A71FD2E2991FCD743F003A (void);
// 0x00000110 System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_m5BB6CE35AF68EE00CFEB4BA5EBA17E10667551D3 (void);
// 0x00000111 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mB37559C5B0638161878D20E00B7C672FC38BBBAA (void);
// 0x00000112 PlayerState PlayerState::get_Instance()
extern void PlayerState_get_Instance_mC7ADF0C9DB4050A1402E9913BF6F41E575072394 (void);
// 0x00000113 System.Void PlayerState::set_Instance(PlayerState)
extern void PlayerState_set_Instance_m52BDFC6E36A069269C103E53873260AFD84B0163 (void);
// 0x00000114 System.Void PlayerState::Awake()
extern void PlayerState_Awake_m5CE90490C1A2EEFE675F99D2AD643F84408621BF (void);
// 0x00000115 System.Void PlayerState::Start()
extern void PlayerState_Start_mE51FD39D15383B0F4065010AA6AFC9D176CE1768 (void);
// 0x00000116 System.Collections.IEnumerator PlayerState::decreaseHydration()
extern void PlayerState_decreaseHydration_m53B04A26BA2E20A54A38F9DACCBF678F68EA0D10 (void);
// 0x00000117 System.Void PlayerState::Update()
extern void PlayerState_Update_m0877F4235CA36008E62785240D1824BF4EB00636 (void);
// 0x00000118 System.Void PlayerState::setHealth(System.Single)
extern void PlayerState_setHealth_m13E312FCAE519FB567130C0F626B541539AA1626 (void);
// 0x00000119 System.Void PlayerState::setCalories(System.Single)
extern void PlayerState_setCalories_mA8819A01F0F11E404139D67DE60C5FD66A7E1296 (void);
// 0x0000011A System.Void PlayerState::setHydration(System.Single)
extern void PlayerState_setHydration_m127DF1E18A14832587ECAB3CE6CC11C014D526A6 (void);
// 0x0000011B System.Void PlayerState::.ctor()
extern void PlayerState__ctor_m3D86839408DEC12A9FBDF17CB95C7CE18CCA8190 (void);
// 0x0000011C System.Void PlayerState/<decreaseHydration>d__16::.ctor(System.Int32)
extern void U3CdecreaseHydrationU3Ed__16__ctor_m312D32504E6C2C8784EDE06756534AA4262CBF89 (void);
// 0x0000011D System.Void PlayerState/<decreaseHydration>d__16::System.IDisposable.Dispose()
extern void U3CdecreaseHydrationU3Ed__16_System_IDisposable_Dispose_m781C63AF4758FBF1A71AF67D9C6D11410CF500AE (void);
// 0x0000011E System.Boolean PlayerState/<decreaseHydration>d__16::MoveNext()
extern void U3CdecreaseHydrationU3Ed__16_MoveNext_m37738919F6D3967828337D6ED6A118D3BC18D64C (void);
// 0x0000011F System.Object PlayerState/<decreaseHydration>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdecreaseHydrationU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCCCD2E46848BEBDB8C20DC1579C00FEB798235CB (void);
// 0x00000120 System.Void PlayerState/<decreaseHydration>d__16::System.Collections.IEnumerator.Reset()
extern void U3CdecreaseHydrationU3Ed__16_System_Collections_IEnumerator_Reset_m49E7F143F161E82B4B2BC27A5B5D42BE2B205D74 (void);
// 0x00000121 System.Object PlayerState/<decreaseHydration>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CdecreaseHydrationU3Ed__16_System_Collections_IEnumerator_get_Current_m35979710E4F8803810D01BAD352146B01809DAD6 (void);
// 0x00000122 System.Void ResourceHealthBar::Awake()
extern void ResourceHealthBar_Awake_m391500609A352FBAD484BD74F845B45626134048 (void);
// 0x00000123 System.Void ResourceHealthBar::Update()
extern void ResourceHealthBar_Update_mD819E1AD81B63C296CC25F0F59DD741FEE25F182 (void);
// 0x00000124 System.Void ResourceHealthBar::.ctor()
extern void ResourceHealthBar__ctor_m30AF3EBC605792217B4BA0B6E61CA474C141108A (void);
// 0x00000125 SelectionManager SelectionManager::get_Instance()
extern void SelectionManager_get_Instance_mD229898374797B5EFE4028726BF3AB6BCEE6B5F7 (void);
// 0x00000126 System.Void SelectionManager::set_Instance(SelectionManager)
extern void SelectionManager_set_Instance_m15A9E41EFBF7EC08DCF9591C03202D820B2D80BC (void);
// 0x00000127 System.Void SelectionManager::Start()
extern void SelectionManager_Start_m28F2F9098BC42632CA2B43A635A37943D700CC96 (void);
// 0x00000128 System.Void SelectionManager::Awake()
extern void SelectionManager_Awake_mBA351387D1889A767AFD8E6649081BC9EA97899C (void);
// 0x00000129 System.Void SelectionManager::Update()
extern void SelectionManager_Update_mCE1890549CD6485487E7BFDB43460D2BC0DE1404 (void);
// 0x0000012A System.Void SelectionManager::onPause()
extern void SelectionManager_onPause_m7A8FF3DC7EC1A0218E231E1FD45DD0BB74E16D96 (void);
// 0x0000012B System.Void SelectionManager::DisableSelection()
extern void SelectionManager_DisableSelection_m32B7BC50828154CCAC99AFB0AB07E2A90218C5BF (void);
// 0x0000012C System.Void SelectionManager::EnableSelection()
extern void SelectionManager_EnableSelection_m1AB180C14FE3F53D89880D1127B4C0DBEE986595 (void);
// 0x0000012D System.Void SelectionManager::.ctor()
extern void SelectionManager__ctor_mB63C19EF82E0D6901A11D7794718430F86DA8E44 (void);
// 0x0000012E System.Void SkyboxRotator::Update()
extern void SkyboxRotator_Update_mFE56F58DC98F7F18F15998B18FED505A0B82B41A (void);
// 0x0000012F System.Void SkyboxRotator::.ctor()
extern void SkyboxRotator__ctor_mB8D77845ADC494953A08C671652C80B2B49DF111 (void);
// 0x00000130 SoundManager SoundManager::get_Instance()
extern void SoundManager_get_Instance_mE37BC6A788B14F6895B2B50A4E461F8C7895BAEA (void);
// 0x00000131 System.Void SoundManager::set_Instance(SoundManager)
extern void SoundManager_set_Instance_m93AE933900CE8CB03F41EB983F159CCE87BF2207 (void);
// 0x00000132 System.Void SoundManager::Awake()
extern void SoundManager_Awake_mEB5694CE6F2913D14C32C4AF41C936AA76007825 (void);
// 0x00000133 System.Void SoundManager::PlaySound(UnityEngine.AudioSource)
extern void SoundManager_PlaySound_mC6BB918C6F664FF2859AC7D164A9BFD6085E1F95 (void);
// 0x00000134 System.Void SoundManager::.ctor()
extern void SoundManager__ctor_m27816732AF730AF6BEDE4A67ABC9D1A094777213 (void);
// 0x00000135 UnityEngine.GameObject TrashSlot::get_draggedItem()
extern void TrashSlot_get_draggedItem_m9A372F25212685524A375DD151D4268A75C9B092 (void);
// 0x00000136 System.String TrashSlot::get_itemName()
extern void TrashSlot_get_itemName_m6A49AC7C63B767200E42DE85C1922E8D4918CD9D (void);
// 0x00000137 System.Void TrashSlot::Start()
extern void TrashSlot_Start_mEA7AF57899B0B0BFAE0FE51241A906863267ACD5 (void);
// 0x00000138 System.Void TrashSlot::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void TrashSlot_OnDrop_m3EA13F857D82DC3B6091919D2ACFE9B87E3BC924 (void);
// 0x00000139 System.Collections.IEnumerator TrashSlot::notifyBeforeDeletion()
extern void TrashSlot_notifyBeforeDeletion_m379A722E56CBAED4CEBB19BA7E9DD69CD42AB1FC (void);
// 0x0000013A System.Void TrashSlot::CancelDeletion()
extern void TrashSlot_CancelDeletion_mE660D67AF4D0500055A50C5836AE313D13F4249A (void);
// 0x0000013B System.Void TrashSlot::DeleteItem()
extern void TrashSlot_DeleteItem_mD7E735F941D0CB593B98C37F0C629D221965D4C9 (void);
// 0x0000013C System.Void TrashSlot::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TrashSlot_OnPointerEnter_m589F3C355AEF7C7A1F3D0802BB0BFBA966EF9106 (void);
// 0x0000013D System.Void TrashSlot::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TrashSlot_OnPointerExit_m4C04E2B1C72BD1C1AA4F99B723F9FDEBD11AF28E (void);
// 0x0000013E System.Void TrashSlot::.ctor()
extern void TrashSlot__ctor_m2C21D4DAB8DE561A86A2796BCB981B33FD2E22B3 (void);
// 0x0000013F System.Void TrashSlot::<Start>b__12_0()
extern void TrashSlot_U3CStartU3Eb__12_0_mA2453A739F9964EF9DC347CDD5BA985270439C30 (void);
// 0x00000140 System.Void TrashSlot::<Start>b__12_1()
extern void TrashSlot_U3CStartU3Eb__12_1_m4A027CC9528374F12A8841CF5E70719DE6AE23D1 (void);
// 0x00000141 System.Void TrashSlot/<notifyBeforeDeletion>d__14::.ctor(System.Int32)
extern void U3CnotifyBeforeDeletionU3Ed__14__ctor_m4409832B9C192EC4569A0CEF4F613A7E0DBE292F (void);
// 0x00000142 System.Void TrashSlot/<notifyBeforeDeletion>d__14::System.IDisposable.Dispose()
extern void U3CnotifyBeforeDeletionU3Ed__14_System_IDisposable_Dispose_mDE44F645444FA8BD8ED64B66ECD439964FEB4E42 (void);
// 0x00000143 System.Boolean TrashSlot/<notifyBeforeDeletion>d__14::MoveNext()
extern void U3CnotifyBeforeDeletionU3Ed__14_MoveNext_m2265DF53FED029DD851B8681F4EED0EEF5F1A779 (void);
// 0x00000144 System.Object TrashSlot/<notifyBeforeDeletion>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CnotifyBeforeDeletionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDD16BDE03D00ADF3C2F689A45CCC3B02FC906A0 (void);
// 0x00000145 System.Void TrashSlot/<notifyBeforeDeletion>d__14::System.Collections.IEnumerator.Reset()
extern void U3CnotifyBeforeDeletionU3Ed__14_System_Collections_IEnumerator_Reset_m878613246A19363A20697FF465A492D2FC6BE93C (void);
// 0x00000146 System.Object TrashSlot/<notifyBeforeDeletion>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CnotifyBeforeDeletionU3Ed__14_System_Collections_IEnumerator_get_Current_mC27EC02BD35CF3111A130CB54655EC71FA4DA929 (void);
// 0x00000147 System.Void WaterSoundControl::Start()
extern void WaterSoundControl_Start_m871C2B88E8602048D31AF5F81D294F10F9754E17 (void);
// 0x00000148 System.Void WaterSoundControl::Update()
extern void WaterSoundControl_Update_m754E7BDB22D7D34240E723CC741D361D8C17EFDD (void);
// 0x00000149 System.Void WaterSoundControl::OnTriggerEnter(UnityEngine.Collider)
extern void WaterSoundControl_OnTriggerEnter_m5D729D9C9F5A35FE83311932AABA4A0EF835F99F (void);
// 0x0000014A System.Void WaterSoundControl::OnTriggerExit(UnityEngine.Collider)
extern void WaterSoundControl_OnTriggerExit_m98E4E5B93778FDE2E3FB4E485EA9AF4A2B992225 (void);
// 0x0000014B System.Void WaterSoundControl::.ctor()
extern void WaterSoundControl__ctor_m50CA1F0AD967AB96355B25DCE148CA51E111BCCA (void);
// 0x0000014C System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x0000014D System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x0000014E System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722 (void);
// 0x0000014F System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x00000150 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F (void);
// 0x00000151 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B (void);
// 0x00000152 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x00000153 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x00000154 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x00000155 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2 (void);
// 0x00000156 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C (void);
// 0x00000157 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0 (void);
// 0x00000158 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565 (void);
// 0x00000159 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52 (void);
// 0x0000015A System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA (void);
// 0x0000015B System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x0000015C System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x0000015D System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x0000015E System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x0000015F TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x00000160 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x00000161 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x00000162 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x00000163 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x00000164 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x00000165 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x00000166 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x00000167 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x00000168 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x00000169 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x0000016A System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x0000016B System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x0000016C System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x0000016D System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189 (void);
// 0x0000016E System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475 (void);
// 0x0000016F System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33 (void);
// 0x00000170 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6 (void);
// 0x00000171 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02 (void);
// 0x00000172 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x00000173 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x00000174 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x00000175 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x00000176 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x00000177 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x00000178 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x00000179 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x0000017A System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69 (void);
// 0x0000017B System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D (void);
// 0x0000017C System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9 (void);
// 0x0000017D System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6 (void);
// 0x0000017E System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A (void);
// 0x0000017F System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD (void);
// 0x00000180 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x00000181 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x00000182 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A (void);
// 0x00000183 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC (void);
// 0x00000184 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9 (void);
// 0x00000185 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46 (void);
// 0x00000186 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA (void);
// 0x00000187 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A (void);
// 0x00000188 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x00000189 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x0000018A System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3 (void);
// 0x0000018B System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x0000018C System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x0000018D System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x0000018E System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x0000018F System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x00000190 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x00000191 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x00000192 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F (void);
// 0x00000193 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x00000194 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x00000195 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x00000196 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x00000197 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x00000198 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x00000199 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56 (void);
// 0x0000019A System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x0000019B System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D (void);
// 0x0000019C System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71 (void);
// 0x0000019D System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC (void);
// 0x0000019E System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95 (void);
// 0x0000019F System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA (void);
// 0x000001A0 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA (void);
// 0x000001A1 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x000001A2 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x000001A3 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x000001A4 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x000001A5 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x000001A6 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB (void);
// 0x000001A7 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362 (void);
// 0x000001A8 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x000001A9 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2 (void);
// 0x000001AA System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51 (void);
// 0x000001AB System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E (void);
// 0x000001AC System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB (void);
// 0x000001AD System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5 (void);
// 0x000001AE System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5 (void);
// 0x000001AF System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x000001B0 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x000001B1 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x000001B2 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA (void);
// 0x000001B3 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2 (void);
// 0x000001B4 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45 (void);
// 0x000001B5 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA (void);
// 0x000001B6 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9 (void);
// 0x000001B7 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8 (void);
// 0x000001B8 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x000001B9 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x000001BA System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x000001BB System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x000001BC System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A (void);
// 0x000001BD System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52 (void);
// 0x000001BE System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1 (void);
// 0x000001BF System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x000001C0 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45 (void);
// 0x000001C1 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B (void);
// 0x000001C2 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE (void);
// 0x000001C3 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854 (void);
// 0x000001C4 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3 (void);
// 0x000001C5 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186 (void);
// 0x000001C6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC (void);
// 0x000001C7 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5 (void);
// 0x000001C8 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C (void);
// 0x000001C9 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683 (void);
// 0x000001CA System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D (void);
// 0x000001CB System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E (void);
// 0x000001CC System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x000001CD System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x000001CE System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x000001CF System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x000001D0 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x000001D1 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557 (void);
// 0x000001D2 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0 (void);
// 0x000001D3 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB (void);
// 0x000001D4 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8 (void);
// 0x000001D5 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A (void);
// 0x000001D6 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797 (void);
// 0x000001D7 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54 (void);
// 0x000001D8 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923 (void);
// 0x000001D9 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B (void);
// 0x000001DA System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4 (void);
// 0x000001DB System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041 (void);
// 0x000001DC System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1 (void);
// 0x000001DD System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55 (void);
// 0x000001DE System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D (void);
// 0x000001DF System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x000001E0 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x000001E1 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x000001E2 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C (void);
// 0x000001E3 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x000001E4 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x000001E5 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x000001E6 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x000001E7 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x000001E8 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x000001E9 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x000001EA System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723 (void);
// 0x000001EB System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x000001EC System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x000001ED System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x000001EE System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE (void);
// 0x000001EF System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215 (void);
// 0x000001F0 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79 (void);
// 0x000001F1 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559 (void);
// 0x000001F2 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779 (void);
// 0x000001F3 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x000001F4 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x000001F5 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x000001F6 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x000001F7 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x000001F8 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x000001F9 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x000001FA System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x000001FB System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x000001FC System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x000001FD System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE (void);
// 0x000001FE System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x000001FF System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x00000200 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x00000201 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x00000202 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x00000203 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398 (void);
// 0x00000204 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x00000205 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x00000206 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x00000207 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x00000208 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36 (void);
// 0x00000209 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x0000020A System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x0000020B System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x0000020C System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC (void);
// 0x0000020D System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x0000020E System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262 (void);
// 0x0000020F System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF (void);
// 0x00000210 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60 (void);
// 0x00000211 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1 (void);
// 0x00000212 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899 (void);
// 0x00000213 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C (void);
// 0x00000214 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x00000215 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x00000216 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x00000217 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x00000218 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8 (void);
// 0x00000219 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88 (void);
// 0x0000021A System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x0000021B System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A (void);
// 0x0000021C System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8 (void);
// 0x0000021D System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273 (void);
// 0x0000021E System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405 (void);
// 0x0000021F System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0 (void);
// 0x00000220 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9 (void);
// 0x00000221 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x00000222 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x00000223 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x00000224 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x00000225 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77 (void);
// 0x00000226 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38 (void);
// 0x00000227 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x00000228 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A (void);
// 0x00000229 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7 (void);
// 0x0000022A System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934 (void);
// 0x0000022B System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE (void);
// 0x0000022C System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90 (void);
// 0x0000022D System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B (void);
// 0x0000022E System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x0000022F System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x00000230 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x00000231 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x00000232 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D (void);
// 0x00000233 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F (void);
// 0x00000234 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x00000235 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D (void);
// 0x00000236 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8 (void);
// 0x00000237 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348 (void);
// 0x00000238 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845 (void);
// 0x00000239 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A (void);
// 0x0000023A System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0 (void);
// 0x0000023B System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x0000023C System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x0000023D System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x0000023E System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x0000023F System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7 (void);
// 0x00000240 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0 (void);
// 0x00000241 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x00000242 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A (void);
// 0x00000243 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A (void);
// 0x00000244 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6 (void);
// 0x00000245 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E (void);
// 0x00000246 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E (void);
// 0x00000247 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B (void);
// 0x00000248 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821 (void);
// 0x00000249 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C (void);
// 0x0000024A System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x0000024B System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x0000024C UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF (void);
// 0x0000024D System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98 (void);
// 0x0000024E System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x0000024F System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB (void);
// 0x00000250 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53 (void);
// 0x00000251 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02 (void);
// 0x00000252 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3 (void);
// 0x00000253 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42 (void);
// 0x00000254 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19 (void);
// 0x00000255 System.Void FiveRabbitsDemo.AnimatorParamatersChange::Start()
extern void AnimatorParamatersChange_Start_m871E4A208B70403948907DD6EFF16B5C89AF6558 (void);
// 0x00000256 System.Void FiveRabbitsDemo.AnimatorParamatersChange::Update()
extern void AnimatorParamatersChange_Update_mB838E976A19CD1C3F3FE31502501C8D442DCBBCD (void);
// 0x00000257 System.Void FiveRabbitsDemo.AnimatorParamatersChange::OnGUI()
extern void AnimatorParamatersChange_OnGUI_m240E5495716AFA707AEA5F259ACB8B4F85E0B3FD (void);
// 0x00000258 System.Void FiveRabbitsDemo.AnimatorParamatersChange::.ctor()
extern void AnimatorParamatersChange__ctor_m4C2265B2747B0F180A4CC7EDFCC2F516838CC1AF (void);
// 0x00000259 UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::CurrentAAMaterial()
extern void Antialiasing_CurrentAAMaterial_m616ACC65D35018EF4D483D5806041ECD09E2935A (void);
// 0x0000025A System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::CheckResources()
extern void Antialiasing_CheckResources_mAEDFCD3A79F2A463C14675E8A517A2A8CB0B1AA4 (void);
// 0x0000025B System.Void UnityStandardAssets.ImageEffects.Antialiasing::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Antialiasing_OnRenderImage_m43277916DED7A82DA3C7B76B880B3241A22C08C3 (void);
// 0x0000025C System.Void UnityStandardAssets.ImageEffects.Antialiasing::.ctor()
extern void Antialiasing__ctor_m462AB00D4D7D5B984126BD14A6B197FB7713E28A (void);
// 0x0000025D System.Boolean UnityStandardAssets.ImageEffects.Bloom::CheckResources()
extern void Bloom_CheckResources_mE5E995DC8F2EAF421C6231A991FC21E05BF19A3E (void);
// 0x0000025E System.Void UnityStandardAssets.ImageEffects.Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_OnRenderImage_m91D52D682467DE9C1ED3D38A617EDDC3DEDB4542 (void);
// 0x0000025F System.Void UnityStandardAssets.ImageEffects.Bloom::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_AddTo_mBC2B4CB5D7085DE45E9087794088E0156293249B (void);
// 0x00000260 System.Void UnityStandardAssets.ImageEffects.Bloom::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BlendFlares_mA45BC26BE27BBFF337D7091303DAE4C6BF99D3F1 (void);
// 0x00000261 System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BrightFilter_mC7413A396C92E39BEADDDF0BD1D6C875B9619CEA (void);
// 0x00000262 System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(UnityEngine.Color,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BrightFilter_m02A1C963E82D3EC39D46953DA82953B5A011E863 (void);
// 0x00000263 System.Void UnityStandardAssets.ImageEffects.Bloom::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_Vignette_mD5A802FBE6716C0B281883004266778F37F0066A (void);
// 0x00000264 System.Void UnityStandardAssets.ImageEffects.Bloom::.ctor()
extern void Bloom__ctor_m42F7BAB1B3812EBB56DEBAB04ED8AE14D2C22D10 (void);
// 0x00000265 System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::CheckResources()
extern void BloomAndFlares_CheckResources_m25E72D332C6B7E4BA07D091336B9A07A215ABD9D (void);
// 0x00000266 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_OnRenderImage_m3A0E0EEAB00CB8961754815CDCD3B751407453A9 (void);
// 0x00000267 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_AddTo_mDF8D4610504FF933746ECE9149009899202A0D2D (void);
// 0x00000268 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_BlendFlares_m8EE10767AE510FD6B6C8E34A87B94526EAEA9911 (void);
// 0x00000269 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BrightFilter(System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_BrightFilter_m68F28105229213BD4C24ED26E768BAFF50E1D941 (void);
// 0x0000026A System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_Vignette_mDF8148D41EE021A5C506A74749FBE3AA4496D7FB (void);
// 0x0000026B System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::.ctor()
extern void BloomAndFlares__ctor_mF41ECDDA3A818081EADD14391504A77C1BC317B2 (void);
// 0x0000026C System.Boolean UnityStandardAssets.ImageEffects.BloomOptimized::CheckResources()
extern void BloomOptimized_CheckResources_m108A809CE36BF4F07ADDC9D055A32F046085C8A1 (void);
// 0x0000026D System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnDisable()
extern void BloomOptimized_OnDisable_m39774D4731E12D5B3D97815705D8E56CBF1DEB9C (void);
// 0x0000026E System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomOptimized_OnRenderImage_m866388BBDF62234065C6D89F692044E92A85447D (void);
// 0x0000026F System.Void UnityStandardAssets.ImageEffects.BloomOptimized::.ctor()
extern void BloomOptimized__ctor_mEA93BDFF232E2DDE86C49AFFD8884CD68196A93A (void);
// 0x00000270 UnityEngine.Material UnityStandardAssets.ImageEffects.Blur::get_material()
extern void Blur_get_material_mB9BCB7739A59468A30B5D1EEA39389CEB68DE6A1 (void);
// 0x00000271 System.Void UnityStandardAssets.ImageEffects.Blur::OnDisable()
extern void Blur_OnDisable_m5C34BC91F6C2FB85142D81CA022522641A8A56C6 (void);
// 0x00000272 System.Void UnityStandardAssets.ImageEffects.Blur::Start()
extern void Blur_Start_m7B0242EF7E086AF91E633384FBD3818C5BEC07EA (void);
// 0x00000273 System.Void UnityStandardAssets.ImageEffects.Blur::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern void Blur_FourTapCone_m089475C2D8D118A4F7949669DEC3A562BEED4D46 (void);
// 0x00000274 System.Void UnityStandardAssets.ImageEffects.Blur::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Blur_DownSample4x_mB0689283883431638F88A750CD4BCA695470C38A (void);
// 0x00000275 System.Void UnityStandardAssets.ImageEffects.Blur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Blur_OnRenderImage_m8303C27411C4F059DCCCCBAB72F4C93F93BD87A3 (void);
// 0x00000276 System.Void UnityStandardAssets.ImageEffects.Blur::.ctor()
extern void Blur__ctor_m7F3FDA508B9E862A06AC0A7E914CE8239116317F (void);
// 0x00000277 System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources()
extern void BlurOptimized_CheckResources_mC32D0A636D288E64F2E12FD1F08DAA4FBEFECF69 (void);
// 0x00000278 System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnDisable()
extern void BlurOptimized_OnDisable_m0E9A2DF9846B126BC9533814F82748ADCCE0615A (void);
// 0x00000279 System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BlurOptimized_OnRenderImage_m9E9D0E2B1210867A24E2198D214E0CF07D8356EB (void);
// 0x0000027A System.Void UnityStandardAssets.ImageEffects.BlurOptimized::.ctor()
extern void BlurOptimized__ctor_m6DB6B84228DCD3F15ECAE0FFD874EE418C2ECD48 (void);
// 0x0000027B System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::CalculateViewProjection()
extern void CameraMotionBlur_CalculateViewProjection_m89B118B051F4C5CE754F151C6A5D4996D22C5E4A (void);
// 0x0000027C System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Start()
extern void CameraMotionBlur_Start_mF97E62379A2265CC081CCD96D38156BEDCBB51A7 (void);
// 0x0000027D System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnEnable()
extern void CameraMotionBlur_OnEnable_m5F2A506FAD0EF9C5D9339C4604259D69BA464694 (void);
// 0x0000027E System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnDisable()
extern void CameraMotionBlur_OnDisable_m2BAFA146755B557A389C1231798293BE51F7114F (void);
// 0x0000027F System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::CheckResources()
extern void CameraMotionBlur_CheckResources_mC74FE9882CB13291322CCA5340A0EFAA2CCB6EDA (void);
// 0x00000280 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void CameraMotionBlur_OnRenderImage_mB237BC2F5A2822AA3071639C2AE7B857CD3E965F (void);
// 0x00000281 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Remember()
extern void CameraMotionBlur_Remember_mE2C9E38314CF73169C273DA569280DDEF41B6151 (void);
// 0x00000282 UnityEngine.Camera UnityStandardAssets.ImageEffects.CameraMotionBlur::GetTmpCam()
extern void CameraMotionBlur_GetTmpCam_mDD546C874BFC169F515E42A151ABDC3CDB379254 (void);
// 0x00000283 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::StartFrame()
extern void CameraMotionBlur_StartFrame_m1EFBDB22B9DAA74E4A7F96FFC46095A7FF0B33F9 (void);
// 0x00000284 System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::divRoundUp(System.Int32,System.Int32)
extern void CameraMotionBlur_divRoundUp_mEB15E394BD9A589D23F3F32DFD6D9C5B66D3B983 (void);
// 0x00000285 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.ctor()
extern void CameraMotionBlur__ctor_mBF3F5856FDDD16AF94E45A37862AFB8E559D22C4 (void);
// 0x00000286 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.cctor()
extern void CameraMotionBlur__cctor_mB9C329A189E7239A01C70A4338520905E004763D (void);
// 0x00000287 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern void ColorCorrectionCurves_Start_m8BEEAC0FA96F6180735794867AD2E152AA3B9178 (void);
// 0x00000288 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern void ColorCorrectionCurves_Awake_mEBEDC0872067064FA5CCC7F4AB1656532C100456 (void);
// 0x00000289 System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern void ColorCorrectionCurves_CheckResources_m2E51D375CAD799867434AA46FF1CC9D69A3C3830 (void);
// 0x0000028A System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern void ColorCorrectionCurves_UpdateParameters_mAC7A0DD437ADD29E631D1DE306DEC09269D9636B (void);
// 0x0000028B System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern void ColorCorrectionCurves_UpdateTextures_mDB0CE54845439B6942B3AF2086BF0ED6B15E68BA (void);
// 0x0000028C System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionCurves_OnRenderImage_m5451EA9A29065B6AE78C281A834722BB5FD8CF90 (void);
// 0x0000028D System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern void ColorCorrectionCurves__ctor_m57B1F217548D607FEBB56163AAB246509FBAC9B3 (void);
// 0x0000028E System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::CheckResources()
extern void ColorCorrectionLookup_CheckResources_mE798BD5135D4A4AE1B418A55F8A4C7091B4989C3 (void);
// 0x0000028F System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDisable()
extern void ColorCorrectionLookup_OnDisable_mF964C0BF7C7181BF40C6B9A652AABD182BB58A2F (void);
// 0x00000290 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDestroy()
extern void ColorCorrectionLookup_OnDestroy_m916EFAECBAE25947B02CEC323BD69FB406B5250A (void);
// 0x00000291 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::SetIdentityLut()
extern void ColorCorrectionLookup_SetIdentityLut_m6A79565B09A07036C767AF1A16C274500AC130D3 (void);
// 0x00000292 System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::ValidDimensions(UnityEngine.Texture2D)
extern void ColorCorrectionLookup_ValidDimensions_m41CA8EF5DD8281804F2E091A92495D56940CFBEF (void);
// 0x00000293 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::Convert(UnityEngine.Texture2D,System.String)
extern void ColorCorrectionLookup_Convert_mF4235B7639E43DF5A2C309C167B81D77EE517111 (void);
// 0x00000294 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionLookup_OnRenderImage_m0545AE128DF587EC53F9C802F13F7892922B7DAF (void);
// 0x00000295 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::.ctor()
extern void ColorCorrectionLookup__ctor_mB7DB4D90090C593309EE6822E9D14F51A89423B8 (void);
// 0x00000296 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionRamp_OnRenderImage_m489A2D7B3567FA0E26B4E315BA1F95E1B92C6093 (void);
// 0x00000297 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::.ctor()
extern void ColorCorrectionRamp__ctor_mF3F11103A22E62E20914697B24B975E1DBBD00B2 (void);
// 0x00000298 System.Boolean UnityStandardAssets.ImageEffects.ContrastEnhance::CheckResources()
extern void ContrastEnhance_CheckResources_m8CBC6E6E65DA28E6A5EA2B6D235583C54C6DE5D8 (void);
// 0x00000299 System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ContrastEnhance_OnRenderImage_mC9521D66DF962B0DBA263DA937E3F8C37AFF00DD (void);
// 0x0000029A System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::.ctor()
extern void ContrastEnhance__ctor_mA046704E2483DBB3080A4FE34D27FD03D774F02B (void);
// 0x0000029B UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialLum()
extern void ContrastStretch_get_materialLum_m3E0BCDCD06A97473C3F509D6E47C5C937E0C5630 (void);
// 0x0000029C UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialReduce()
extern void ContrastStretch_get_materialReduce_mFF50C7EA9BD50C89DB513F8EEFFE21137F8C47E5 (void);
// 0x0000029D UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialAdapt()
extern void ContrastStretch_get_materialAdapt_mA286A06F5BF2CBCAE2B2EB8F2074D43C927FBD7E (void);
// 0x0000029E UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialApply()
extern void ContrastStretch_get_materialApply_mB7339208DD2EF717DA91766024071E1D8E670912 (void);
// 0x0000029F System.Void UnityStandardAssets.ImageEffects.ContrastStretch::Start()
extern void ContrastStretch_Start_mD99DCAF9FF50BC7C76F32AB250EA6414767CF365 (void);
// 0x000002A0 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnEnable()
extern void ContrastStretch_OnEnable_m3D4DF80A85246544A3A585D787E9F864F529379A (void);
// 0x000002A1 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnDisable()
extern void ContrastStretch_OnDisable_mFF56DEE2F8871A1C5FDEDF1F39B09806835594B7 (void);
// 0x000002A2 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ContrastStretch_OnRenderImage_mCBFF98D8D7AF36A66CA40088F65D667C00CBC411 (void);
// 0x000002A3 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::CalculateAdaptation(UnityEngine.Texture)
extern void ContrastStretch_CalculateAdaptation_m801A05FEDB49C0586E503F53EBCD36F7B755170D (void);
// 0x000002A4 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::.ctor()
extern void ContrastStretch__ctor_mDDD983556852FAFF344F2033C8AEAF43DEB21B55 (void);
// 0x000002A5 System.Boolean UnityStandardAssets.ImageEffects.CreaseShading::CheckResources()
extern void CreaseShading_CheckResources_m11195082724590FFA17545626F44D62DE9E5BAFE (void);
// 0x000002A6 System.Void UnityStandardAssets.ImageEffects.CreaseShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void CreaseShading_OnRenderImage_m7ABCB39ABC2AEDDBE2BC38258B1CC7DF3C603003 (void);
// 0x000002A7 System.Void UnityStandardAssets.ImageEffects.CreaseShading::.ctor()
extern void CreaseShading__ctor_mB81EC56595A322BFD46F7B6ABB77B11834AEBD95 (void);
// 0x000002A8 System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::CheckResources()
extern void DepthOfField_CheckResources_m5B73E318E80ADCBFA181C196B658C8F2EF3EB87D (void);
// 0x000002A9 System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnEnable()
extern void DepthOfField_OnEnable_mF22CEEE7130A0FD5B717C4FE268CB4708A67A416 (void);
// 0x000002AA System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnDisable()
extern void DepthOfField_OnDisable_m6A4C40367E5041B08FDFFCB9AF9DF0EC4707C71A (void);
// 0x000002AB System.Void UnityStandardAssets.ImageEffects.DepthOfField::ReleaseComputeResources()
extern void DepthOfField_ReleaseComputeResources_mE39DFC9B512EE74FCCC7D0A58828CDF2D446FB1F (void);
// 0x000002AC System.Void UnityStandardAssets.ImageEffects.DepthOfField::CreateComputeResources()
extern void DepthOfField_CreateComputeResources_m44A3D7A9CA59140FB293D9FD3FF0B1E9B9231F19 (void);
// 0x000002AD System.Single UnityStandardAssets.ImageEffects.DepthOfField::FocalDistance01(System.Single)
extern void DepthOfField_FocalDistance01_m9753A71BBBDFE2BB44294D4506A2D6BDEF77ED9D (void);
// 0x000002AE System.Void UnityStandardAssets.ImageEffects.DepthOfField::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern void DepthOfField_WriteCoc_mE94B9C8FD075063F31E2B906B329CB7C4BB6F8B1 (void);
// 0x000002AF System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfField_OnRenderImage_mAA557E588FE92346DF0FEC7043A9BEABC78857F0 (void);
// 0x000002B0 System.Void UnityStandardAssets.ImageEffects.DepthOfField::.ctor()
extern void DepthOfField__ctor_m2A265D1437B0AAA1065A785E8428E162552F0393 (void);
// 0x000002B1 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CreateMaterials()
extern void DepthOfFieldDeprecated_CreateMaterials_m3DFE5FBA8B2F6DF52BC208205A3945454479890B (void);
// 0x000002B2 System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CheckResources()
extern void DepthOfFieldDeprecated_CheckResources_mE8F5F2654CF5F3668BDCAC07E5022170055BD07F (void);
// 0x000002B3 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnDisable()
extern void DepthOfFieldDeprecated_OnDisable_m3E3D5ACFF1FDC10FD900F54C5D9CBF2BEE920D4E (void);
// 0x000002B4 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnEnable()
extern void DepthOfFieldDeprecated_OnEnable_m86B858F3493ADC8E9F49BCFD33E54AF51D103561 (void);
// 0x000002B5 System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::FocalDistance01(System.Single)
extern void DepthOfFieldDeprecated_FocalDistance01_mC2B55F876AFE563170E6F57211078C5A5D89C5A8 (void);
// 0x000002B6 System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetDividerBasedOnQuality()
extern void DepthOfFieldDeprecated_GetDividerBasedOnQuality_m80B6BF4C7B3249E986BEF7D8D8D2B0DE201067AB (void);
// 0x000002B7 System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetLowResolutionDividerBasedOnQuality(System.Int32)
extern void DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m985CDBEC2CF43F2E8907FA1FE19D3D127C170CB0 (void);
// 0x000002B8 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_OnRenderImage_m65BD2421A8466090B3329A2E3D9DD982A436282C (void);
// 0x000002B9 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness,System.Int32,System.Single)
extern void DepthOfFieldDeprecated_Blur_mA2B98FF3332C7AD3D25634AF426A6732768CE451 (void);
// 0x000002BA System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated/DofBlurriness,System.Int32,System.Single)
extern void DepthOfFieldDeprecated_BlurFg_m9EC1D58A51FA15F10BD6B6F201354D5F5942FD61 (void);
// 0x000002BB System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_BlurHex_mA06B816018A052277D95895580456609D2FB4855 (void);
// 0x000002BC System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_Downsample_mECCC2FD41C3593F6C4881D392F9B0CE21FB56CCC (void);
// 0x000002BD System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_AddBokeh_m5C7284DF55D8065B3F01A41D8F699A0D79E33077 (void);
// 0x000002BE System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::ReleaseTextures()
extern void DepthOfFieldDeprecated_ReleaseTextures_m3F4F40F3CAEB5D02B1EB43C65457B811CCF61FA4 (void);
// 0x000002BF System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32)
extern void DepthOfFieldDeprecated_AllocateTextures_m26D14CBF056C12BA4CC153913DBDDBE9BFB29B0D (void);
// 0x000002C0 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.ctor()
extern void DepthOfFieldDeprecated__ctor_m6B6B37E85FB0EA2831AE0CA05FD781FAEE5DA0BF (void);
// 0x000002C1 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.cctor()
extern void DepthOfFieldDeprecated__cctor_m3FA1672494E284D85C238AE853FDAC4C596326B6 (void);
// 0x000002C2 System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources()
extern void EdgeDetection_CheckResources_m844678E40D8131D435B81A85884316C437AA33C3 (void);
// 0x000002C3 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::Start()
extern void EdgeDetection_Start_mB5C5CBF5FB91B71A0E44915A297D76B8A936F5D8 (void);
// 0x000002C4 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::SetCameraFlag()
extern void EdgeDetection_SetCameraFlag_mFBFBA5CBA1DD869326CB998571155DEB1147FB7C (void);
// 0x000002C5 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnEnable()
extern void EdgeDetection_OnEnable_mA5DAB7483904036D88EB9EB54693AF7212C2AB3E (void);
// 0x000002C6 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void EdgeDetection_OnRenderImage_m38E961698AF5A6CD1FB1F3646B6E4EBB78260DD1 (void);
// 0x000002C7 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::.ctor()
extern void EdgeDetection__ctor_m0D04687855A216627755DA1FFA8688A69A46FDE8 (void);
// 0x000002C8 System.Boolean UnityStandardAssets.ImageEffects.Fisheye::CheckResources()
extern void Fisheye_CheckResources_m6FFFA5A2454821C9C998FFEFE2FB212D2BC4D9D8 (void);
// 0x000002C9 System.Void UnityStandardAssets.ImageEffects.Fisheye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Fisheye_OnRenderImage_m28DBEA9F277263BF7E11C4A3DF06B713E505254B (void);
// 0x000002CA System.Void UnityStandardAssets.ImageEffects.Fisheye::.ctor()
extern void Fisheye__ctor_m8A4F67E1FC937F5867702B596171FCEB427BF3E5 (void);
// 0x000002CB System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::CheckResources()
extern void GlobalFog_CheckResources_m9F40B749F22806F92122CF5CECE86612E6FA2E53 (void);
// 0x000002CC System.Void UnityStandardAssets.ImageEffects.GlobalFog::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void GlobalFog_OnRenderImage_mE19FBCBCFB6223066FA043162F3DB09DCE56CF5B (void);
// 0x000002CD System.Void UnityStandardAssets.ImageEffects.GlobalFog::.ctor()
extern void GlobalFog__ctor_m1C6432E5A10611049D2CFE56B4B8FE03D0560B7F (void);
// 0x000002CE System.Void UnityStandardAssets.ImageEffects.Grayscale::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Grayscale_OnRenderImage_mFD132F2F2E0CA27ED9CCD86C1FF19D1F6C3CB4C0 (void);
// 0x000002CF System.Void UnityStandardAssets.ImageEffects.Grayscale::.ctor()
extern void Grayscale__ctor_m01B85C45849A4473DEC2FA28A3DBB920D46F22DC (void);
// 0x000002D0 System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::Start()
extern void ImageEffectBase_Start_mAAC05E3ED1455E1CB4C5BCB5AB9414CEAF18EB8B (void);
// 0x000002D1 UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::get_material()
extern void ImageEffectBase_get_material_mB006F34B91F5520C2F543594586F8456AB2CB024 (void);
// 0x000002D2 System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::OnDisable()
extern void ImageEffectBase_OnDisable_m521F62E819A026FE6946761CC76A7C7F7C8AD014 (void);
// 0x000002D3 System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::.ctor()
extern void ImageEffectBase__ctor_mD934C852B973B7EB2FA5CD62E6D0BD7135D0A42B (void);
// 0x000002D4 System.Void UnityStandardAssets.ImageEffects.ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern void ImageEffects_RenderDistortion_mB9B8E1ABEC327C7EE2387A51108C4F6B972561BC (void);
// 0x000002D5 System.Void UnityStandardAssets.ImageEffects.ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ImageEffects_Blit_m8EA90C6A40EF19EE94B174AD10ABBFE01735FB6D (void);
// 0x000002D6 System.Void UnityStandardAssets.ImageEffects.ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ImageEffects_BlitWithMaterial_m52DEE89A5B2726D9F0E3B88DD2D9681747BE0417 (void);
// 0x000002D7 System.Void UnityStandardAssets.ImageEffects.ImageEffects::.ctor()
extern void ImageEffects__ctor_mA575B9A385AEA923FDB67680C6BBEF06D50A8649 (void);
// 0x000002D8 System.Void UnityStandardAssets.ImageEffects.MotionBlur::Start()
extern void MotionBlur_Start_mE939F398C1D7025D5F95CAA5BB7B51ACBBEAB0CB (void);
// 0x000002D9 System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnDisable()
extern void MotionBlur_OnDisable_m524F34F73889C816D7CC89E09B165FD13E403B4E (void);
// 0x000002DA System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void MotionBlur_OnRenderImage_m7591D001CA3CFC52F5BBAA2C23E4B46C79BA04A6 (void);
// 0x000002DB System.Void UnityStandardAssets.ImageEffects.MotionBlur::.ctor()
extern void MotionBlur__ctor_m99420C3172B7C4F8AF1BCD99E2B57AA26FAF564F (void);
// 0x000002DC System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::CheckResources()
extern void NoiseAndGrain_CheckResources_mB08923E967617EB5023BC61CF41C28F2D453F1D8 (void);
// 0x000002DD System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void NoiseAndGrain_OnRenderImage_m92D9F85A52979B12AB52AD9074CB23E86EC71C3D (void);
// 0x000002DE System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::DrawNoiseQuadGrid(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture2D,System.Int32)
extern void NoiseAndGrain_DrawNoiseQuadGrid_mB378C3977FE3F97AF4168FE05DEB395F8A0491D6 (void);
// 0x000002DF System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.ctor()
extern void NoiseAndGrain__ctor_mFDE0C9E284B4E9B63B3DD335020EA3B4723BED70 (void);
// 0x000002E0 System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.cctor()
extern void NoiseAndGrain__cctor_mB399C524E753D4475ACA9091FC4B23506BC5DE8C (void);
// 0x000002E1 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::Start()
extern void NoiseAndScratches_Start_mEBF2EF7B65C924EA3C9AFC1DDE2817A3766D0D48 (void);
// 0x000002E2 UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::get_material()
extern void NoiseAndScratches_get_material_m45C5A28EBCBB71E6599FC49511E1AB3A9338D585 (void);
// 0x000002E3 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnDisable()
extern void NoiseAndScratches_OnDisable_m545D985962BE453BB260CE23FE2C0AA899115975 (void);
// 0x000002E4 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::SanitizeParameters()
extern void NoiseAndScratches_SanitizeParameters_m8B9C3842DAD850EBB04F3EAE8D41D4349AC96D3D (void);
// 0x000002E5 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void NoiseAndScratches_OnRenderImage_m8CA535280A5FF3E201006E0F2D1CDD6F7E5FD426 (void);
// 0x000002E6 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::.ctor()
extern void NoiseAndScratches__ctor_m8A6D067A18DF0EA3F2B27919AF6AD21DB55093EB (void);
// 0x000002E7 UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern void PostEffectsBase_CheckShaderAndCreateMaterial_m05677F5055873EAB286B69870015998FBB9D6927 (void);
// 0x000002E8 UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern void PostEffectsBase_CreateMaterial_mC9860A23866B8230D964BC09EDF7FD2878BA0D6C (void);
// 0x000002E9 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnEnable()
extern void PostEffectsBase_OnEnable_m8BC44BA7499D510F0A9887FECDC2C475C3EC159A (void);
// 0x000002EA System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnDestroy()
extern void PostEffectsBase_OnDestroy_m543FD01209891987E236BEBB1EB25FFCF595DCAB (void);
// 0x000002EB System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::RemoveCreatedMaterials()
extern void PostEffectsBase_RemoveCreatedMaterials_m5F54C33DA625B453B6737FDEF70DBCC7912371C7 (void);
// 0x000002EC System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport()
extern void PostEffectsBase_CheckSupport_m059D1547A9DFFFF9D18D2B702D9C1A98871B3689 (void);
// 0x000002ED System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources()
extern void PostEffectsBase_CheckResources_m01EFEABDDEECF8931BDE27F6D76536FD52C2E273 (void);
// 0x000002EE System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::Start()
extern void PostEffectsBase_Start_m256D1B7C8E17442D85DCCD08D6D157080A89B79C (void);
// 0x000002EF System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern void PostEffectsBase_CheckSupport_m97A1AE9C8FAFEA8D8FF7FA69916F388ACCA9FB34 (void);
// 0x000002F0 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern void PostEffectsBase_CheckSupport_mA7398DAA19FD5AC947119F26E3509974EAFC1664 (void);
// 0x000002F1 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::Dx11Support()
extern void PostEffectsBase_Dx11Support_mCAEFE6A80827A8B34E73B44DA8304C0CAD8330E5 (void);
// 0x000002F2 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern void PostEffectsBase_ReportAutoDisable_m4A617083CFD2A09EEECF6329CD2C9AD101095E34 (void);
// 0x000002F3 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShader(UnityEngine.Shader)
extern void PostEffectsBase_CheckShader_m2915D7EE44889306449307C06FE3F9770FAAF012 (void);
// 0x000002F4 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern void PostEffectsBase_NotSupported_m3328CFF3B6C73682EAC7141F15716F34940D86AD (void);
// 0x000002F5 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsBase_DrawBorder_m501B2ABEF35941F9B67F4DE18B28051D2CEBBA53 (void);
// 0x000002F6 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern void PostEffectsBase__ctor_mA6375ECEEA9338D745C8ADDDF2CE2CFE19AD3919 (void);
// 0x000002F7 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void PostEffectsHelper_OnRenderImage_mA18DD006648EA3B0DFF3B2F398CF092CF4F80AFD (void);
// 0x000002F8 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelPlaneAlignedWithCamera(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Camera)
extern void PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m53C287FE762F776C762CDCC54CC78FA7F722AB73 (void);
// 0x000002F9 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsHelper_DrawBorder_m24A63C8C77ED434DB28D78DADEA1E9F9D125CD9F (void);
// 0x000002FA System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelQuad(System.Single,System.Single,System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsHelper_DrawLowLevelQuad_m1FE6DC9DAF0604EFF00C05FDB06B424AE1856C12 (void);
// 0x000002FB System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::.ctor()
extern void PostEffectsHelper__ctor_m416594A86C0F56174E3FA23BBDADD32D6E8E2F30 (void);
// 0x000002FC System.Boolean UnityStandardAssets.ImageEffects.Quads::HasMeshes()
extern void Quads_HasMeshes_m41D66F9A795040CCB2219E1412C88C1C59729A04 (void);
// 0x000002FD System.Void UnityStandardAssets.ImageEffects.Quads::Cleanup()
extern void Quads_Cleanup_m18CF6B317A33E5BFF0C5D139F3075321CE31A461 (void);
// 0x000002FE UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::GetMeshes(System.Int32,System.Int32)
extern void Quads_GetMeshes_mB329F322CBF3A8CEFE17BD982B669E48E2559625 (void);
// 0x000002FF UnityEngine.Mesh UnityStandardAssets.ImageEffects.Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Quads_GetMesh_m3001DD2FCD94103FE9D38091C4BF006CCA25F44E (void);
// 0x00000300 System.Void UnityStandardAssets.ImageEffects.Quads::.ctor()
extern void Quads__ctor_m16AD4881C27EBAFF0BEB90D19BFDD7BEA88F0B1D (void);
// 0x00000301 System.Boolean UnityStandardAssets.ImageEffects.ScreenOverlay::CheckResources()
extern void ScreenOverlay_CheckResources_m6A890799CFBE98D63CCCF134A0CA69A3F3CB88D6 (void);
// 0x00000302 System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenOverlay_OnRenderImage_mA6F3FFBE94BD2C17D1516E81B93B16015E3B9F41 (void);
// 0x00000303 System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::.ctor()
extern void ScreenOverlay__ctor_mDC65CB472F36EE51C7A7F52999B3AC09054E55F9 (void);
// 0x00000304 System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::CheckResources()
extern void ScreenSpaceAmbientObscurance_CheckResources_mFF7D1009C46F8CCF18C7869614FAA0E00A99226E (void);
// 0x00000305 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnDisable()
extern void ScreenSpaceAmbientObscurance_OnDisable_mF578FB1D02B1EEE113D3A0341D455B969C71DE38 (void);
// 0x00000306 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenSpaceAmbientObscurance_OnRenderImage_m230B77750177C0D038530D29B7CD5217CD778A84 (void);
// 0x00000307 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::.ctor()
extern void ScreenSpaceAmbientObscurance__ctor_m044ADAA04CE20850C56457877E9CE0C0F620414A (void);
// 0x00000308 UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterial(UnityEngine.Shader)
extern void ScreenSpaceAmbientOcclusion_CreateMaterial_mF7A3FD447AF298C8C49954F744F28EFBDAA1C6F0 (void);
// 0x00000309 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::DestroyMaterial(UnityEngine.Material)
extern void ScreenSpaceAmbientOcclusion_DestroyMaterial_m41A6707B9A5D617667823081040A20A121818F77 (void);
// 0x0000030A System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnDisable()
extern void ScreenSpaceAmbientOcclusion_OnDisable_mB3286E83ECA87FD2140D19B3CCAEFC5C2CE46B63 (void);
// 0x0000030B System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::Start()
extern void ScreenSpaceAmbientOcclusion_Start_m7C1617597789A3A15FD5ED8855606D8A2D4ED123 (void);
// 0x0000030C System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnEnable()
extern void ScreenSpaceAmbientOcclusion_OnEnable_m193984EB3410877D65EAB44BA45194270A8EA804 (void);
// 0x0000030D System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterials()
extern void ScreenSpaceAmbientOcclusion_CreateMaterials_mC63630500AFFAE0FBBD968E3CC980CCEF882118F (void);
// 0x0000030E System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenSpaceAmbientOcclusion_OnRenderImage_m24F6A58D4D44A6A3BBB477A9BB13CDAF91B71505 (void);
// 0x0000030F System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::.ctor()
extern void ScreenSpaceAmbientOcclusion__ctor_m615337A0FBED24E5C41E17418A2615FF8EF14991 (void);
// 0x00000310 System.Void UnityStandardAssets.ImageEffects.SepiaTone::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SepiaTone_OnRenderImage_m31BF87C02794E9A05C1677F4F520E535761EB5C1 (void);
// 0x00000311 System.Void UnityStandardAssets.ImageEffects.SepiaTone::.ctor()
extern void SepiaTone__ctor_m34F40550EC1F27CA8A3DB7DC35AFAF4813EBB157 (void);
// 0x00000312 System.Boolean UnityStandardAssets.ImageEffects.SunShafts::CheckResources()
extern void SunShafts_CheckResources_m9C75528877A6260288B58129276616124B900555 (void);
// 0x00000313 System.Void UnityStandardAssets.ImageEffects.SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SunShafts_OnRenderImage_m09CB5311D8F7CC6064215683AFA18D8E2DA94BF3 (void);
// 0x00000314 System.Void UnityStandardAssets.ImageEffects.SunShafts::.ctor()
extern void SunShafts__ctor_m17D8FCF6A1EEDA1A07F46BA3FD3D2579203C1C67 (void);
// 0x00000315 System.Boolean UnityStandardAssets.ImageEffects.TiltShift::CheckResources()
extern void TiltShift_CheckResources_mFF9ED6FAA29732CD2F698BCF10C44D6454024026 (void);
// 0x00000316 System.Void UnityStandardAssets.ImageEffects.TiltShift::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void TiltShift_OnRenderImage_m23A47A2B2AF059156929865A0FE5FA39EC7D8BD5 (void);
// 0x00000317 System.Void UnityStandardAssets.ImageEffects.TiltShift::.ctor()
extern void TiltShift__ctor_m309B85AE3B13B1CE309EE2E6D36571E4CD2EBB0B (void);
// 0x00000318 System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CheckResources()
extern void Tonemapping_CheckResources_mC1C0CAB02BD19684E08232CD57248E018FD07C8E (void);
// 0x00000319 System.Single UnityStandardAssets.ImageEffects.Tonemapping::UpdateCurve()
extern void Tonemapping_UpdateCurve_mADC42B4344BD7C6BF315CA7419DCE55A51B13156 (void);
// 0x0000031A System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnDisable()
extern void Tonemapping_OnDisable_m4E9ADFC3365B18E2A3ACF5F5B87BBE50FF08DF14 (void);
// 0x0000031B System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CreateInternalRenderTexture()
extern void Tonemapping_CreateInternalRenderTexture_mEA7D2798E23F5144C84D708B3CD71A3093B36E34 (void);
// 0x0000031C System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Tonemapping_OnRenderImage_m3DDE8CADC9E8BEB73403CF71C46CC83F1E32C960 (void);
// 0x0000031D System.Void UnityStandardAssets.ImageEffects.Tonemapping::.ctor()
extern void Tonemapping__ctor_mEF2579982D0293C0899810E4BCB8FF6B8862E84F (void);
// 0x0000031E System.Boolean UnityStandardAssets.ImageEffects.Triangles::HasMeshes()
extern void Triangles_HasMeshes_m06D1A010EFB4AFD22701D79DEA321C5BDCE8C0EE (void);
// 0x0000031F System.Void UnityStandardAssets.ImageEffects.Triangles::Cleanup()
extern void Triangles_Cleanup_m51E44CD73E253FDFBB58CC18030D0D03676B2194 (void);
// 0x00000320 UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::GetMeshes(System.Int32,System.Int32)
extern void Triangles_GetMeshes_m3DCF64012831DC4D525D97E07F13846F119F00F8 (void);
// 0x00000321 UnityEngine.Mesh UnityStandardAssets.ImageEffects.Triangles::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Triangles_GetMesh_m2794A44B43D1572051664372102732E88DF16B05 (void);
// 0x00000322 System.Void UnityStandardAssets.ImageEffects.Triangles::.ctor()
extern void Triangles__ctor_mB64F02B9481FC7C583FF49508F4AF36506137851 (void);
// 0x00000323 System.Void UnityStandardAssets.ImageEffects.Twirl::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Twirl_OnRenderImage_m29976F163DF515C836C6925BD2770B8B6AE9C925 (void);
// 0x00000324 System.Void UnityStandardAssets.ImageEffects.Twirl::.ctor()
extern void Twirl__ctor_m77736ED53F9B5740B06DA39C508641158E6D4B03 (void);
// 0x00000325 System.Boolean UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::CheckResources()
extern void VignetteAndChromaticAberration_CheckResources_m6F1DD9FE91586FC0B7C56E490DAC8E68E82AE9EC (void);
// 0x00000326 System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void VignetteAndChromaticAberration_OnRenderImage_m5C3262455A520C611E1C5818DA3AEBF7C5611DC6 (void);
// 0x00000327 System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::.ctor()
extern void VignetteAndChromaticAberration__ctor_m3DA6B990E322E0336C4040A1F6A40016E930A20A (void);
// 0x00000328 System.Void UnityStandardAssets.ImageEffects.Vortex::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Vortex_OnRenderImage_m9E5533007C6B15733582D6DBE20CE932EE9C2CFD (void);
// 0x00000329 System.Void UnityStandardAssets.ImageEffects.Vortex::.ctor()
extern void Vortex__ctor_m1E336A10B273F31D568CC82C1C5CEBB927402DD6 (void);
static Il2CppMethodPointer s_methodPointers[809] = 
{
	DynamicSkyLite_Awake_mD05B187151D8AF82976909464999B18975599B58,
	DynamicSkyLite_LateUpdate_m4B34FC4F639CF6987CC7FC153E5C895D745E9561,
	DynamicSkyLite__ctor_mCB7C02619FA1BB4FF3566B436B49500C0FCBDE14,
	FPSCounter_Awake_mADDB1FC3597AD55B60F1359D329EC9E81774C22C,
	FPSCounter_OnEnable_m6554D2DACA28C66EBE821F42F83BA2C955141EDC,
	FPSCounter_Update_m8FD51CDD4FC6103E47194F4D9848A7D325004E30,
	FPSCounter_DrawGraph_m7E4BEB5E1EE59B0A11047D1B5DB474D255557414,
	FPSCounter_CreateCounter_m9B1865ED789B779C54FB906FFC1E5AD0D11CEB68,
	FPSCounter_GiveLine_m2A91354B96AC53F958EA82E35C28A7D50C81AD53,
	FPSCounter__ctor_m4814EB8CF15E05040C15D4F6B057357EC68B7268,
	U3CDrawGraphU3Ed__20__ctor_m8CC663E118E5465C99EC34EDCCD7756345740767,
	U3CDrawGraphU3Ed__20_System_IDisposable_Dispose_m0E08B87199DB83B60DAB58AE965E204B93EFD195,
	U3CDrawGraphU3Ed__20_MoveNext_m1138E88461B5BBDF9620CB6F87BB1BE76B860581,
	U3CDrawGraphU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08CAD522045EAD1AF11B3C36015BD171E8B20354,
	U3CDrawGraphU3Ed__20_System_Collections_IEnumerator_Reset_m583A769391B91A101E5FCB10D336F77CA90AB004,
	U3CDrawGraphU3Ed__20_System_Collections_IEnumerator_get_Current_mB540BFA0CEBB2A5943109DB0D85AA3F9770A650C,
	StFPS_Counter_m51132A4B6EF178E1BB431E75DBACA71E6DF2BC7E,
	StFPS__cctor_m4757E6940866B358734CA065EB65C97C653AEED3,
	Water_Awake_mA0CF580A48B28332DF36ABC2AD9B1EDDA0C08F08,
	Water_OnEnable_mD7D50AE2976B32F886BDF5592B2B57F91E3AE822,
	Water_LateUpdate_mEE64408A397585CD33EA9825A952E68A31827928,
	Water__ctor_m39BD6E49B74929BAB39A81C4A4A7DDFFC5C8146F,
	Shark_Awake_m7861399260147AB35AF2A7384CA09B84E8959975,
	Shark_LateUpdate_m6C05851ACF1C0DEFCECDB1A29AFB05954A96EC71,
	Shark_Hunting_m8E2B397B7AD1E0F987420E821E0D32CBF84DD18B,
	Shark_Move_m94231792C92A537C8B73F5E12C7099C32746B93A,
	Shark_CameraRig_m687ED8420E7B8D158E526358B4865E8EDC0330DB,
	Shark_RandomVector_mAC25B961835E406CC55C6F8C7F0E8820684C3937,
	Shark_DebugPath_m519E1209F731F0F86408D4869FB9FE7C60ACC443,
	Shark__ctor_m44D4C7E30B83B9FA3CB11C7AFA49CBDCA79CE53E,
	Shark__cctor_mA83CD825BC2540B957117191B22B9F49AE6419F2,
	U3CRandomVectorU3Ed__26__ctor_mE3EF35212D466F18FCED8FCD2B2C4DFAC6BDF7C1,
	U3CRandomVectorU3Ed__26_System_IDisposable_Dispose_m5D1FA545D964689830D48232E7EB119B65315F98,
	U3CRandomVectorU3Ed__26_MoveNext_m3881F6AC56F441A25FF117AB498608723CF4B92E,
	U3CRandomVectorU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF68C011DBED959E418ECACB26C2D38919C6EDE0,
	U3CRandomVectorU3Ed__26_System_Collections_IEnumerator_Reset_m719569C8C8F7A881438D7234399BCB66FAEA3617,
	U3CRandomVectorU3Ed__26_System_Collections_IEnumerator_get_Current_m77C114A7B587BF2CE52C2E84BBF611CBCCF6D526,
	SlowMo_Awake_mC3D61772F946ECB30A57EFA558587F8317C500B8,
	SlowMo_LateUpdate_m84837A88B05D29F4C136ABF267D890352B285A47,
	SlowMo_OnApplicationQuit_mB2C144681DE899649E309847D9400C859923CA78,
	SlowMo__ctor_mA7CE26DF0BD2C3F0397843C764C5F07C6C012CC7,
	TDControl_Awake_mD9C8F1D0EBE6862EC456DE56170CE73762A16B2C,
	TDControl_LateUpdate_m8EC9D696B49C2B43EE3F7BDA00EC995B63911EB3,
	TDControl_Rotation_mD9A3545A1ABA6739FEFE0A8163AAF926A26E86E2,
	TDControl_Lift_m82831324FEA6BB2E57EBAA91983250DE5E096F1B,
	TDControl_CameraTransform_mE5052E37A7FB82B30C6725C4165AC298C9E8A6DA,
	TDControl__ctor_m3214C8ED125DFB18731A2A8FC8FA08B7D68F0CDB,
	Underwater_Awake_m0FB6A411A1E5417982DC4DA1125A0F5CC6880DFA,
	Underwater_LateUpdate_mB052DF2D28001B461CE1962133317B2E84DCBD16,
	Underwater__ctor_mF7835E6A62844ED26594301F6052C3C8AE099265,
	NVBoids_Awake_m5ADBFB6AABCC2EBE83BE280BD84359AB1B277147,
	NVBoids_LateUpdate_m4BE7F707DA445016F2DD35743B7F86C02F7EE7DE,
	NVBoids_FlocksMove_m530F8D5D816472DDA7B57983D8EF2AA8F43C008B,
	NVBoids_BirdsMove_m2793ABDB45B5B9C7AFF873E7008F65B75104159D,
	NVBoids_Danger_m8FF58867142747A9811C3393C3D1E89FFF892C5A,
	NVBoids_BehavioralChange_m017C5707C20BF800D129CD103F1D27AC9521098E,
	NVBoids_CreateFlock_mBEC8F2E91EF7D4936E117ED12D8DFDC253573D34,
	NVBoids_CreateBird_mDEE562AA687E0F493A7414A7E08AA86961B55408,
	NVBoids_BirdsRotationClamp_mB85FE5E10687EA18E5DD9817006CEF894A5526BB,
	NVBoids__ctor_mD8A280E8811740B328B85FA57E5A7628615EABB0,
	U3CDangerU3Ed__46__ctor_mAB0BB3A40C436B3D0F4084C41EFFA0F8803F566F,
	U3CDangerU3Ed__46_System_IDisposable_Dispose_m961FB5C0299DD1B9A408A3E121E337CAE7374316,
	U3CDangerU3Ed__46_MoveNext_m6528C89181BE2B86315709763A6924E756CB4927,
	U3CDangerU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17A5CE6205D59D77D83AEB82B980ED780895255C,
	U3CDangerU3Ed__46_System_Collections_IEnumerator_Reset_m78045BB15689020B4CC8FADEBB764942339619F0,
	U3CDangerU3Ed__46_System_Collections_IEnumerator_get_Current_m6E552E8C095410E56A45CA746EFA786660AD161C,
	U3CBehavioralChangeU3Ed__47__ctor_mEFC303C8B78FEE506C47D6A7471B08A1A30CBF77,
	U3CBehavioralChangeU3Ed__47_System_IDisposable_Dispose_mF6CFD9756C20CAA2179EA57276A5199E68C24CE8,
	U3CBehavioralChangeU3Ed__47_MoveNext_m19817EF5F5018F09BD1952B9A5502D42EE9ED5C1,
	U3CBehavioralChangeU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60025393D50350AC0B0455728682E136EBF86993,
	U3CBehavioralChangeU3Ed__47_System_Collections_IEnumerator_Reset_m62FD827919A6E92EFCD77E55A19B38508687472C,
	U3CBehavioralChangeU3Ed__47_System_Collections_IEnumerator_get_Current_m2C6C4D45FF21DEDC04DB0EEEA8F3F547306A4F0E,
	QuirkySeriesDemo_Start_m747A4AC49D3CAFEC9C1103AB871870474E14271B,
	QuirkySeriesDemo_Update_mD02D19C041CC46536540BB8494375E3995F8D282,
	QuirkySeriesDemo_NextAnimal_m10DF76EF2E976EF99487454A585076911C9FECF3,
	QuirkySeriesDemo_PrevAnimal_m190A48284BE754E311E8E9868B7F3F4B31D75FE4,
	QuirkySeriesDemo_ChangeAnimal_m5B17A23EEBC0FB89F0787BCC063E3705D8BA6166,
	QuirkySeriesDemo_NextAnimation_mC8627130D4B4A66CF187EE140FD76536F34EFFD4,
	QuirkySeriesDemo_PrevAnimation_mB71B2F4A9D6BBA49098F2A38FDCDDC50DD00799E,
	QuirkySeriesDemo_ChangeAnimation_m4C082A9C5E2C5D2DADEDC3FC5B287E32795FF80F,
	QuirkySeriesDemo_NextShapekey_m57CD6F4C160E1ECE7F636DF61CC338099C233A49,
	QuirkySeriesDemo_PrevShapekey_m57419B1380DFC1850AF8A4D0844FE4529BE9336F,
	QuirkySeriesDemo_ChangeShapekey_m97D4F6B5F91F408A66FCB7FFD2DE74EA15117ABF,
	QuirkySeriesDemo_GoToWebsite_m496EFD0D64E397CD015EC4122EF7D778D58F0CE1,
	QuirkySeriesDemo__ctor_m0708F2164E1C1F2AB81B0BA14F9085A1E73C6599,
	RotateOnScroll_Update_mE46306C0A627A240A57F576907537CE452AB7E28,
	RotateOnScroll__ctor_mAC7C1AD78086BD0DDEDF115BBBC008C544CF3B47,
	AI_Movement_Start_m92572122060FC96E299085951E34A0D66D97A9D7,
	AI_Movement_Update_mE02A5C38F41C6D1A4406404D5452257E758DDE7F,
	AI_Movement_ChooseDirection_mB34608D1D225C4A0077E25F67E49463FE8AE3331,
	AI_Movement__ctor_mDD9BFBD7B4A7353BE29D02C3B224CC9DD452F1E7,
	Blueprint__ctor_m29DF651A6C8C9B2B76BC5062C7FBF881CBD82007,
	Constructable_Start_m6E364968F6A936DAD69EF5E1511F4B8A08395C4E,
	Constructable_Update_mF9B64AD64C14660972FF9569DFA7C64358D90383,
	Constructable_OnTriggerEnter_m08203DCC67338E1FB4033EB266F3A00FE98909C1,
	Constructable_OnTriggerExit_m3681F8B59E5BA5ADD3B6EF53775DCF311549DFA0,
	Constructable_SetInvalidColor_m6D1D4172572AF42C322DFB78C947C489BBDEADE6,
	Constructable_SetValidColor_m47D0B59FAE464206DE0CE22AC4547452A000ECF3,
	Constructable_SetDefaultColor_m952BDAE0373EBC8DF8D5DB352130B6509A066854,
	Constructable_ExtractGhostMembers_m6E83619A377A9C76BEC947A0B9FA5B71D127F0F2,
	Constructable__ctor_m0519ABA7B9B748A69C6D17C15E3AFB2DACE8EEEB,
	ConstructionManager_get_Instance_mA6F04AA889A753B3E875CA6FB446F45C7BE48B75,
	ConstructionManager_set_Instance_mC6928E040A34938A8274E41DD98280819A178CA0,
	ConstructionManager_Awake_mB486DDBF1FB096511ACBBD07144FF2982ECCF072,
	ConstructionManager_ActivateConstructionPlacement_m8A31E747C1AC48F1C2DA582A3A42C6A9F1EB81E4,
	ConstructionManager_GetAllGhosts_m180E56CD36DF8A21C9F7BF66177888EBD9E2A974,
	ConstructionManager_PerformGhostDeletionScan_m3D5FD613F1F298F16EE5B7D361FFE45FF2B751AF,
	ConstructionManager_XPositionToAccurateFloat_m9B8B38C899B00FA9F684AEAF2CBFB1DFA9840780,
	ConstructionManager_ZPositionToAccurateFloat_m29659109F4BD500F73A172BED690F1C201CDB610,
	ConstructionManager_Update_mEED81B962801098D4388F50FB89F48D172D1C515,
	ConstructionManager_PlaceItemInGhostPosition_m31A30A04E5297450BB3FDF28260C9620F5D0BE1B,
	ConstructionManager_PlaceItemFreeStyle_m37CC14D0862C1741DE5BD2D4A5A59237FDDA9698,
	ConstructionManager_DestroyItem_mC2CEB497CFA70F12C6FAC81F6FD2276A87359E76,
	ConstructionManager_CheckValidConstructionPosition_m48CC28362CBCFF8A893C9C5F1B02718D5C019CF6,
	ConstructionManager__ctor_m30D1B98B383CC7D5C54F1A8C37E5F45F2BAB6BE6,
	GhostItem_Start_mCFDC822ED48463498392286878682FC15A59CF3F,
	GhostItem_Update_mE0B362B6E8CBD2D0186AB27338D1AC365EEE20A8,
	GhostItem__ctor_m3659200A968AC3F0E1C5983C63F5F703EF1ECC83,
	CaloriesBar_Awake_mB05DF410AACD70A46317E05A01EF6AD89662C488,
	CaloriesBar_Update_m6AF7EF7EDA36D5AB669BC3A6AD1E38B63317A285,
	CaloriesBar__ctor_m6EB7120EE1F5797B161392E31E5D57C9825BE4E1,
	ChoppableTree_Start_m0065824BADB628018FBB6AE9EE93E0312B857C92,
	ChoppableTree_OnTriggerEnter_m843F92C2C5FF87630817841E6C26655672DAA4DE,
	ChoppableTree_OnTriggerExit_m47C18343B31F8AD02504D9CE1B4A06DAC8C3D22C,
	ChoppableTree_GetHit_m2CE97ADD642FA6B4BEC6C365EAE3FF72DAEED5D2,
	ChoppableTree_treeIsDead_mAB1BB216708E4C1CE351DF396A772BB6727E7728,
	ChoppableTree_Update_m650A6A56603AB2869A27ACDA6D936710E4EEAA9C,
	ChoppableTree__ctor_mCA8BD881BD4664BB39130C8CE29933A699E626D3,
	CircleMovement_Start_m21C05D9867F846187CC736CE81FEF598C1CE931A,
	CircleMovement_Update_mB2D3E0502EC5A14FE7DA64188E6B1B2BD66CA628,
	CircleMovement__ctor_m1B8FE3DE581E0F20F6C4F8B44F9F7C2329C18F86,
	ControllMenu_choiMoi_mC702996169C127BE46EEC1D872A6D425CCC3E8D3,
	ControllMenu_Quit_mBCCDBC8B3FB744371445B79A27383B6C8350A51B,
	ControllMenu_thoat_m3FC424C83CD50B856B331B031817C50687E22249,
	ControllMenu__ctor_mAF8358B5CC64C36C311945EE1844DF097E5CA0C6,
	ControllMenu1_choiMoi_m12D676A33E6F0A3E7BDEC9A2F24C652C81291D46,
	ControllMenu1_thoat_mB3E3B3FA88EFAA5468083A1FFD2754E4B90F9665,
	ControllMenu1_toMainMenu_m2EB85A26C37882509092374CDFCAF1E4EC666CBF,
	ControllMenu1_toSettingMenu_m638CB156AFA8269C62270678812794865E155B6A,
	ControllMenu1__ctor_mCB999EDAD568A09C730BC248915FAD37DB058F99,
	CraftingSystem_get_instance_m9593F248B3D6EEA7B80AA3B7FCF876AA44E846F2,
	CraftingSystem_set_instance_mD77417C842A63FD64BDC864EA8717EE7D059A3B2,
	CraftingSystem_Awake_m7A892F1440082227DB7AD26A88BEB9CA4C1C12CB,
	CraftingSystem_Start_m9D06C83690A9ED5A82EA79B88ECBA1A79C99CABA,
	CraftingSystem_OpenToolsCategory_mE382B9D843F9B46E2DD1B30441CE007A94A78A47,
	CraftingSystem_OpenSurvivalCategory_m85234E7A002A5F916A812B1C973D18FEE0F805CE,
	CraftingSystem_OpenRefineCategory_m897DFFFA3302DEE0755A3D70EC5FFCEEACB8DC2A,
	CraftingSystem_OpenConstructionCategory_mDE1646DAB01A8A0155AC2FD34F413B9525F66CC4,
	CraftingSystem_CraftAnyItem_mDCD0FB86F06AE60AC0F49456C1F2351A6D33AAAC,
	CraftingSystem_calculate_m8355A04BF802E2D458848E9D4930D59EB3A1F72A,
	CraftingSystem_craftedDelayForSound_m6647CCCB27E318BC95745DDF6E798BAB77F6143B,
	CraftingSystem_Update_m87A363596B4EDD089E0ACC9324B22E5B32C03C1B,
	CraftingSystem_onCraft_m51E317055A0C863CC45EF2B1FD7BF4982864A08B,
	CraftingSystem_RefreshNeededItems_m903B8F8D973A9B5525A249555DD334BEAD91723F,
	CraftingSystem__ctor_mA635F5340DEFD3ED0C726058910CA4FB84233207,
	CraftingSystem_U3CStartU3Eb__29_0_m54A552C21F4F3C53D30B0E002F90A40AAAA65A56,
	CraftingSystem_U3CStartU3Eb__29_1_m5AE907AD87DAA27A2A8E4FE171144FC52ABF4118,
	CraftingSystem_U3CStartU3Eb__29_2_mCC1B78F48EFB0FAB9B1C06B4543604C95F37CEE5,
	CraftingSystem_U3CStartU3Eb__29_3_m20A89377360F42872B48F0F49AA1722907A4F047,
	CraftingSystem_U3CStartU3Eb__29_4_mA4A8452F855FC2DCD97139CC89379F1A851AC102,
	CraftingSystem_U3CStartU3Eb__29_5_m4901D723F18AB4CBE961BB55FD280A4C7149A275,
	CraftingSystem_U3CStartU3Eb__29_6_mD9BE6093408D4AD3473E9497437981DED44DDE30,
	CraftingSystem_U3CStartU3Eb__29_7_m6EE2D9A7C9214E6FB6BD3769942A88C55FDA499F,
	U3CcalculateU3Ed__35__ctor_m64A8ADEC6B7CACC99228C5CCF90D7CD614B44F2A,
	U3CcalculateU3Ed__35_System_IDisposable_Dispose_mC468F7AB616EEF078F53697549522E274E78AA70,
	U3CcalculateU3Ed__35_MoveNext_m477892FD58209B35885AE52AEF2FECB611F476A7,
	U3CcalculateU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m83EECEE35820BA91456B47775E9EBAA669FC21C0,
	U3CcalculateU3Ed__35_System_Collections_IEnumerator_Reset_mB2D7CC0AB280B1119F91C70A60AFED37AF6FE30C,
	U3CcalculateU3Ed__35_System_Collections_IEnumerator_get_Current_mD31488D38404FFA813FD57A40D387AD8388DEC36,
	U3CcraftedDelayForSoundU3Ed__36__ctor_mE036DE455AFA1540452EDD0F135F6E09C3385B0A,
	U3CcraftedDelayForSoundU3Ed__36_System_IDisposable_Dispose_mFD0DEF72FD0E913C76D7BBA680BC5A29BB36ECD1,
	U3CcraftedDelayForSoundU3Ed__36_MoveNext_mE751BECCD30AC4D8CB2F65EA40EAF07DB9E92990,
	U3CcraftedDelayForSoundU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m980A3F556F00B73336E1A73C1869AE3FFE06A800,
	U3CcraftedDelayForSoundU3Ed__36_System_Collections_IEnumerator_Reset_m43430CA9041C4E783A6B4373C67131E546E18C8F,
	U3CcraftedDelayForSoundU3Ed__36_System_Collections_IEnumerator_get_Current_mE4F12D5040BEDC69C2D31995C0C6213A8923AB7F,
	DayNightCycle_Start_mEBC69829A9266BB95FA3439A12DD170B6CCA4EFC,
	DayNightCycle_Update_m93263D9CE1D7218754CD5AB4ABF1FB8D63C8ECC5,
	DayNightCycle__ctor_mA2B6C9B178964D2867D717FCDCD1A4539370C38E,
	DragDrop_Awake_m3D9656016C42E05B0F64F12CCBE69AF06E40BBB4,
	DragDrop_OnBeginDrag_m7969B96669116003DCA318A99818A1215DF6AC2C,
	DragDrop_OnDrag_m66843E500FB5601B0C526881781E743DACA78302,
	DragDrop_OnEndDrag_m30CB848DAE1C7CF2912120A49C20B5617E76C448,
	DragDrop__ctor_mE5A2331828F64E1B46DA7C6AC3CC64A1D6721707,
	EquipableItem_Start_m990822285B4BDC81B1F765DE8ED87260D607C0BD,
	EquipableItem_Update_mFD9342585CF35EF348C99A5C85E0A1F364E73759,
	EquipableItem_GetHit_mD8D433D14E14347E0511199E7F4A068DED941C41,
	EquipableItem_SwingSoundDelay_mCF8EDF69F17B6878F21F953EB624902CF4F4D9F9,
	EquipableItem__ctor_m381DFC1B7500595871E06E6685577B5741BDDEAD,
	U3CSwingSoundDelayU3Ed__4__ctor_m5B281461312D8735C15679B0A49BAC39B963DDEB,
	U3CSwingSoundDelayU3Ed__4_System_IDisposable_Dispose_m7108440ED3D2DA311CC7EA2D76CD9792B0EB9CD2,
	U3CSwingSoundDelayU3Ed__4_MoveNext_mEDC318F55BCB9A37CCF133570CCF28B2DD1B168B,
	U3CSwingSoundDelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8D121DF3DA9B779DA2FD74F8BBB0E20805124A7,
	U3CSwingSoundDelayU3Ed__4_System_Collections_IEnumerator_Reset_m2CDAC5D0BAB7D9B1BA4C63311DA146E588D815ED,
	U3CSwingSoundDelayU3Ed__4_System_Collections_IEnumerator_get_Current_m4B1E6271EA1CC237066A24DBAFA264CED342987C,
	EquipSystem_get_Instance_mE74148BB6496B9C7D7340B2F6229EBCB660DB75A,
	EquipSystem_set_Instance_m4756034B115B194CFFEAE37A7EBDDAAB3E1C385F,
	EquipSystem_Awake_m0728040A99A2E0C6ED340AFA12625467A1895D3E,
	EquipSystem_Start_m3E2B6440FF0574A4F435325CA085E78E249D2530,
	EquipSystem_Update_m674AFF858E7981FFCBC18144631BC28E524BA9C3,
	EquipSystem_SelectQuickSlot_mDEAC5DDDCCEB4BA2971007BB70F28A0E6223CA75,
	EquipSystem_SetEquippedModel_m319884207FB0AF920B561B5BA7BF0A80F4D0AF77,
	EquipSystem_getSelectedItem_m3A23A463D83A3E25FEC0F10B95E197BD4A9AF104,
	EquipSystem_checkIfSlotIsFull_m04EEF5549FA7CDA3DB31C7AACBA91EA0688C9E9E,
	EquipSystem_PopulateSlotList_mAF977CB7E0E8810BCBBE2B4D03821C74A7646B01,
	EquipSystem_AddToQuickSlots_m20B57EA918A4B5AE2C19371EF5C37BA2EB49DFFE,
	EquipSystem_FindNextEmptySlot_m6C1C16DABCD15E3B8028DA753D074BDEBA648701,
	EquipSystem_CheckIfFull_m1E50C1D9CDD9736C0642BC3617607E4FE0AC566D,
	EquipSystem__ctor_m70E1419644CB102B3047E7CFD6FD63FFE2F23D8C,
	GlobalState_get_Instance_mC0D9E0D6620185B77A070D8E71BCB253700CCEB7,
	GlobalState_set_Instance_mF9918E2BF57F8D8FC2BFEF8A78F9E36A588DBB15,
	GlobalState_Awake_m00C055E230885B69230D401842BEC2F925AEDD0E,
	GlobalState__ctor_m6A35850A8D251697ED58A2F94812DDE0E86813F5,
	HealthBar_Awake_mD335445D153349F1E440D2FAAA2AC3CEC8803ED2,
	HealthBar_Update_mD3754EFEE710D376C4EEAA8D71E90B456C5E9AFD,
	HealthBar__ctor_m6874A2796BC8D86E80B24E349500653ACFA80662,
	HydrationBar_Awake_mB053E895C4B0A107AD05B84EDBE74678323A29BD,
	HydrationBar_Update_mE3D614F7D8FB416D7586640D2604BB1C630C5025,
	HydrationBar__ctor_mF3F95E14303BE4749559E26BAE5F0B0C721E3141,
	InteractableObject_GetItemName_m9D1EEC4B43093BA55C86CEEA17D7517A47DC2BD9,
	InteractableObject_Update_mC1B0F9850D186FFF22389540655B9356F0B246AE,
	InteractableObject_OnTriggerEnter_m51A41E9533CC7BE7E1C632FD61A3F6671028175D,
	InteractableObject_OnTriggerExit_mF0831C00D61A47FD1B23AB3E6FF6227A8ECE760C,
	InteractableObject__ctor_m2349D95BF7851F97EB4CCD417B9C97041AED0A17,
	IntroGame_Start_mD21A1781443897F84ACBB3A1FD0A62A92AAFDA68,
	IntroGame_Update_m9C6D5C4B80C4B7AAD8429309FFD572D177EBAB39,
	IntroGame_popupIntroA_m4F3C899060141B29470884B9E802439D56019612,
	IntroGame_popupIntroB_m916F73F41E3A4063A827CA9A1365612E9E0A2E85,
	IntroGame_popupIntroC_m13102B4CBF3B67FF3FAB4BD946795F737F393D10,
	IntroGame_popupIntroD_m6F4DA272B0409767A03141F05AC6D15EF1EB4D2B,
	IntroGame_popupIntroE_m2B5A1BED228D0B07A367E6B4BBB86E64610E4B9A,
	IntroGame_popupIntroF_m6F27C1E0FE0CF6BF0BD098A72BB37461B716C050,
	IntroGame_quitIntro_m99FF0D478FB443DF6D55190F0F6240A31D77F7E7,
	IntroGame__ctor_mD13DC8E8EEF690D18715893EE039F0B1D902F35F,
	InventoryItem_Start_mB9C5506703D7C324664AAE562DE4FF0B544E7E3F,
	InventoryItem_Update_m32C5FB14B4BB3A88AD3F5EC922A7FBAAA0403F51,
	InventoryItem_OnPointerEnter_m6BD43353CB537D89464DD294E96D2712ECBC69FE,
	InventoryItem_OnPointerExit_m62157E08E9C5D02CB9BCD7D48C61622F71EA0172,
	InventoryItem_OnPointerDown_mD86D1152600ADBED0AE1E6ECCB339D0A20984CDB,
	InventoryItem_OnPointerUp_m8ADF1669DC139D774B429E1FE41BA82BA124893C,
	InventoryItem_UseItem_m7DB0BBBC78DCCBAA1E6DF2F29B2008632B499832,
	InventoryItem_consumingFunction_mC2B9102BD4FBC5BFBB3FEFB58B62FA1760633BC8,
	InventoryItem_healthEffectCalculation_m7C064A45674AADB84650CA5252C45B225B59FBD2,
	InventoryItem_caloriesEffectCalculation_m565D1D4BD5BB1453FEB572BC37667EE2192C0640,
	InventoryItem_hydrationEffectCalculation_m35C04D6692AB4966A24F46016DE738AE6B2BC334,
	InventoryItem__ctor_m775807C9917B41C904F5B88DD55F6771AC7A8AB5,
	InventorySystem_get_Instance_mB6754F78EDB5BD12D53AF447D7619A2F4F95EB3F,
	InventorySystem_set_Instance_m21E25C52FC950C0920735724DDA41BFB70DD582F,
	InventorySystem_Awake_mB6C33C30DC735179B928B792EB8B8220EAE2C7B3,
	InventorySystem_Start_mEF024BBA06D3C3C9762B4E8415BF35CCA6E3B907,
	InventorySystem_PopulateSlotList_mE30A4F1661DB5554B4572CA932D8EB2EC661C7E1,
	InventorySystem_Update_m62B5F2641E3DF8D61AF348C70FC1D19157A93DE7,
	InventorySystem_onCursor_mC7A868AA6BCB850B7A6299E3E178CE81BF44D1BD,
	InventorySystem_offCursor_m3A2C2B5396D8D1514512490120C1C282E9EA6384,
	InventorySystem_onBag_mC7AB36BD26CBCB61335A5915BCF56C14A7A8EB80,
	InventorySystem_AddToInventory_m6DAE590D5699C39DC57FC76EF76D2643363CB7D2,
	InventorySystem_TriggerPickupPopUp_m9B8CD2982FEBF9CCD9A01A4765BE311BB3CB5BBE,
	InventorySystem_TriggerOffPopUp_mE8C4F716AE8FFB150D77D074B5C168547F3FFD82,
	InventorySystem_TriggerIsFullPopup_m5F134A42AD461B11A0418F8E8E2CB69DC31314C2,
	InventorySystem_TriggerOffIsFullPopup_mA2ABAADC926FABE42844AF00238F9D3BF475509B,
	InventorySystem_CheckSlotsAvailable_mF317C7813A1A783AFC31F2EE3EA3AC18F35294DB,
	InventorySystem_FindNextEmptySlot_mDED79AC1F99B09059DF534344618B475C226790F,
	InventorySystem_RemoveItem_m54277B050AFE146336707738BDFAEF55DFB4190B,
	InventorySystem_ReCalculateList_mBEC98715D31F7ED4AABA42008A51D0BD710B01A0,
	InventorySystem__ctor_m6941EDD5EB50BD476EB6D26671D3CD7E9C09D976,
	ItemSlot_get_Item_mBC890648338DC27ADE77C882A4398AFCBF470098,
	ItemSlot_OnDrop_mA85B22B894F538E98322BF1A3C3C972F7681FF95,
	ItemSlot__ctor_mAAA0B63EB83F00953BB1E7E67F72C2DC09B60620,
	MouseMovement_Start_m05EAE2791625108B3410AF53784B704477173D73,
	MouseMovement_Update_m8AB36A8D7BD05E1D4F3F7DAF92CA32DBFAE7D708,
	MouseMovement__ctor_m0EDD6A5D9AEA3DAAC726E751BEBE97919FA38C00,
	PlayerMovement_Start_m83FD44DCA324CE3D05A71FD2E2991FCD743F003A,
	PlayerMovement_Update_m5BB6CE35AF68EE00CFEB4BA5EBA17E10667551D3,
	PlayerMovement__ctor_mB37559C5B0638161878D20E00B7C672FC38BBBAA,
	PlayerState_get_Instance_mC7ADF0C9DB4050A1402E9913BF6F41E575072394,
	PlayerState_set_Instance_m52BDFC6E36A069269C103E53873260AFD84B0163,
	PlayerState_Awake_m5CE90490C1A2EEFE675F99D2AD643F84408621BF,
	PlayerState_Start_mE51FD39D15383B0F4065010AA6AFC9D176CE1768,
	PlayerState_decreaseHydration_m53B04A26BA2E20A54A38F9DACCBF678F68EA0D10,
	PlayerState_Update_m0877F4235CA36008E62785240D1824BF4EB00636,
	PlayerState_setHealth_m13E312FCAE519FB567130C0F626B541539AA1626,
	PlayerState_setCalories_mA8819A01F0F11E404139D67DE60C5FD66A7E1296,
	PlayerState_setHydration_m127DF1E18A14832587ECAB3CE6CC11C014D526A6,
	PlayerState__ctor_m3D86839408DEC12A9FBDF17CB95C7CE18CCA8190,
	U3CdecreaseHydrationU3Ed__16__ctor_m312D32504E6C2C8784EDE06756534AA4262CBF89,
	U3CdecreaseHydrationU3Ed__16_System_IDisposable_Dispose_m781C63AF4758FBF1A71AF67D9C6D11410CF500AE,
	U3CdecreaseHydrationU3Ed__16_MoveNext_m37738919F6D3967828337D6ED6A118D3BC18D64C,
	U3CdecreaseHydrationU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCCCD2E46848BEBDB8C20DC1579C00FEB798235CB,
	U3CdecreaseHydrationU3Ed__16_System_Collections_IEnumerator_Reset_m49E7F143F161E82B4B2BC27A5B5D42BE2B205D74,
	U3CdecreaseHydrationU3Ed__16_System_Collections_IEnumerator_get_Current_m35979710E4F8803810D01BAD352146B01809DAD6,
	ResourceHealthBar_Awake_m391500609A352FBAD484BD74F845B45626134048,
	ResourceHealthBar_Update_mD819E1AD81B63C296CC25F0F59DD741FEE25F182,
	ResourceHealthBar__ctor_m30AF3EBC605792217B4BA0B6E61CA474C141108A,
	SelectionManager_get_Instance_mD229898374797B5EFE4028726BF3AB6BCEE6B5F7,
	SelectionManager_set_Instance_m15A9E41EFBF7EC08DCF9591C03202D820B2D80BC,
	SelectionManager_Start_m28F2F9098BC42632CA2B43A635A37943D700CC96,
	SelectionManager_Awake_mBA351387D1889A767AFD8E6649081BC9EA97899C,
	SelectionManager_Update_mCE1890549CD6485487E7BFDB43460D2BC0DE1404,
	SelectionManager_onPause_m7A8FF3DC7EC1A0218E231E1FD45DD0BB74E16D96,
	SelectionManager_DisableSelection_m32B7BC50828154CCAC99AFB0AB07E2A90218C5BF,
	SelectionManager_EnableSelection_m1AB180C14FE3F53D89880D1127B4C0DBEE986595,
	SelectionManager__ctor_mB63C19EF82E0D6901A11D7794718430F86DA8E44,
	SkyboxRotator_Update_mFE56F58DC98F7F18F15998B18FED505A0B82B41A,
	SkyboxRotator__ctor_mB8D77845ADC494953A08C671652C80B2B49DF111,
	SoundManager_get_Instance_mE37BC6A788B14F6895B2B50A4E461F8C7895BAEA,
	SoundManager_set_Instance_m93AE933900CE8CB03F41EB983F159CCE87BF2207,
	SoundManager_Awake_mEB5694CE6F2913D14C32C4AF41C936AA76007825,
	SoundManager_PlaySound_mC6BB918C6F664FF2859AC7D164A9BFD6085E1F95,
	SoundManager__ctor_m27816732AF730AF6BEDE4A67ABC9D1A094777213,
	TrashSlot_get_draggedItem_m9A372F25212685524A375DD151D4268A75C9B092,
	TrashSlot_get_itemName_m6A49AC7C63B767200E42DE85C1922E8D4918CD9D,
	TrashSlot_Start_mEA7AF57899B0B0BFAE0FE51241A906863267ACD5,
	TrashSlot_OnDrop_m3EA13F857D82DC3B6091919D2ACFE9B87E3BC924,
	TrashSlot_notifyBeforeDeletion_m379A722E56CBAED4CEBB19BA7E9DD69CD42AB1FC,
	TrashSlot_CancelDeletion_mE660D67AF4D0500055A50C5836AE313D13F4249A,
	TrashSlot_DeleteItem_mD7E735F941D0CB593B98C37F0C629D221965D4C9,
	TrashSlot_OnPointerEnter_m589F3C355AEF7C7A1F3D0802BB0BFBA966EF9106,
	TrashSlot_OnPointerExit_m4C04E2B1C72BD1C1AA4F99B723F9FDEBD11AF28E,
	TrashSlot__ctor_m2C21D4DAB8DE561A86A2796BCB981B33FD2E22B3,
	TrashSlot_U3CStartU3Eb__12_0_mA2453A739F9964EF9DC347CDD5BA985270439C30,
	TrashSlot_U3CStartU3Eb__12_1_m4A027CC9528374F12A8841CF5E70719DE6AE23D1,
	U3CnotifyBeforeDeletionU3Ed__14__ctor_m4409832B9C192EC4569A0CEF4F613A7E0DBE292F,
	U3CnotifyBeforeDeletionU3Ed__14_System_IDisposable_Dispose_mDE44F645444FA8BD8ED64B66ECD439964FEB4E42,
	U3CnotifyBeforeDeletionU3Ed__14_MoveNext_m2265DF53FED029DD851B8681F4EED0EEF5F1A779,
	U3CnotifyBeforeDeletionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDD16BDE03D00ADF3C2F689A45CCC3B02FC906A0,
	U3CnotifyBeforeDeletionU3Ed__14_System_Collections_IEnumerator_Reset_m878613246A19363A20697FF465A492D2FC6BE93C,
	U3CnotifyBeforeDeletionU3Ed__14_System_Collections_IEnumerator_get_Current_mC27EC02BD35CF3111A130CB54655EC71FA4DA929,
	WaterSoundControl_Start_m871C2B88E8602048D31AF5F81D294F10F9754E17,
	WaterSoundControl_Update_m754E7BDB22D7D34240E723CC741D361D8C17EFDD,
	WaterSoundControl_OnTriggerEnter_m5D729D9C9F5A35FE83311932AABA4A0EF835F99F,
	WaterSoundControl_OnTriggerExit_m98E4E5B93778FDE2E3FB4E485EA9AF4A2B992225,
	WaterSoundControl__ctor_m50CA1F0AD967AB96355B25DCE148CA51E111BCCA,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F,
	DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C,
	U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189,
	TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475,
	TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33,
	TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6,
	TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D,
	U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC,
	U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB,
	SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51,
	U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2,
	U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A,
	TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52,
	TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B,
	U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186,
	U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5,
	U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55,
	TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE,
	TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215,
	TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79,
	TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559,
	TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8,
	VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77,
	VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D,
	VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7,
	VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF,
	WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53,
	U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19,
	AnimatorParamatersChange_Start_m871E4A208B70403948907DD6EFF16B5C89AF6558,
	AnimatorParamatersChange_Update_mB838E976A19CD1C3F3FE31502501C8D442DCBBCD,
	AnimatorParamatersChange_OnGUI_m240E5495716AFA707AEA5F259ACB8B4F85E0B3FD,
	AnimatorParamatersChange__ctor_m4C2265B2747B0F180A4CC7EDFCC2F516838CC1AF,
	Antialiasing_CurrentAAMaterial_m616ACC65D35018EF4D483D5806041ECD09E2935A,
	Antialiasing_CheckResources_mAEDFCD3A79F2A463C14675E8A517A2A8CB0B1AA4,
	Antialiasing_OnRenderImage_m43277916DED7A82DA3C7B76B880B3241A22C08C3,
	Antialiasing__ctor_m462AB00D4D7D5B984126BD14A6B197FB7713E28A,
	Bloom_CheckResources_mE5E995DC8F2EAF421C6231A991FC21E05BF19A3E,
	Bloom_OnRenderImage_m91D52D682467DE9C1ED3D38A617EDDC3DEDB4542,
	Bloom_AddTo_mBC2B4CB5D7085DE45E9087794088E0156293249B,
	Bloom_BlendFlares_mA45BC26BE27BBFF337D7091303DAE4C6BF99D3F1,
	Bloom_BrightFilter_mC7413A396C92E39BEADDDF0BD1D6C875B9619CEA,
	Bloom_BrightFilter_m02A1C963E82D3EC39D46953DA82953B5A011E863,
	Bloom_Vignette_mD5A802FBE6716C0B281883004266778F37F0066A,
	Bloom__ctor_m42F7BAB1B3812EBB56DEBAB04ED8AE14D2C22D10,
	BloomAndFlares_CheckResources_m25E72D332C6B7E4BA07D091336B9A07A215ABD9D,
	BloomAndFlares_OnRenderImage_m3A0E0EEAB00CB8961754815CDCD3B751407453A9,
	BloomAndFlares_AddTo_mDF8D4610504FF933746ECE9149009899202A0D2D,
	BloomAndFlares_BlendFlares_m8EE10767AE510FD6B6C8E34A87B94526EAEA9911,
	BloomAndFlares_BrightFilter_m68F28105229213BD4C24ED26E768BAFF50E1D941,
	BloomAndFlares_Vignette_mDF8148D41EE021A5C506A74749FBE3AA4496D7FB,
	BloomAndFlares__ctor_mF41ECDDA3A818081EADD14391504A77C1BC317B2,
	BloomOptimized_CheckResources_m108A809CE36BF4F07ADDC9D055A32F046085C8A1,
	BloomOptimized_OnDisable_m39774D4731E12D5B3D97815705D8E56CBF1DEB9C,
	BloomOptimized_OnRenderImage_m866388BBDF62234065C6D89F692044E92A85447D,
	BloomOptimized__ctor_mEA93BDFF232E2DDE86C49AFFD8884CD68196A93A,
	Blur_get_material_mB9BCB7739A59468A30B5D1EEA39389CEB68DE6A1,
	Blur_OnDisable_m5C34BC91F6C2FB85142D81CA022522641A8A56C6,
	Blur_Start_m7B0242EF7E086AF91E633384FBD3818C5BEC07EA,
	Blur_FourTapCone_m089475C2D8D118A4F7949669DEC3A562BEED4D46,
	Blur_DownSample4x_mB0689283883431638F88A750CD4BCA695470C38A,
	Blur_OnRenderImage_m8303C27411C4F059DCCCCBAB72F4C93F93BD87A3,
	Blur__ctor_m7F3FDA508B9E862A06AC0A7E914CE8239116317F,
	BlurOptimized_CheckResources_mC32D0A636D288E64F2E12FD1F08DAA4FBEFECF69,
	BlurOptimized_OnDisable_m0E9A2DF9846B126BC9533814F82748ADCCE0615A,
	BlurOptimized_OnRenderImage_m9E9D0E2B1210867A24E2198D214E0CF07D8356EB,
	BlurOptimized__ctor_m6DB6B84228DCD3F15ECAE0FFD874EE418C2ECD48,
	CameraMotionBlur_CalculateViewProjection_m89B118B051F4C5CE754F151C6A5D4996D22C5E4A,
	CameraMotionBlur_Start_mF97E62379A2265CC081CCD96D38156BEDCBB51A7,
	CameraMotionBlur_OnEnable_m5F2A506FAD0EF9C5D9339C4604259D69BA464694,
	CameraMotionBlur_OnDisable_m2BAFA146755B557A389C1231798293BE51F7114F,
	CameraMotionBlur_CheckResources_mC74FE9882CB13291322CCA5340A0EFAA2CCB6EDA,
	CameraMotionBlur_OnRenderImage_mB237BC2F5A2822AA3071639C2AE7B857CD3E965F,
	CameraMotionBlur_Remember_mE2C9E38314CF73169C273DA569280DDEF41B6151,
	CameraMotionBlur_GetTmpCam_mDD546C874BFC169F515E42A151ABDC3CDB379254,
	CameraMotionBlur_StartFrame_m1EFBDB22B9DAA74E4A7F96FFC46095A7FF0B33F9,
	CameraMotionBlur_divRoundUp_mEB15E394BD9A589D23F3F32DFD6D9C5B66D3B983,
	CameraMotionBlur__ctor_mBF3F5856FDDD16AF94E45A37862AFB8E559D22C4,
	CameraMotionBlur__cctor_mB9C329A189E7239A01C70A4338520905E004763D,
	ColorCorrectionCurves_Start_m8BEEAC0FA96F6180735794867AD2E152AA3B9178,
	ColorCorrectionCurves_Awake_mEBEDC0872067064FA5CCC7F4AB1656532C100456,
	ColorCorrectionCurves_CheckResources_m2E51D375CAD799867434AA46FF1CC9D69A3C3830,
	ColorCorrectionCurves_UpdateParameters_mAC7A0DD437ADD29E631D1DE306DEC09269D9636B,
	ColorCorrectionCurves_UpdateTextures_mDB0CE54845439B6942B3AF2086BF0ED6B15E68BA,
	ColorCorrectionCurves_OnRenderImage_m5451EA9A29065B6AE78C281A834722BB5FD8CF90,
	ColorCorrectionCurves__ctor_m57B1F217548D607FEBB56163AAB246509FBAC9B3,
	ColorCorrectionLookup_CheckResources_mE798BD5135D4A4AE1B418A55F8A4C7091B4989C3,
	ColorCorrectionLookup_OnDisable_mF964C0BF7C7181BF40C6B9A652AABD182BB58A2F,
	ColorCorrectionLookup_OnDestroy_m916EFAECBAE25947B02CEC323BD69FB406B5250A,
	ColorCorrectionLookup_SetIdentityLut_m6A79565B09A07036C767AF1A16C274500AC130D3,
	ColorCorrectionLookup_ValidDimensions_m41CA8EF5DD8281804F2E091A92495D56940CFBEF,
	ColorCorrectionLookup_Convert_mF4235B7639E43DF5A2C309C167B81D77EE517111,
	ColorCorrectionLookup_OnRenderImage_m0545AE128DF587EC53F9C802F13F7892922B7DAF,
	ColorCorrectionLookup__ctor_mB7DB4D90090C593309EE6822E9D14F51A89423B8,
	ColorCorrectionRamp_OnRenderImage_m489A2D7B3567FA0E26B4E315BA1F95E1B92C6093,
	ColorCorrectionRamp__ctor_mF3F11103A22E62E20914697B24B975E1DBBD00B2,
	ContrastEnhance_CheckResources_m8CBC6E6E65DA28E6A5EA2B6D235583C54C6DE5D8,
	ContrastEnhance_OnRenderImage_mC9521D66DF962B0DBA263DA937E3F8C37AFF00DD,
	ContrastEnhance__ctor_mA046704E2483DBB3080A4FE34D27FD03D774F02B,
	ContrastStretch_get_materialLum_m3E0BCDCD06A97473C3F509D6E47C5C937E0C5630,
	ContrastStretch_get_materialReduce_mFF50C7EA9BD50C89DB513F8EEFFE21137F8C47E5,
	ContrastStretch_get_materialAdapt_mA286A06F5BF2CBCAE2B2EB8F2074D43C927FBD7E,
	ContrastStretch_get_materialApply_mB7339208DD2EF717DA91766024071E1D8E670912,
	ContrastStretch_Start_mD99DCAF9FF50BC7C76F32AB250EA6414767CF365,
	ContrastStretch_OnEnable_m3D4DF80A85246544A3A585D787E9F864F529379A,
	ContrastStretch_OnDisable_mFF56DEE2F8871A1C5FDEDF1F39B09806835594B7,
	ContrastStretch_OnRenderImage_mCBFF98D8D7AF36A66CA40088F65D667C00CBC411,
	ContrastStretch_CalculateAdaptation_m801A05FEDB49C0586E503F53EBCD36F7B755170D,
	ContrastStretch__ctor_mDDD983556852FAFF344F2033C8AEAF43DEB21B55,
	CreaseShading_CheckResources_m11195082724590FFA17545626F44D62DE9E5BAFE,
	CreaseShading_OnRenderImage_m7ABCB39ABC2AEDDBE2BC38258B1CC7DF3C603003,
	CreaseShading__ctor_mB81EC56595A322BFD46F7B6ABB77B11834AEBD95,
	DepthOfField_CheckResources_m5B73E318E80ADCBFA181C196B658C8F2EF3EB87D,
	DepthOfField_OnEnable_mF22CEEE7130A0FD5B717C4FE268CB4708A67A416,
	DepthOfField_OnDisable_m6A4C40367E5041B08FDFFCB9AF9DF0EC4707C71A,
	DepthOfField_ReleaseComputeResources_mE39DFC9B512EE74FCCC7D0A58828CDF2D446FB1F,
	DepthOfField_CreateComputeResources_m44A3D7A9CA59140FB293D9FD3FF0B1E9B9231F19,
	DepthOfField_FocalDistance01_m9753A71BBBDFE2BB44294D4506A2D6BDEF77ED9D,
	DepthOfField_WriteCoc_mE94B9C8FD075063F31E2B906B329CB7C4BB6F8B1,
	DepthOfField_OnRenderImage_mAA557E588FE92346DF0FEC7043A9BEABC78857F0,
	DepthOfField__ctor_m2A265D1437B0AAA1065A785E8428E162552F0393,
	DepthOfFieldDeprecated_CreateMaterials_m3DFE5FBA8B2F6DF52BC208205A3945454479890B,
	DepthOfFieldDeprecated_CheckResources_mE8F5F2654CF5F3668BDCAC07E5022170055BD07F,
	DepthOfFieldDeprecated_OnDisable_m3E3D5ACFF1FDC10FD900F54C5D9CBF2BEE920D4E,
	DepthOfFieldDeprecated_OnEnable_m86B858F3493ADC8E9F49BCFD33E54AF51D103561,
	DepthOfFieldDeprecated_FocalDistance01_mC2B55F876AFE563170E6F57211078C5A5D89C5A8,
	DepthOfFieldDeprecated_GetDividerBasedOnQuality_m80B6BF4C7B3249E986BEF7D8D8D2B0DE201067AB,
	DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m985CDBEC2CF43F2E8907FA1FE19D3D127C170CB0,
	DepthOfFieldDeprecated_OnRenderImage_m65BD2421A8466090B3329A2E3D9DD982A436282C,
	DepthOfFieldDeprecated_Blur_mA2B98FF3332C7AD3D25634AF426A6732768CE451,
	DepthOfFieldDeprecated_BlurFg_m9EC1D58A51FA15F10BD6B6F201354D5F5942FD61,
	DepthOfFieldDeprecated_BlurHex_mA06B816018A052277D95895580456609D2FB4855,
	DepthOfFieldDeprecated_Downsample_mECCC2FD41C3593F6C4881D392F9B0CE21FB56CCC,
	DepthOfFieldDeprecated_AddBokeh_m5C7284DF55D8065B3F01A41D8F699A0D79E33077,
	DepthOfFieldDeprecated_ReleaseTextures_m3F4F40F3CAEB5D02B1EB43C65457B811CCF61FA4,
	DepthOfFieldDeprecated_AllocateTextures_m26D14CBF056C12BA4CC153913DBDDBE9BFB29B0D,
	DepthOfFieldDeprecated__ctor_m6B6B37E85FB0EA2831AE0CA05FD781FAEE5DA0BF,
	DepthOfFieldDeprecated__cctor_m3FA1672494E284D85C238AE853FDAC4C596326B6,
	EdgeDetection_CheckResources_m844678E40D8131D435B81A85884316C437AA33C3,
	EdgeDetection_Start_mB5C5CBF5FB91B71A0E44915A297D76B8A936F5D8,
	EdgeDetection_SetCameraFlag_mFBFBA5CBA1DD869326CB998571155DEB1147FB7C,
	EdgeDetection_OnEnable_mA5DAB7483904036D88EB9EB54693AF7212C2AB3E,
	EdgeDetection_OnRenderImage_m38E961698AF5A6CD1FB1F3646B6E4EBB78260DD1,
	EdgeDetection__ctor_m0D04687855A216627755DA1FFA8688A69A46FDE8,
	Fisheye_CheckResources_m6FFFA5A2454821C9C998FFEFE2FB212D2BC4D9D8,
	Fisheye_OnRenderImage_m28DBEA9F277263BF7E11C4A3DF06B713E505254B,
	Fisheye__ctor_m8A4F67E1FC937F5867702B596171FCEB427BF3E5,
	GlobalFog_CheckResources_m9F40B749F22806F92122CF5CECE86612E6FA2E53,
	GlobalFog_OnRenderImage_mE19FBCBCFB6223066FA043162F3DB09DCE56CF5B,
	GlobalFog__ctor_m1C6432E5A10611049D2CFE56B4B8FE03D0560B7F,
	Grayscale_OnRenderImage_mFD132F2F2E0CA27ED9CCD86C1FF19D1F6C3CB4C0,
	Grayscale__ctor_m01B85C45849A4473DEC2FA28A3DBB920D46F22DC,
	ImageEffectBase_Start_mAAC05E3ED1455E1CB4C5BCB5AB9414CEAF18EB8B,
	ImageEffectBase_get_material_mB006F34B91F5520C2F543594586F8456AB2CB024,
	ImageEffectBase_OnDisable_m521F62E819A026FE6946761CC76A7C7F7C8AD014,
	ImageEffectBase__ctor_mD934C852B973B7EB2FA5CD62E6D0BD7135D0A42B,
	ImageEffects_RenderDistortion_mB9B8E1ABEC327C7EE2387A51108C4F6B972561BC,
	ImageEffects_Blit_m8EA90C6A40EF19EE94B174AD10ABBFE01735FB6D,
	ImageEffects_BlitWithMaterial_m52DEE89A5B2726D9F0E3B88DD2D9681747BE0417,
	ImageEffects__ctor_mA575B9A385AEA923FDB67680C6BBEF06D50A8649,
	MotionBlur_Start_mE939F398C1D7025D5F95CAA5BB7B51ACBBEAB0CB,
	MotionBlur_OnDisable_m524F34F73889C816D7CC89E09B165FD13E403B4E,
	MotionBlur_OnRenderImage_m7591D001CA3CFC52F5BBAA2C23E4B46C79BA04A6,
	MotionBlur__ctor_m99420C3172B7C4F8AF1BCD99E2B57AA26FAF564F,
	NoiseAndGrain_CheckResources_mB08923E967617EB5023BC61CF41C28F2D453F1D8,
	NoiseAndGrain_OnRenderImage_m92D9F85A52979B12AB52AD9074CB23E86EC71C3D,
	NoiseAndGrain_DrawNoiseQuadGrid_mB378C3977FE3F97AF4168FE05DEB395F8A0491D6,
	NoiseAndGrain__ctor_mFDE0C9E284B4E9B63B3DD335020EA3B4723BED70,
	NoiseAndGrain__cctor_mB399C524E753D4475ACA9091FC4B23506BC5DE8C,
	NoiseAndScratches_Start_mEBF2EF7B65C924EA3C9AFC1DDE2817A3766D0D48,
	NoiseAndScratches_get_material_m45C5A28EBCBB71E6599FC49511E1AB3A9338D585,
	NoiseAndScratches_OnDisable_m545D985962BE453BB260CE23FE2C0AA899115975,
	NoiseAndScratches_SanitizeParameters_m8B9C3842DAD850EBB04F3EAE8D41D4349AC96D3D,
	NoiseAndScratches_OnRenderImage_m8CA535280A5FF3E201006E0F2D1CDD6F7E5FD426,
	NoiseAndScratches__ctor_m8A6D067A18DF0EA3F2B27919AF6AD21DB55093EB,
	PostEffectsBase_CheckShaderAndCreateMaterial_m05677F5055873EAB286B69870015998FBB9D6927,
	PostEffectsBase_CreateMaterial_mC9860A23866B8230D964BC09EDF7FD2878BA0D6C,
	PostEffectsBase_OnEnable_m8BC44BA7499D510F0A9887FECDC2C475C3EC159A,
	PostEffectsBase_OnDestroy_m543FD01209891987E236BEBB1EB25FFCF595DCAB,
	PostEffectsBase_RemoveCreatedMaterials_m5F54C33DA625B453B6737FDEF70DBCC7912371C7,
	PostEffectsBase_CheckSupport_m059D1547A9DFFFF9D18D2B702D9C1A98871B3689,
	PostEffectsBase_CheckResources_m01EFEABDDEECF8931BDE27F6D76536FD52C2E273,
	PostEffectsBase_Start_m256D1B7C8E17442D85DCCD08D6D157080A89B79C,
	PostEffectsBase_CheckSupport_m97A1AE9C8FAFEA8D8FF7FA69916F388ACCA9FB34,
	PostEffectsBase_CheckSupport_mA7398DAA19FD5AC947119F26E3509974EAFC1664,
	PostEffectsBase_Dx11Support_mCAEFE6A80827A8B34E73B44DA8304C0CAD8330E5,
	PostEffectsBase_ReportAutoDisable_m4A617083CFD2A09EEECF6329CD2C9AD101095E34,
	PostEffectsBase_CheckShader_m2915D7EE44889306449307C06FE3F9770FAAF012,
	PostEffectsBase_NotSupported_m3328CFF3B6C73682EAC7141F15716F34940D86AD,
	PostEffectsBase_DrawBorder_m501B2ABEF35941F9B67F4DE18B28051D2CEBBA53,
	PostEffectsBase__ctor_mA6375ECEEA9338D745C8ADDDF2CE2CFE19AD3919,
	PostEffectsHelper_OnRenderImage_mA18DD006648EA3B0DFF3B2F398CF092CF4F80AFD,
	PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m53C287FE762F776C762CDCC54CC78FA7F722AB73,
	PostEffectsHelper_DrawBorder_m24A63C8C77ED434DB28D78DADEA1E9F9D125CD9F,
	PostEffectsHelper_DrawLowLevelQuad_m1FE6DC9DAF0604EFF00C05FDB06B424AE1856C12,
	PostEffectsHelper__ctor_m416594A86C0F56174E3FA23BBDADD32D6E8E2F30,
	Quads_HasMeshes_m41D66F9A795040CCB2219E1412C88C1C59729A04,
	Quads_Cleanup_m18CF6B317A33E5BFF0C5D139F3075321CE31A461,
	Quads_GetMeshes_mB329F322CBF3A8CEFE17BD982B669E48E2559625,
	Quads_GetMesh_m3001DD2FCD94103FE9D38091C4BF006CCA25F44E,
	Quads__ctor_m16AD4881C27EBAFF0BEB90D19BFDD7BEA88F0B1D,
	ScreenOverlay_CheckResources_m6A890799CFBE98D63CCCF134A0CA69A3F3CB88D6,
	ScreenOverlay_OnRenderImage_mA6F3FFBE94BD2C17D1516E81B93B16015E3B9F41,
	ScreenOverlay__ctor_mDC65CB472F36EE51C7A7F52999B3AC09054E55F9,
	ScreenSpaceAmbientObscurance_CheckResources_mFF7D1009C46F8CCF18C7869614FAA0E00A99226E,
	ScreenSpaceAmbientObscurance_OnDisable_mF578FB1D02B1EEE113D3A0341D455B969C71DE38,
	ScreenSpaceAmbientObscurance_OnRenderImage_m230B77750177C0D038530D29B7CD5217CD778A84,
	ScreenSpaceAmbientObscurance__ctor_m044ADAA04CE20850C56457877E9CE0C0F620414A,
	ScreenSpaceAmbientOcclusion_CreateMaterial_mF7A3FD447AF298C8C49954F744F28EFBDAA1C6F0,
	ScreenSpaceAmbientOcclusion_DestroyMaterial_m41A6707B9A5D617667823081040A20A121818F77,
	ScreenSpaceAmbientOcclusion_OnDisable_mB3286E83ECA87FD2140D19B3CCAEFC5C2CE46B63,
	ScreenSpaceAmbientOcclusion_Start_m7C1617597789A3A15FD5ED8855606D8A2D4ED123,
	ScreenSpaceAmbientOcclusion_OnEnable_m193984EB3410877D65EAB44BA45194270A8EA804,
	ScreenSpaceAmbientOcclusion_CreateMaterials_mC63630500AFFAE0FBBD968E3CC980CCEF882118F,
	ScreenSpaceAmbientOcclusion_OnRenderImage_m24F6A58D4D44A6A3BBB477A9BB13CDAF91B71505,
	ScreenSpaceAmbientOcclusion__ctor_m615337A0FBED24E5C41E17418A2615FF8EF14991,
	SepiaTone_OnRenderImage_m31BF87C02794E9A05C1677F4F520E535761EB5C1,
	SepiaTone__ctor_m34F40550EC1F27CA8A3DB7DC35AFAF4813EBB157,
	SunShafts_CheckResources_m9C75528877A6260288B58129276616124B900555,
	SunShafts_OnRenderImage_m09CB5311D8F7CC6064215683AFA18D8E2DA94BF3,
	SunShafts__ctor_m17D8FCF6A1EEDA1A07F46BA3FD3D2579203C1C67,
	TiltShift_CheckResources_mFF9ED6FAA29732CD2F698BCF10C44D6454024026,
	TiltShift_OnRenderImage_m23A47A2B2AF059156929865A0FE5FA39EC7D8BD5,
	TiltShift__ctor_m309B85AE3B13B1CE309EE2E6D36571E4CD2EBB0B,
	Tonemapping_CheckResources_mC1C0CAB02BD19684E08232CD57248E018FD07C8E,
	Tonemapping_UpdateCurve_mADC42B4344BD7C6BF315CA7419DCE55A51B13156,
	Tonemapping_OnDisable_m4E9ADFC3365B18E2A3ACF5F5B87BBE50FF08DF14,
	Tonemapping_CreateInternalRenderTexture_mEA7D2798E23F5144C84D708B3CD71A3093B36E34,
	Tonemapping_OnRenderImage_m3DDE8CADC9E8BEB73403CF71C46CC83F1E32C960,
	Tonemapping__ctor_mEF2579982D0293C0899810E4BCB8FF6B8862E84F,
	Triangles_HasMeshes_m06D1A010EFB4AFD22701D79DEA321C5BDCE8C0EE,
	Triangles_Cleanup_m51E44CD73E253FDFBB58CC18030D0D03676B2194,
	Triangles_GetMeshes_m3DCF64012831DC4D525D97E07F13846F119F00F8,
	Triangles_GetMesh_m2794A44B43D1572051664372102732E88DF16B05,
	Triangles__ctor_mB64F02B9481FC7C583FF49508F4AF36506137851,
	Twirl_OnRenderImage_m29976F163DF515C836C6925BD2770B8B6AE9C925,
	Twirl__ctor_m77736ED53F9B5740B06DA39C508641158E6D4B03,
	VignetteAndChromaticAberration_CheckResources_m6F1DD9FE91586FC0B7C56E490DAC8E68E82AE9EC,
	VignetteAndChromaticAberration_OnRenderImage_m5C3262455A520C611E1C5818DA3AEBF7C5611DC6,
	VignetteAndChromaticAberration__ctor_m3DA6B990E322E0336C4040A1F6A40016E930A20A,
	Vortex_OnRenderImage_m9E5533007C6B15733582D6DBE20CE932EE9C2CFD,
	Vortex__ctor_m1E336A10B273F31D568CC82C1C5CEBB927402DD6,
};
static const int32_t s_InvokerIndices[809] = 
{
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3880,
	3977,
	5965,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	5412,
	5995,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3880,
	3977,
	3977,
	5995,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3880,
	3880,
	3977,
	3977,
	5335,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3223,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	95,
	3977,
	3977,
	3223,
	3223,
	3977,
	3977,
	3977,
	3977,
	3977,
	5965,
	5884,
	3977,
	3223,
	3223,
	3977,
	2921,
	2921,
	3977,
	3223,
	3977,
	3223,
	3814,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3223,
	3223,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	5965,
	5884,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3223,
	3880,
	2864,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3223,
	3223,
	3223,
	3977,
	3977,
	3977,
	3977,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	5965,
	5884,
	3977,
	3977,
	3977,
	3204,
	3223,
	2862,
	2289,
	3977,
	3223,
	3880,
	3814,
	3977,
	5965,
	5884,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3880,
	3977,
	3223,
	3223,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3223,
	3223,
	3223,
	3223,
	3977,
	1033,
	5891,
	5891,
	5891,
	3977,
	5965,
	5884,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3223,
	1847,
	3977,
	3977,
	3977,
	2289,
	3880,
	1842,
	3977,
	3977,
	3880,
	3223,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	5965,
	5884,
	3977,
	3977,
	3880,
	3977,
	3254,
	3254,
	3254,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	5965,
	5884,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	5965,
	5884,
	3977,
	3223,
	3977,
	3880,
	3880,
	3977,
	3223,
	3880,
	3977,
	3977,
	3223,
	3223,
	3977,
	3977,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3223,
	3223,
	3977,
	3977,
	3977,
	3223,
	3977,
	3977,
	3977,
	3977,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	874,
	3977,
	874,
	3977,
	3880,
	3223,
	3880,
	3223,
	3880,
	3223,
	3880,
	3223,
	3880,
	3223,
	3977,
	3977,
	3223,
	3223,
	1892,
	1892,
	988,
	988,
	1005,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3977,
	2864,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3223,
	2864,
	2864,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3880,
	3880,
	3977,
	5995,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3204,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3977,
	3204,
	3977,
	3977,
	3977,
	1892,
	1892,
	988,
	988,
	1005,
	3977,
	3977,
	3977,
	3977,
	3223,
	3223,
	3977,
	3977,
	3977,
	3977,
	3223,
	3977,
	3223,
	3223,
	3223,
	3223,
	3204,
	3977,
	3977,
	3977,
	3977,
	3204,
	3977,
	3977,
	3977,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3223,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3223,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3223,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3223,
	3880,
	3977,
	3977,
	1281,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	2864,
	3880,
	3977,
	3204,
	3977,
	3814,
	3880,
	3977,
	3880,
	3977,
	3977,
	3977,
	3977,
	3880,
	3814,
	1847,
	3977,
	3814,
	1847,
	1030,
	1847,
	1030,
	912,
	1030,
	3977,
	3814,
	1847,
	1030,
	1847,
	720,
	1030,
	3977,
	3814,
	3977,
	1847,
	3977,
	3880,
	3977,
	3977,
	1005,
	1847,
	1847,
	3977,
	3814,
	3977,
	1847,
	3977,
	3977,
	3977,
	3977,
	3977,
	3814,
	1847,
	3977,
	3880,
	3977,
	5246,
	3977,
	5995,
	3977,
	3977,
	3814,
	3977,
	3977,
	1847,
	3977,
	3814,
	3977,
	3977,
	3977,
	2310,
	1847,
	1847,
	3977,
	1847,
	3977,
	3814,
	1847,
	3977,
	3880,
	3880,
	3880,
	3880,
	3977,
	3977,
	3977,
	1847,
	3223,
	3977,
	3814,
	1847,
	3977,
	3814,
	3977,
	3977,
	3977,
	3977,
	2922,
	1835,
	1847,
	3977,
	3977,
	3814,
	3977,
	3977,
	2922,
	3859,
	2684,
	1847,
	331,
	331,
	333,
	1847,
	1006,
	3977,
	626,
	3977,
	5995,
	3814,
	3977,
	3977,
	3977,
	1847,
	3977,
	3814,
	1847,
	3977,
	3814,
	1847,
	3977,
	1847,
	3977,
	3977,
	3880,
	3977,
	3977,
	4212,
	5464,
	5038,
	3977,
	3977,
	3977,
	1847,
	3977,
	3814,
	1847,
	4402,
	3977,
	5995,
	3977,
	3880,
	3977,
	3977,
	1847,
	3977,
	1435,
	1435,
	3977,
	3977,
	3977,
	3814,
	3814,
	3977,
	2247,
	1089,
	3814,
	3977,
	2310,
	3977,
	1847,
	3977,
	1847,
	4408,
	5464,
	4141,
	3977,
	5945,
	5995,
	5289,
	4581,
	3977,
	3814,
	1847,
	3977,
	3814,
	3977,
	1847,
	3977,
	5727,
	5884,
	3977,
	3977,
	3977,
	3977,
	1847,
	3977,
	1847,
	3977,
	3814,
	1847,
	3977,
	3814,
	1847,
	3977,
	3814,
	3918,
	3977,
	3814,
	1847,
	3977,
	5945,
	5995,
	5289,
	4581,
	3977,
	1847,
	3977,
	3814,
	1847,
	3977,
	1847,
	3977,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	809,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
