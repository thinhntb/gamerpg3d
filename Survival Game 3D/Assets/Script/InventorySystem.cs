using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventorySystem : MonoBehaviour
{
    public GameObject ItemInfoUI;
    public static InventorySystem Instance { get; set; }
 
    public GameObject inventoryScreenUI;
    public bool isOpen;

    public List<GameObject> slotList = new List<GameObject>();
    public List<string> itemList = new List<string>();
    private GameObject itemToAdd;
    private GameObject whatSlotToEquip;
    public bool isFull;

    //Pickup Popup
    public GameObject pickupAlert;
    public TextMeshProUGUI pickupName;
    public Image pickupImage;

    public GameObject itIsFull;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }


    void Start()
    {
        isOpen = false;

        isFull = false;
        PopulateSlotList();

        Cursor.visible = false;

    }
 
    private void PopulateSlotList(){

        foreach (Transform child in inventoryScreenUI.transform){
            if(child.CompareTag("Slot")){
                slotList.Add(child.gameObject);
            }
        }

    }
 
    void Update()
    {
 
        if (Input.GetKeyDown(KeyCode.I) && !isOpen && !ConstructionManager.Instance.inConstructionMode)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.ofpenBagSound);
			Debug.Log("i is pressed");
            inventoryScreenUI.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            SelectionManager.Instance.DisableSelection();
            SelectionManager.Instance.GetComponent<SelectionManager>().enabled = false;

            isOpen = true;
 
        }
        else if (Input.GetKeyDown(KeyCode.I) && isOpen)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.closeBagSound);
            inventoryScreenUI.SetActive(false);

            if(!CraftingSystem.instance.isOpen){
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                SelectionManager.Instance.EnableSelection();
                SelectionManager.Instance.GetComponent<SelectionManager>().enabled = true;
            }
            
            isOpen = false;
        }
    }

    public void onCursor(){
        isOpen = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        SelectionManager.Instance.DisableSelection();
        SelectionManager.Instance.GetComponent<SelectionManager>().enabled = false;
    }

    public void offCursor(){
        if(!CraftingSystem.instance.isOpen){
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            SelectionManager.Instance.EnableSelection();
            SelectionManager.Instance.GetComponent<SelectionManager>().enabled = true;
        }
        isOpen = false;
    }

    public void onBag(){
        if (!isOpen && !ConstructionManager.Instance.inConstructionMode)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.ofpenBagSound);
			Debug.Log("i is pressed");
            inventoryScreenUI.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            SelectionManager.Instance.DisableSelection();
            SelectionManager.Instance.GetComponent<SelectionManager>().enabled = false;

            isOpen = true;
 
        }
        else if (isOpen)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.closeBagSound);
            inventoryScreenUI.SetActive(false);

            if(!CraftingSystem.instance.isOpen){
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                SelectionManager.Instance.EnableSelection();
                SelectionManager.Instance.GetComponent<SelectionManager>().enabled = true;
            }
            
            isOpen = false;
        }
    }

    public void AddToInventory(string itemName){
        // Thêm thông báo gỡ lỗi sau đây
        Debug.Log("Adding item to inventory: " + itemName);

        GameObject itemToAdd = Resources.Load<GameObject>(itemName);
        if (itemToAdd == null)
        {
            Debug.LogError("Failed to load item resource: " + itemName);
            return;
        }
        
        SoundManager.Instance.PlaySound(SoundManager.Instance.pickupItemSound);

        // if(CheckSlotsAvailable(0)){
        //     Debug.Log("the inventory is full");
        //     isFull = true;
        // }
        // else {
            whatSlotToEquip = FindNextEmptySlot();

            itemToAdd = Instantiate(Resources.Load<GameObject>(itemName), whatSlotToEquip.transform.position, whatSlotToEquip.transform.rotation);
            itemToAdd.transform.SetParent(whatSlotToEquip.transform);

            itemList.Add(itemName);

            ReCalculateList();
            CraftingSystem.instance.RefreshNeededItems();

            // Sprite sprite = itemToAdd.GetComponent<Image>().sprite;
            TriggerPickupPopUp(itemName, itemToAdd.GetComponent<Image>().sprite);

        // }
    }

    void TriggerPickupPopUp(string itemName, Sprite itemSprite){
        pickupAlert.SetActive(true);
        pickupName.text = itemName;
        pickupImage.sprite = itemSprite;
        Invoke("TriggerOffPopUp", 4f);
    }

    void TriggerOffPopUp(){
        pickupAlert.SetActive(false);
    }

    public void TriggerIsFullPopup(){
        itIsFull.SetActive(true);
        Invoke("TriggerOffIsFullPopup", 4f);
    }

    void TriggerOffIsFullPopup(){
        itIsFull.SetActive(false);
    }
    public bool CheckSlotsAvailable(int emptyMeeded){
        
        int emptySlots = 0;

        foreach(GameObject slot in slotList){
            if(slot.transform.childCount <= 0){
                emptySlots += 1;
            }
        }

        if(emptySlots >= emptyMeeded){
            return true;
        } else {
            return false;
        }

    }

    private GameObject FindNextEmptySlot(){

        foreach(GameObject slot in slotList){
            if(slot.transform.childCount == 0){
                return slot;
            }
        }
        return new GameObject();
    }

    public void RemoveItem(string nameToRemove, int amountToRemove){
        int counter = amountToRemove;

        for(var i = slotList.Count - 1; i >= 0; i--){
            if(slotList[i].transform.childCount > 0){
                if(slotList[i].transform.GetChild(0).name == nameToRemove + "(Clone)" && counter != 0){
                    Destroy(slotList[i].transform.GetChild(0).gameObject);
                    counter -= 1;
                }
            }
        }
    }

    public void ReCalculateList(){
        itemList.Clear();

        foreach (GameObject slot in slotList){
            if (slot.transform.childCount > 0){
                string name = slot.transform.GetChild(0).name; // Stone (Clone)

                // string str1 = name;
                string str2 = "(Clone)";

                string result = name.Replace(str2, "");

                itemList.Add(result);
            }
        }
    }

    //Chat GPT
    // public void UpdateItemQuickSlotStatus(GameObject item, bool isInsideQuickSlot)
    // {
    //     item.GetComponent<InventoryItem>().isInsideQuickSlot = isInsideQuickSlot;
    // }

}
