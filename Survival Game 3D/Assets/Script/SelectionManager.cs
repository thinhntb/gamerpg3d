using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectionManager : MonoBehaviour
{

    public static SelectionManager Instance {get; set;}
    public bool onTarget;

    public GameObject menuPause;
    public bool isPause = false;

    public Image PauseBTN;
    public Sprite icon_pause;
    public Sprite icon_play;
    public GameObject selectedObject;
    public GameObject interaction_Info_UI;
    TextMeshProUGUI interaction_text;
    public Image centerDotlmage;
    public Image handIcon;

    public bool handIsVisible;

    public GameObject selectedTree;
    public GameObject chopHolder;

    public GameObject selectedRock;
    public GameObject breakHolder;

    public GameObject selectedAnimal;
    public GameObject AttackHolder;
    private void Start()
    {
        onTarget = false;
        interaction_text = interaction_Info_UI.GetComponent<TextMeshProUGUI>();
    }
 
    private void Awake() {
        if (Instance != null && Instance != this){
            Destroy(gameObject);
        }
        else{
            Instance = this;
        }
    }
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            var selectionTransform = hit.transform;

            InteractableObject interactable = selectionTransform.GetComponent<InteractableObject>();
            BreakingRock breakingRock = selectionTransform.GetComponent<BreakingRock>();
            ChoppableTree choppableTree = selectionTransform.GetComponent<ChoppableTree>();
            Transform rabbit_base = selectionTransform.Find("rabbit_base");
            HitAtack hitAtack = null;
            if (rabbit_base != null) {
                rabbit_base.TryGetComponent<HitAtack>(out hitAtack);
            }
            
            if (hitAtack != null && hitAtack.playerInRange){
                Debug.Log("Rabbit InRange true called");
                hitAtack.canBeAttacked = true;
                selectedAnimal= hitAtack.gameObject;
                AttackHolder.gameObject.SetActive(true);
            } else {
                Debug.Log("Rabbit InRange false called");
                if (selectedAnimal != null){
                    selectedAnimal.gameObject.GetComponent<HitAtack>().canBeAttacked = false;
                    selectedAnimal = null;
                    AttackHolder.gameObject.SetActive(false);
                }
            }

            if (breakingRock && breakingRock.playerInRange){
                Debug.Log("playerInRange true called");
                breakingRock.canBeBreaked = true;
                selectedRock = breakingRock.gameObject;
                breakHolder.gameObject.SetActive(true);
            } else {
                Debug.Log("playerInRange false called");
                if (selectedRock != null){
                    selectedRock.gameObject.GetComponent<BreakingRock>().canBeBreaked = false;
                    selectedRock = null;
                    breakHolder.gameObject.SetActive(false);
                }
            }

            if (choppableTree && choppableTree.playerInRange){
                choppableTree.canBeChopped = true;
                selectedTree = choppableTree.gameObject;
                chopHolder.gameObject.SetActive(true);
            } else {
                if (selectedTree != null){
                    selectedTree.gameObject.GetComponent<ChoppableTree>().canBeChopped = false;
                    selectedTree = null;
                    chopHolder.gameObject.SetActive(false);
                }
            }

            Debug.Log("playerInRange called");

 
            if (interactable && interactable.playerInRange)
            {
                onTarget = true;
                selectedObject = interactable.gameObject;
                interaction_text.text = interactable.GetItemName();
                interaction_Info_UI.SetActive(true);

                if(interactable.CompareTag("pickable")){
                    handIcon.gameObject.SetActive(true);
                    centerDotlmage.gameObject.SetActive(false);
                    handIsVisible = true;
                } else {
                    onTarget = false;
                    handIcon.gameObject.SetActive(false);
                    centerDotlmage.gameObject.SetActive(true);
                    handIsVisible = false;
                }
            }
            else // if there is a hit, but without an interactable Script.
            { 
                onTarget = false;
                interaction_Info_UI.SetActive(false);

                handIcon.gameObject.SetActive(false);
                centerDotlmage.gameObject.SetActive(true);

                handIsVisible = false;
            }

        }
        else // if there is no hit at all
        {
            onTarget = false;
            interaction_Info_UI.SetActive(false);

            handIcon.gameObject.SetActive(false);
            centerDotlmage.gameObject.SetActive(true);

            handIsVisible = false;
        }
    
        if (Input.GetKeyDown(KeyCode.P) && isPause == false){
            Debug.Log("Input Key Code P");
            // onPause();
            isPause = true;
            menuPause.SetActive(true);
            InventorySystem.Instance.onCursor();
            PauseBTN.sprite = icon_play;
        } else if (Input.GetKeyDown(KeyCode.P) && isPause == true){
            Debug.Log("Input Key Code P");
            // onPause();
            isPause = false;
            menuPause.SetActive(false);
            InventorySystem.Instance.offCursor();
            PauseBTN.sprite = icon_pause;
        }

    }

    public void onPause(){
        if(!isPause){
            isPause = true;
            menuPause.SetActive(true);
            InventorySystem.Instance.onCursor();
            PauseBTN.sprite = icon_play;
        } else{
            isPause = false;
            menuPause.SetActive(false);
            InventorySystem.Instance.offCursor();
            PauseBTN.sprite = icon_pause;
        }
    }

    public void DisableSelection(){
        handIcon.enabled = false;
        centerDotlmage.enabled = false;
        interaction_Info_UI.SetActive(false);

        selectedObject = null;
    }

    public void EnableSelection(){
        handIcon.enabled = true;
        centerDotlmage.enabled = true;
        interaction_Info_UI.SetActive(true);
    }
}
