using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    public float dayIntensity = 1.0f;
    public float nightIntensity = 0.2f;
    public float transitionSpeed = 1.0f;
    public float dayDuration = 10.0f; // Thời gian cho một ngày (giây)
    public float nightDuration = 10.0f; // Thời gian cho một đêm (giây)
    public Material daySkybox;
    public Material nightSkybox;

    public Light directionalLight;

    private float currentIntensity;
    private float currentValue = 0.0f; // Giá trị từ 0 (đêm) đến 1 (ngày)
    private float elapsedTime = 0.0f;
    private bool isDay = true;
    void Start()
    {
        // Tìm ánh sáng hướng (Directional Light) trong scene
        // directionalLight = FindObjectOfType<Light>();
        if (directionalLight.type != LightType.Directional)
        {
            Debug.LogWarning("No directional light found in the scene!");
        }
    }

    void Update()
    {
        // Nếu ánh sáng hướng không tồn tại, không cần cập nhật độ sáng
        if (directionalLight == null) return;

        // Cập nhật thời gian trôi qua
        elapsedTime += Time.deltaTime;

        // Lerp giữa độ sáng ban ngày và ban đêm
        currentIntensity = Mathf.Lerp(nightIntensity, dayIntensity, currentValue);
        directionalLight.intensity = Mathf.Lerp(directionalLight.intensity, currentIntensity, Time.deltaTime * transitionSpeed);

        // Thay đổi Skybox dựa trên giá trị hiện tại
        if (daySkybox != null && nightSkybox != null)
        {
            Debug.LogWarning("change SKYBOX in the scene!");
            RenderSettings.skybox.Lerp(nightSkybox, daySkybox, currentValue);
            DynamicGI.UpdateEnvironment(); // Cập nhật Global Illumination
        } else {
            Debug.LogWarning("Can't SKYBOX in the scene!");
        }

        // Xác định liệu có phải là ban ngày hay ban đêm
        if (isDay)
        {
            currentValue = elapsedTime / dayDuration;
            if (elapsedTime >= dayDuration)
            {
                elapsedTime = 0.0f;
                isDay = false;
            }
        }
        else
        {
            currentValue = elapsedTime / nightDuration;
            if (elapsedTime >= nightDuration)
            {
                elapsedTime = 0.0f;
                isDay = true;
            }
        }
    }
}