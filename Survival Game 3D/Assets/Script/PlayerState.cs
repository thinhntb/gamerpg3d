using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    public static PlayerState Instance { get; set;}

    // ---- Player Hralth ----//
    public float currentHealth;
    public float maxHealth;

    // ---- Player Calories ----//
    public float currentCalories;
    public float maxCalories;

    float distanceTravelled = 0;
    Vector3 lastPosition;
    public GameObject playerBody;

    // ---- Player Hydration ---- //
    public float currentHydrationPercent;
    public float maxHydrationPercent;

    private bool isHydrationActive;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start(){
        currentHealth = maxHealth;
        currentCalories = maxCalories;
        currentHydrationPercent = maxHydrationPercent;

        StartCoroutine(decreaseHydration());
    }

    IEnumerator decreaseHydration(){
        while (true){
            if(currentHydrationPercent > 0){
                currentHydrationPercent -= 1;
            }
            yield return new WaitForSeconds(10);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Testing Health Bar
        if (Input.GetKeyDown(KeyCode.N)){
            if (currentHealth > 10){
                currentHealth -= 10;
            } else {
                currentHealth = 0;
            }
            
        }

        //Testing Calories Bar
        distanceTravelled += Vector3.Distance(playerBody.transform.position, lastPosition);
        lastPosition = playerBody.transform.position;

        if (distanceTravelled >= 5){
            distanceTravelled = 0;
            currentCalories -= 1;
        }
    }

    public void setHealth(float newHealth){
        currentHealth = newHealth;
    }

    public void setCalories(float newCalories){
        currentCalories = newCalories;
    }

    public void setHydration(float newHydration){
        currentHydrationPercent = newHydration;
    }

}
