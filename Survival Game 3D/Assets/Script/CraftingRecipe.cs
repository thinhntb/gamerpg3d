using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class CraftingRecipe
{
    public string recipeName;
    public List<string> requiredItems;
    public List<int> requiredItemCounts;
    public int requiredSlots;
    public TextMeshProUGUI[] requirementTexts;
    public Button craftButton;
}