using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HydrationBar : MonoBehaviour
{
    private Slider slider;
    public TextMeshProUGUI HydrationCounter;
    public GameObject playerState;
    private float currentHydration, maxHydration;


    // Start is called before the first frame update
    void Awake()
    {
        slider = GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        currentHydration = playerState.GetComponent<PlayerState>().currentHydrationPercent;
        maxHydration = playerState.GetComponent<PlayerState>().maxHydrationPercent;

        float fillValue = currentHydration / maxHydration; // 100/100 // 1/
        slider.value = fillValue;

        HydrationCounter.text = currentHydration + "%" ; // 80/100
    }
}
