using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroGame : MonoBehaviour
{

    public bool isShowed = false;

    public GameObject introGame;
    public GameObject introA, introB, introC, introD, introE, introF;

    // Start is called before the first frame update
    void Start()
    {
        if(isShowed == false){
            Invoke("popupIntroA", 4f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void popupIntroA(){
        InventorySystem.Instance.onCursor();
        introA.SetActive(true);
        introB.SetActive(false);
        introC.SetActive(false);
        introD.SetActive(false);
        introE.SetActive(false);
        introF.SetActive(false);
    }

    public void popupIntroB(){
        InventorySystem.Instance.onCursor();
        introA.SetActive(false);
        introB.SetActive(true);
        introC.SetActive(false);
        introD.SetActive(false);
        introE.SetActive(false);
        introF.SetActive(false);
    }

    public void popupIntroC(){
        InventorySystem.Instance.onCursor();
        introA.SetActive(false);
        introB.SetActive(false);
        introC.SetActive(true);
        introD.SetActive(false);
        introE.SetActive(false);
        introF.SetActive(false);
    }

    public void popupIntroD(){
        InventorySystem.Instance.onCursor();
        introA.SetActive(false);
        introB.SetActive(false);
        introC.SetActive(false);
        introD.SetActive(true);
        introE.SetActive(false);
        introF.SetActive(false);
    }

    public void popupIntroE(){
        InventorySystem.Instance.onCursor();
        introA.SetActive(false);
        introB.SetActive(false);
        introC.SetActive(false);
        introD.SetActive(false);
        introE.SetActive(true);
        introF.SetActive(false);
    }

    public void popupIntroF(){
        InventorySystem.Instance.onCursor();
        introA.SetActive(false);
        introB.SetActive(false);
        introC.SetActive(false);
        introD.SetActive(false);
        introE.SetActive(false);
        introF.SetActive(true);
    }

    public void quitIntro(){
        InventorySystem.Instance.offCursor();
        introGame.SetActive(false);
    }
}
