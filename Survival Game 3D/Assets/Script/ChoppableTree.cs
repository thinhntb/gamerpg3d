using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ChoppableTree : MonoBehaviour
{
    
    public bool playerInRange;
    public bool canBeChopped;

    public float treeMaxHealth;
    public float treeHealth;

    public Animator animator;

    public float caloriesSpentChoppingWood = 10;

    private void Start() {
        treeHealth = treeMaxHealth;
        animator = transform.parent.transform.parent.GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")){
            playerInRange = false;
        }
    }

    public void GetHit(){
        // treeHealth -=1;

        animator.SetTrigger("shake");

        treeHealth -= 2;

        PlayerState.Instance.currentCalories -= caloriesSpentChoppingWood;

        if (treeHealth <= 0){
            treeIsDead();
        }

        // StartCoroutine(hit());
    }

    // public IEnumerator hit(){
    //     yield return new WaitForSeconds(0.6f);

    //     animator.SetTrigger("shake");

    //     treeHealth -= 1;

    //     PlayerState.Instance.currentCalories -= caloriesSpentChoppingWood;

    //     if (treeHealth <= 0){
    //         treeIsDead();
    //     }
    // }

    void treeIsDead(){
        Vector3 treePosition = transform.position;

        Destroy(transform.parent.transform.parent.gameObject);
        SoundManager.Instance.PlaySound(SoundManager.Instance.TreeFall);
        canBeChopped = false;
        SelectionManager.Instance.selectedTree = null;
        SelectionManager.Instance.chopHolder.gameObject.SetActive(false);

        GameObject brokenTree = Instantiate(Resources.Load<GameObject>("ChoppedTree"),
                                new Vector3(treePosition.x, treePosition.y + 1, treePosition.z), Quaternion.Euler(0, 0, 0));
    }
    private void Update() {
        if (canBeChopped){
            GlobalState.Instance.resourceHealth = treeHealth;
            GlobalState.Instance.resourceMaxHealth = treeMaxHealth;
        }
    }
}
