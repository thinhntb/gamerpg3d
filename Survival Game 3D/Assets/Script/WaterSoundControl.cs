using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSoundControl : MonoBehaviour
{

    void Start()
    {

    }

    void Update()
    {
        // SoundManager.Instance.waterSound.volume -= volume; // Giảm âm lượng của AudioSource đi 0.1 đơn vị.
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            SoundManager.Instance.PlaySound(SoundManager.Instance.waterSound);
        }
        
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")){
            SoundManager.Instance.waterSound.Stop();
        }
    }
}
