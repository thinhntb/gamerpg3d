using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CaloriesBar : MonoBehaviour
{
    private Slider slider;
    public TextMeshProUGUI CaloriesCounter;
    public GameObject playerState;
    private float currentCalories, maxCalories;


    // Start is called before the first frame update
    void Awake()
    {
        slider = GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        currentCalories = playerState.GetComponent<PlayerState>().currentCalories;
        maxCalories = playerState.GetComponent<PlayerState>().maxCalories;

        float fillValue = currentCalories / maxCalories; // 100/100 // 1/
        slider.value = fillValue;

        CaloriesCounter.text = currentCalories + "/" + maxCalories; // 80/100
    }
}
