using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorObject : MonoBehaviour
{
    public Animator animator;
    public bool playerInRange;
    public bool isOfpen = false;

    public GameObject doorMode;
    // Start is called before the first frame update
    void Start()
    {
        animator = transform.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && playerInRange){
            // Debug.Log("item added to inventory");

            if (!isOfpen){
                isOfpen = true;
                animator.SetTrigger("ofpen");
            } else {
                animator.SetTrigger("close");
                isOfpen = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            playerInRange = true;
            doorMode.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")){
            playerInRange = false;
            doorMode.SetActive(false);
        }
    }
}
