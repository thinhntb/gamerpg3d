using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class HitAtack : MonoBehaviour
{
    public bool playerInRange;
    public bool canBeAttacked;

    public float animalMaxHealth;
    public float animalHealth;

    public Animator animator;

    public float caloriesSpentChoppingWood = 10;

    private void Start() {
        animalHealth = animalMaxHealth;
        animator = transform.parent.GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")){
            playerInRange = false;
        }
    }

    public void GetHit(){

        // animator.SetTrigger("Next");

        animalHealth -= 45;

        PlayerState.Instance.currentCalories -= caloriesSpentChoppingWood;

        if (animalHealth <= 0){
            animator.SetInteger("AnimIndex",2);
            animator.SetTrigger("Next");
            // animalIsDead();
            StartCoroutine(animalIsDie());
        }

    }

    public IEnumerator animalIsDie(){
        yield return new WaitForSeconds(1.5f);

        Vector3 animalPosition = transform.position;

        Destroy(transform.parent.gameObject);
        SoundManager.Instance.PlaySound(SoundManager.Instance.TreeFall);
        canBeAttacked = false;
        SelectionManager.Instance.selectedTree = null;
        SelectionManager.Instance.chopHolder.gameObject.SetActive(false);

        GameObject brokenTree = Instantiate(Resources.Load<GameObject>("HitedRabbit"),
                                new Vector3(animalPosition.x, animalPosition.y + 1, animalPosition.z), Quaternion.Euler(0, 0, 0));
    }

    void animalIsDead(){
        Vector3 animalPosition = transform.position;

        Destroy(transform.parent.transform.parent.gameObject);
        SoundManager.Instance.PlaySound(SoundManager.Instance.TreeFall);
        canBeAttacked = false;
        SelectionManager.Instance.selectedTree = null;
        SelectionManager.Instance.chopHolder.gameObject.SetActive(false);

        GameObject brokenTree = Instantiate(Resources.Load<GameObject>("HitedRabbit"),
                                new Vector3(animalPosition.x, animalPosition.y + 1, animalPosition.z), Quaternion.Euler(0, 0, 0));
    }
    private void Update() {
        if (canBeAttacked){
            GlobalState.Instance.resourceHealth = animalHealth;
            GlobalState.Instance.resourceMaxHealth = animalMaxHealth;
        }
    }
}
