using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class BreakingRock : MonoBehaviour
{
    public bool playerInRange;
    public bool canBeBreaked;

    public float rockMaxHealth;
    public float rockHealth;

    public Animator animator;

    public float caloriesSpentBreakingRock = 10;
    // Start is called before the first frame update
    private void Start() {
        rockHealth = rockMaxHealth;
        animator = transform.parent.transform.parent.GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            playerInRange = true;
            // Debug.Log("playerInRange true called");
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")){
            playerInRange = false;
            // Debug.Log("playerInRange false called");
        }
    }

    public void GetHit(){
        // treeHealth -=1;

        animator.SetTrigger("shake");

        rockHealth -= 2;

        PlayerState.Instance.currentCalories -= caloriesSpentBreakingRock;

        if (rockHealth <= 0){
            rockIsDead();
        }

        // StartCoroutine(hit());
    }

    // public IEnumerator hit(){
    //     yield return new WaitForSeconds(0.6f);

    //     animator.SetTrigger("shake");

    //     treeHealth -= 1;

    //     PlayerState.Instance.currentCalories -= caloriesSpentChoppingWood;

    //     if (treeHealth <= 0){
    //         treeIsDead();
    //     }
    // }

    void rockIsDead(){
        Vector3 rockPosition = transform.position;

        Destroy(transform.parent.transform.parent.gameObject);

        SoundManager.Instance.PlaySound(SoundManager.Instance.RockSmash);

        canBeBreaked = false;
        SelectionManager.Instance.selectedRock = null;
        SelectionManager.Instance.breakHolder.gameObject.SetActive(false);

        GameObject brokenTree = Instantiate(Resources.Load<GameObject>("BreakedRock"),
                                new Vector3(rockPosition.x, rockPosition.y + 1, rockPosition.z), Quaternion.Euler(0, 0, 0));
    }
    private void Update() {
        if (canBeBreaked){
            GlobalState.Instance.resourceHealth = rockHealth;
            GlobalState.Instance.resourceMaxHealth = rockMaxHealth;
        }
    }
}
