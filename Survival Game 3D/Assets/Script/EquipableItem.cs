using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EquipableItem : MonoBehaviour
{

    public Animator animitor;

    // Start is called before the first frame update
    void Start()
    {
        animitor = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) &&
            InventorySystem.Instance.isOpen == false &&
            CraftingSystem.instance.isOpen == false &&
            SelectionManager.Instance.handIsVisible == false &&
            !ConstructionManager.Instance.inConstructionMode
            ){

            // Left Mouse Button
            StartCoroutine(SwingSoundDelay());
            animitor.SetTrigger("hit");
            
        }
    }

    public void GetTreeHit(){
        GameObject selectTree = SelectionManager.Instance.selectedTree;

        if (selectTree != null){
            SoundManager.Instance.PlaySound(SoundManager.Instance.chopSound);
            selectTree.GetComponent<ChoppableTree>().GetHit();
        }
    }

    public void GetRockHit(){
        GameObject selectRock = SelectionManager.Instance.selectedRock;

        if (selectRock != null){
            SoundManager.Instance.BreakRock.Stop();
            SoundManager.Instance.PlaySound(SoundManager.Instance.BreakRock);
            selectRock.GetComponent<BreakingRock>().GetHit();
        }
    }

    public void GetAnimalHit(){
        GameObject selectAnimal = SelectionManager.Instance.selectedAnimal;

        if (selectAnimal != null){
            SoundManager.Instance.BreakRock.Stop();
            SoundManager.Instance.PlaySound(SoundManager.Instance.BreakRock);
            selectAnimal.GetComponent<HitAtack>().GetHit();
        }
    }

    IEnumerator SwingSoundDelay(){
        yield return new WaitForSeconds(0.2f);
        SoundManager.Instance.PlaySound(SoundManager.Instance.toolSwingSound);
    }

}
