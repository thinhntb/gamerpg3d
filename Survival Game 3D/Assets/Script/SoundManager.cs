using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; set; }

    public AudioSource dropItemSound;
    public AudioSource craftingSound;
    public AudioSource toolSwingSound;
    public AudioSource chopSound;
    public AudioSource grassWalkSound;
    public AudioSource waterSound;
    public AudioSource craftMenuSound;
    public AudioSource ofpenBagSound;
    public AudioSource closeBagSound;


    public AudioSource pickupItemSound;

    public AudioSource SwordHitMetal;
    public AudioSource TreeFall;
    public AudioSource BreakRock;
    public AudioSource RockSmash;
    public AudioSource ButtonClick;
    public AudioSource MetalHit;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }


    public void PlaySound(AudioSource soundToPlay){
        if (!soundToPlay.isPlaying){
            soundToPlay.Play();
        }
    }

}
