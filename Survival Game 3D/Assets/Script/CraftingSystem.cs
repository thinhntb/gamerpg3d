using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CraftingSystem : MonoBehaviour
{

    public GameObject craftingScreenUI;
    public GameObject toolsScreenUI, survivalScreenUI, refineScreenUI, constructionScreenUI;

    public List<string> inventoryItemList = new List<string>();

    //Category Buttons
    Button toolsBTN, survivalBTN, refineBTN, constructionBTN;

    // //Craft Buttons
    // Button craftAxeBTN, craftPickaxeBTN, craftPlankBTN, craftFoundationBTN, craftWallBTN;

    // //Tools
    // Button craftSwordBTN;

    // //Cooking
    // Button craftSkeweredMushroomBTN, craftVegatableSoupBTN;
    // Button craftFruitPlatterBTN;

    // //Requirement Text
    // TextMeshProUGUI AxeReq1, AxeReq2, PickaxeReq1, PickaxeReq2, PlankReq1, FoundationReq1, WallReq1;

    // //Cooking
    // TextMeshProUGUI SkeweredMushroomReq1, VegatableSoupReq1, VegatableSoupReq2;
    // TextMeshProUGUI FruitPlatterReq1, FruitPlatterReq2;

    // //Tool
    // TextMeshProUGUI SwordReq1, SwordReq2;

    public List<CraftingRecipe> craftingRecipes;

    public bool isOpen;

    //All Blueprint
    // public Blueprint AxeBLP = new Blueprint("Axe", 1, 2, "Stone", 3, "Stick", 3);
    // public Blueprint AxeBLP = new Blueprint("Pickaxe", 1, 2, "Stone", 3, "Stick", 3);
    // public Blueprint PlankBLP = new Blueprint("Plank", 2, 1, "Log", 1, "", 0);
    // public Blueprint FoundationBLP = new Blueprint("Foundation", 1, 1, "Plank", 4, "", 0);
    // public Blueprint WallBLP = new Blueprint("Wall", 1, 1, "Plank", 2, "", 0);
    public Blueprint AxeBLP;
    public Blueprint PickaxeBLP;
    public Blueprint PlankBLP;

    public Blueprint FoundationBLP;
    public Blueprint WallBLP;
    // public Blueprint AxeBLP;

    //Cooking
    public Blueprint SkeweredMushroomBLP;
    public Blueprint VegatableSoupBLP;
    public Blueprint FruitPlatterBLP;

    //Tool
    public Blueprint SwordBLP;

    public static CraftingSystem instance { get; set;}
    private void Awake() {
        if(instance != null && instance != this){
            Destroy(gameObject);
        }
        else{
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("CraftingSystem.Start() called");
        isOpen = false;

        toolsBTN = craftingScreenUI.transform.Find("ToolsButton").GetComponent<Button>();
        toolsBTN.onClick.AddListener(delegate { OpenToolsCategory(); });

        survivalBTN = craftingScreenUI.transform.Find("SurvivalButton").GetComponent<Button>();
        survivalBTN.onClick.AddListener(delegate { OpenSurvivalCategory(); });

        refineBTN = craftingScreenUI.transform.Find("RefineButton").GetComponent<Button>();
        refineBTN.onClick.AddListener(delegate { OpenRefineCategory(); });

        constructionBTN = craftingScreenUI.transform.Find("ConstructionButton").GetComponent<Button>();
        constructionBTN.onClick.AddListener(delegate { OpenConstructionCategory(); });

        // AxeBLP = new Blueprint("Axe", 2, "Stone", 3, "Stick", 3);

        // //Axe
        // AxeReq1 = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Axe").transform.Find("req1").GetComponent<TextMeshProUGUI>();
        // AxeReq2 = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Axe").transform.Find("req2").GetComponent<TextMeshProUGUI>();

        // craftAxeBTN = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Axe").transform.Find("Button").GetComponent<Button>();
        // craftAxeBTN.onClick.AddListener(delegate {CraftAnyItem(AxeBLP);});

        // //Pickaxe
        // PickaxeReq1 = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Pickaxe").transform.Find("req1").GetComponent<TextMeshProUGUI>();
        // PickaxeReq2 = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Pickaxe").transform.Find("req2").GetComponent<TextMeshProUGUI>();

        // craftPickaxeBTN = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Pickaxe").transform.Find("Button").GetComponent<Button>();
        // craftPickaxeBTN.onClick.AddListener(delegate {CraftAnyItem(PickaxeBLP);});

        // //Sword
        // SwordReq1 = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Sword").transform.Find("req1").GetComponent<TextMeshProUGUI>();
        // SwordReq2 = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Sword").transform.Find("req2").GetComponent<TextMeshProUGUI>();

        // craftSwordBTN = toolsScreenUI
        //         .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //         .transform.Find("Sword").transform.Find("Button").GetComponent<Button>();
        // craftSwordBTN.onClick.AddListener(delegate {CraftAnyItem(SwordBLP);});


        // //Plank
        // PlankReq1 = refineScreenUI.transform.Find("Plank").transform.Find("req1").GetComponent<TextMeshProUGUI>();

        // craftPlankBTN = refineScreenUI.transform.Find("Plank").transform.Find("Button").GetComponent<Button>();
        // craftPlankBTN.onClick.AddListener(delegate {CraftAnyItem(PlankBLP);});

        // //Foundation
        // FoundationReq1 = constructionScreenUI.transform.Find("Foundation").transform.Find("req1").GetComponent<TextMeshProUGUI>();

        // craftFoundationBTN = constructionScreenUI.transform.Find("Foundation").transform.Find("Button").GetComponent<Button>();
        // craftFoundationBTN.onClick.AddListener(delegate {CraftAnyItem(FoundationBLP);});

        // //Wall
        // WallReq1 = constructionScreenUI.transform.Find("Wall").transform.Find("req1").GetComponent<TextMeshProUGUI>();

        // craftWallBTN = constructionScreenUI.transform.Find("Wall").transform.Find("Button").GetComponent<Button>();
        // craftWallBTN.onClick.AddListener(delegate {CraftAnyItem(WallBLP);});

        // //COOKING-------------------------------------------//

        // //-SkeweredMushroom
        // SkeweredMushroomReq1 = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Skewered Mushroom").transform.Find("req1").GetComponent<TextMeshProUGUI>();

        // craftSkeweredMushroomBTN = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Skewered Mushroom").transform.Find("Button").GetComponent<Button>();

        // craftSkeweredMushroomBTN.onClick.AddListener(delegate {CraftAnyItem(SkeweredMushroomBLP);});
        // //-SkeweredMushroom

        // //-VegatableSoup
        // VegatableSoupReq1 = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Vegatable Soup").transform.Find("req1").GetComponent<TextMeshProUGUI>();

        // VegatableSoupReq2 = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Vegatable Soup").transform.Find("req2").GetComponent<TextMeshProUGUI>();

        // craftVegatableSoupBTN = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Vegatable Soup").transform.Find("Button").GetComponent<Button>();

        // craftVegatableSoupBTN.onClick.AddListener(delegate {CraftAnyItem(VegatableSoupBLP);});
        // //-----------------------------------------------------------------------------------

        // //-Fruit Platter
        // FruitPlatterReq1 = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Fruit Platter").transform.Find("req1").GetComponent<TextMeshProUGUI>();

        // FruitPlatterReq2 = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Fruit Platter").transform.Find("req2").GetComponent<TextMeshProUGUI>();

        // craftFruitPlatterBTN = survivalScreenUI
        //     .transform.Find("Scroll area").transform.Find("Scroll").transform.Find("Container")
        //     .transform.Find("Fruit Platter").transform.Find("Button").GetComponent<Button>();

        // craftFruitPlatterBTN.onClick.AddListener(delegate {CraftAnyItem(FruitPlatterBLP);});
        // //-----------------------------------------------------------------------------------

        foreach (CraftingRecipe recipe in craftingRecipes)
        {
            recipe.craftButton.onClick.AddListener(delegate {CraftAnyItemRecipe(recipe);});
        }

    }

    void OpenToolsCategory()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.craftMenuSound);
        craftingScreenUI.SetActive(false);
        survivalScreenUI.SetActive(false);
        refineScreenUI.SetActive(false);
        toolsScreenUI.SetActive(true);
        constructionScreenUI.SetActive(false);
    }

    void OpenSurvivalCategory()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.craftMenuSound);
        craftingScreenUI.SetActive(false);
        survivalScreenUI.SetActive(true);
        refineScreenUI.SetActive(false);
        toolsScreenUI.SetActive(false);
        constructionScreenUI.SetActive(false);
    }

    void OpenRefineCategory()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.craftMenuSound);
        craftingScreenUI.SetActive(false);
        survivalScreenUI.SetActive(false);
        refineScreenUI.SetActive(true);
        toolsScreenUI.SetActive(false);
        constructionScreenUI.SetActive(false);
    }

    void OpenConstructionCategory()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.craftMenuSound);
        craftingScreenUI.SetActive(false);
        survivalScreenUI.SetActive(false);
        refineScreenUI.SetActive(false);
        toolsScreenUI.SetActive(false);
        constructionScreenUI.SetActive(true);
    }
    void CraftAnyItem(Blueprint blueprintToCraft){

        if (blueprintToCraft == null)
        {
            Debug.LogError("Blueprint is null");
            return;
        }
        SoundManager.Instance.craftingSound.Stop();
        SoundManager.Instance.PlaySound(SoundManager.Instance.craftingSound);

        // Produce the amount of items according to the blueprint
        StartCoroutine(craftedDelayForSound(blueprintToCraft));

        //remove resources from inventory
        if (blueprintToCraft.numOfRequirements == 1){
            InventorySystem.Instance.RemoveItem(blueprintToCraft.Req1, blueprintToCraft.Req1amount);
        } else if (blueprintToCraft.numOfRequirements == 2) {
            InventorySystem.Instance.RemoveItem(blueprintToCraft.Req1, blueprintToCraft.Req1amount);
            InventorySystem.Instance.RemoveItem(blueprintToCraft.Req2, blueprintToCraft.Req2amount);
        }

        //refreah list
        InventorySystem.Instance.ReCalculateList();

        StartCoroutine(calculate());

        // RefreshNeededItems();

    }

    public IEnumerator calculate(){
        // yield return new WaitForSeconds(1f);

        // InventorySystem.Instance.ReCalculateList();
        yield return 0;
        InventorySystem.Instance.ReCalculateList();
        RefreshNeededItems();
    }

    IEnumerator craftedDelayForSound(Blueprint blueprintToCraft){
        yield return new WaitForSeconds(0f);
        // Produce the amount of items according to the blueprint
        for (var i = 0; i < blueprintToCraft.numOfItemsToProduce; i++){
            //add item into inventory
            InventorySystem.Instance.AddToInventory(blueprintToCraft.itemName);
        } 
    }

    // Update is called once per frame
    void Update()
    {

        // RefreshNeededItems();

        if (Input.GetKeyDown(KeyCode.C) && !isOpen && !ConstructionManager.Instance.inConstructionMode)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.SwordHitMetal);
			Debug.Log("i is pressed");
            craftingScreenUI.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            SelectionManager.Instance.DisableSelection();
            SelectionManager.Instance.GetComponent<SelectionManager>().enabled = false;

            isOpen = true;

        }
        else if (Input.GetKeyDown(KeyCode.C) && isOpen)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.MetalHit);
            craftingScreenUI.SetActive(false);
            toolsScreenUI.SetActive(false);
            survivalScreenUI.SetActive(false);
            refineScreenUI.SetActive(false);
            constructionScreenUI.SetActive(false);

            if(!InventorySystem.Instance.isOpen){
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                SelectionManager.Instance.EnableSelection();
                SelectionManager.Instance.GetComponent<SelectionManager>().enabled = true;
            }
            
            isOpen = false;
        }
    }

    public void onCraft(){
        if (!isOpen && !ConstructionManager.Instance.inConstructionMode)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.SwordHitMetal);
			Debug.Log("i is pressed");
            craftingScreenUI.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            SelectionManager.Instance.DisableSelection();
            SelectionManager.Instance.GetComponent<SelectionManager>().enabled = false;

            isOpen = true;

        }
        else if (isOpen)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.MetalHit);
            craftingScreenUI.SetActive(false);
            toolsScreenUI.SetActive(false);
            survivalScreenUI.SetActive(false);
            refineScreenUI.SetActive(false);
            constructionScreenUI.SetActive(false);

            if(!InventorySystem.Instance.isOpen){
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;

                SelectionManager.Instance.EnableSelection();
                SelectionManager.Instance.GetComponent<SelectionManager>().enabled = true;
            }
            
            isOpen = false;
        }
    }


    // public void RefreshNeededItems(){
    //     //AXE
    //     int stone_count = 0;
    //     int stick_count = 0;
    //     //PLANK
    //     int log_count = 0;
    //     //Foundation
    //     int plank_count = 0;

    //     //--COOKING
    //     ////-Mushroom
    //     int mushroom_count = 0;
    //     //Carrot
    //     int carrot_count = 0;
    //     //Apple
    //     int apple_count = 0;
    //     //Orange
    //     int orange_count = 0;
    //     //Purple Fluorite
    //     int purple_fluorite_count = 0;


        // inventoryItemList = InventorySystem.Instance.itemList;

    //     foreach(string itemName in inventoryItemList){

    //         switch(itemName){

    //             case "Stone":
    //                 stone_count += 1;
    //                 break;
    //             case "Stick":
    //                 stick_count += 1;
    //                 break;
    //             case "Log":
    //                 log_count += 1;
    //                 break;
    //             case "Plank":
    //                 plank_count += 1;
    //                 break;
    //             case "Mushroom":
    //                 mushroom_count += 1;
    //                 break;
    //             case "Carrot":
    //                 carrot_count += 1;
    //                 break;
    //             case "Apple":
    //                 apple_count += 1;
    //                 break;
    //             case "Orange":
    //                 orange_count += 1;
    //                 break;
    //             case "Purple Fluorite":
    //                 purple_fluorite_count += 1;
    //                 break;
    //         }
    //     }

    //     //----AXE-----------------------------------
    //     AxeReq1.text = "3 Stone [" + stone_count +"]";
    //     AxeReq2.text = "3 Stick [" + stick_count +"]";

    //     if(stone_count >= 3 && stick_count >= 3 && InventorySystem.Instance.CheckSlotsAvailable(1)){
    //         craftAxeBTN.gameObject.SetActive(true);
    //     } else {
    //         craftAxeBTN.gameObject.SetActive(false);
    //     }

    //     //----PICKAXE-----------------------------------
    //     PickaxeReq1.text = "3 Stone [" + stone_count +"]";
    //     PickaxeReq2.text = "3 Stick [" + stick_count +"]";

    //     if(stone_count >= 3 && stick_count >= 3 && InventorySystem.Instance.CheckSlotsAvailable(1)){
    //         craftPickaxeBTN.gameObject.SetActive(true);
    //     } else {
    //         craftPickaxeBTN.gameObject.SetActive(false);
    //     }

    //     //----SWORD-----------------------------------
    //     SwordReq1.text = "3 Plank [" + plank_count +"]";
    //     SwordReq2.text = "3 Purple Fluorite [" + purple_fluorite_count +"]";

    //     if(plank_count >= 3 && purple_fluorite_count >= 3 && InventorySystem.Instance.CheckSlotsAvailable(1)){
    //         craftSwordBTN.gameObject.SetActive(true);
    //     } else {
    //         craftSwordBTN.gameObject.SetActive(false);
    //     }



    //     //----PLANK----------------------------------
    //     PlankReq1.text = "1 Log [" + log_count +"]";

    //     if(log_count >= 1 && InventorySystem.Instance.CheckSlotsAvailable(2)){
    //         craftPlankBTN.gameObject.SetActive(true);
    //     } else {
    //         craftPlankBTN.gameObject.SetActive(false);
    //     }
    //     //-------------------------------------------


    //     //----Foundation-----------------------------
    //     FoundationReq1.text = "1 Plank [" + plank_count +"]";

    //     if(plank_count >= 1 && InventorySystem.Instance.CheckSlotsAvailable(2)){
    //         craftFoundationBTN.gameObject.SetActive(true);
    //     } else {
    //         craftFoundationBTN.gameObject.SetActive(false);
    //     }
    //     //-------------------------------------------

    //      //----Wall----------------------------------
    //     WallReq1.text = "1 Plank [" + plank_count +"]";

    //     if(plank_count >= 1 && InventorySystem.Instance.CheckSlotsAvailable(2)){
    //         craftWallBTN.gameObject.SetActive(true);
    //     } else {
    //         craftWallBTN.gameObject.SetActive(false);
    //     }
    //     //-------------------------------------------

    //     //===========COOKING=================//

    //      //----SkeweredMushroom----------------------------------
    //     SkeweredMushroomReq1.text = "3 Mushroom [" + mushroom_count +"]";

    //     if(mushroom_count >= 3 && InventorySystem.Instance.CheckSlotsAvailable(1)){
    //         craftSkeweredMushroomBTN.gameObject.SetActive(true);
    //     } else {
    //         craftSkeweredMushroomBTN.gameObject.SetActive(false);
    //     }
    //     //-------------------------------------------

    //     //----Vegatable Soup-----------------------------------
    //     VegatableSoupReq1.text = "2 Mushroom [" + mushroom_count +"]";
    //     VegatableSoupReq2.text = "2 Carrot [" + carrot_count +"]";

    //     if(mushroom_count >= 2 && carrot_count >= 2 && InventorySystem.Instance.CheckSlotsAvailable(1)){
    //         craftVegatableSoupBTN.gameObject.SetActive(true);
    //     } else {
    //         craftVegatableSoupBTN.gameObject.SetActive(false);
    //     }
    //     //-------------------------------------------

    //     //----Fruit Platter-----------------------------------
    //     FruitPlatterReq1.text = "3 Apple [" + apple_count +"]";
    //     FruitPlatterReq2.text = "3 Orange [" + orange_count +"]";

    //     if(apple_count >= 3 && orange_count >= 3 && InventorySystem.Instance.CheckSlotsAvailable(1)){
    //         craftFruitPlatterBTN.gameObject.SetActive(true);
    //     } else {
    //         craftFruitPlatterBTN.gameObject.SetActive(false);
    //     }
    //     //-------------------------------------------


    // }

    IEnumerator craftedDelayForSoundRecipe(CraftingRecipe blueprintToCraft){
        yield return new WaitForSeconds(0f);
        // Produce the amount of items according to the blueprint
        for (var i = 0; i < blueprintToCraft.requiredSlots; i++){
            //add item into inventory
            InventorySystem.Instance.AddToInventory(blueprintToCraft.recipeName);
        } 
    }

    void CraftAnyItemRecipe(CraftingRecipe blueprintToCraft){

        if (blueprintToCraft == null)
        {
            Debug.LogError("Blueprint is null");
            return;
        }
        SoundManager.Instance.craftingSound.Stop();
        SoundManager.Instance.PlaySound(SoundManager.Instance.craftingSound);

        // Produce the amount of items according to the blueprint
        StartCoroutine(craftedDelayForSoundRecipe(blueprintToCraft));

        //remove resources from inventory
        for (int i = 0; i < blueprintToCraft.requiredItems.Count; i++){
            InventorySystem.Instance.RemoveItem(blueprintToCraft.requiredItems[i], blueprintToCraft.requiredItemCounts[i]);
        }

        //refreah list
        InventorySystem.Instance.ReCalculateList();

        StartCoroutine(calculate());

        // RefreshNeededItems();

    }


    public void RefreshNeededItems(){
        Dictionary<string, int> itemCounts = new Dictionary<string, int>();
        inventoryItemList = InventorySystem.Instance.itemList;

        // Initialize item counts
        foreach (string itemName in inventoryItemList)
        {
            if (!itemCounts.ContainsKey(itemName))
            {
                itemCounts[itemName] = 0;
            }
        }

        // Count items
        foreach (string itemName in inventoryItemList)
        {
            itemCounts[itemName]++;
        }

        // Update crafting recipe requirements and buttons
        foreach (CraftingRecipe recipe in craftingRecipes)
        {
            bool canCraft = InventorySystem.Instance.CheckSlotsAvailable(recipe.requiredSlots);
            for (int i = 0; i < recipe.requiredItems.Count; i++)
            {
                string itemName = recipe.requiredItems[i];
                int requiredCount = recipe.requiredItemCounts[i];
                int currentCount = itemCounts.ContainsKey(itemName) ? itemCounts[itemName] : 0;

                recipe.requirementTexts[i].text = $"{requiredCount} {itemName} [{currentCount}]";

                if (currentCount < requiredCount)
                {
                    canCraft = false;
                }
            }
            
            // recipe.craftButton.onClick.AddListener(delegate {CraftAnyItemRecipe(recipe);});

            recipe.craftButton.gameObject.SetActive(canCraft);
        }
    }

}
