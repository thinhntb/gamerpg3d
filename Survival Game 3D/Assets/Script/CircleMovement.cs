using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleMovement : MonoBehaviour
{
    public Transform target; // Đối tượng mà main camera sẽ xoay xung quanh
    public float radius; // Bán kính của vòng tròn
    public float speed; // Tốc độ di chuyển của camera

    private float angle; // Góc hiện tại của camera

    void Start()
    {
        // Đặt camera tại vị trí ban đầu
        Vector3 initialPosition = target.position + new Vector3(radius, 0, 0);
        transform.position = initialPosition;
    }

    void Update()
    {
        // Tính toán góc mới và cập nhật vị trí của camera
        angle += speed * Time.deltaTime;
        float x = target.position.x + radius * Mathf.Cos(angle);
        float z = target.position.z + radius * Mathf.Sin(angle);
        Vector3 newPosition = new Vector3(x, transform.position.y, z);

        // Di chuyển camera và hướng nhìn theo đường đi
        Vector3 previousPosition = transform.position;
        transform.position = newPosition;
        transform.rotation = Quaternion.LookRotation(newPosition - previousPosition);
    }
}