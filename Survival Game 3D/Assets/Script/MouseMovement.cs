using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    float xRotation = 0f;
    float yRotation = 0f;

    public GameObject MainCamera1;
    public GameObject MainCamera3;
    public GameObject CMvcam;

    public GameObject Header;
    public GameObject LegR, LegL;
    public GameObject Player;

    public GameObject toolHoder;

    public GameObject ConstructionHolder;

    // public GameObject Head;

    private bool isCam3 = false;

    void Start() {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update() {
        if (InventorySystem.Instance.isOpen == false && !CraftingSystem.instance.isOpen)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            yRotation += mouseX;

            ConstructionHolder.transform.localRotation = Quaternion.Euler(xRotation, ConstructionHolder.transform.localRotation.y, 0f);

            if (!isCam3)
            {
                // Góc nhìn thứ nhất
                Header.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                MainCamera1.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                Player.transform.localRotation = Quaternion.Euler(0f, yRotation, 0f);
            }
            else
            {
                // Góc nhìn thứ ba
                Header.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                CMvcam.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                MainCamera3.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                Player.transform.localRotation = Quaternion.Euler(0f, yRotation, 0f);
            }
        }



        if(Input.GetKeyDown(KeyCode.F5)){
            isCam3 = !isCam3;
            if(isCam3){
                MainCamera1.SetActive(false);
                toolHoder.SetActive(false);
                MainCamera3.SetActive(true);
                CMvcam.SetActive(true);

                xRotation = 0f;
                transform.localRotation = Quaternion.Euler(0f, yRotation, 0f);
                
            } else {
                MainCamera1.SetActive(true);
                toolHoder.SetActive(true);
                MainCamera3.SetActive(false);
                CMvcam.SetActive(false);

            }
        }

    }
}
