using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControllMenu1 : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject SettingMenu;

    public AudioSource clickSound;
    public AudioSource playSound;

    public void choiMoi(){
        playSound.Play();
        SceneManager.LoadScene(1);
    }

    public void thoat(){
        clickSound.Play();
        Application.Quit();
    }

    public void toMainMenu(){
        clickSound.Play();
        MainMenu.SetActive(true);
        SettingMenu.SetActive(false);
    }

    public void toSettingMenu(){
        clickSound.Play();
        MainMenu.SetActive(false);
        SettingMenu.SetActive(true);
    }
}
